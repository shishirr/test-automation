from locust import HttpLocust, TaskSet, task
from bs4 import BeautifulSoup
import re
import csv
import datetime

USER_CREDENTIALS = []

epoch = int(datetime.datetime.now().strftime("%s")) * 1000

with open('creds.csv', 'r', newline='') as file:
    csvreader = csv.reader(file)
    next(csvreader)
    for row in csvreader:
        USER_CREDENTIALS.append(row[0])

def csrfTokenParser(response):
    soup = BeautifulSoup(response.content, 'html.parser')
    try:
        csrfTokenKey = re.search('js_CsrfTokenName="(.+?)";', str(soup)).group(1)
        csrfTokenValue = re.search('js_CsrfTokenValue="(.+?)";', str(soup)).group(1)
        print({csrfTokenKey:csrfTokenValue})
        return csrfTokenKey, csrfTokenValue
    except AttributeError:
        print("cannot find token")

def roleIdParser(response):
    soup = BeautifulSoup(response.content, 'html.parser')
    try:
        roleId = re.search(r'javascript: selectRole\(\'(.*?)\', event\)', str(soup)).group(1)
        print(roleId)
        return roleId
    except AttributeError:
        print("cannot find roleId")

class UserBehavior(TaskSet):

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        if len(USER_CREDENTIALS) > 0:
            username = USER_CREDENTIALS.pop()
            self.login(username)

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """

    def login(self, username):
        response1 = self.client.get("/frbweb/logon.do?operation=index&client=FEMA&redir=1")
        csrfTokenKey1, csrfTokenValue1 =  csrfTokenParser(response1)
        response = self.client.post("/frbweb/j_spring_security_check", {"accountid":100, "username":username, "password":username, csrfTokenKey1:csrfTokenValue1})
        csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
        roleId = roleIdParser(response)
        self.client.post("/frbweb/restricted/selectrole.do", {csrfTokenKey:csrfTokenValue, "operation":"roleSelected", "userGroupRoleId": roleId})

    @task(2)
    def home(self):
        response = self.client.get("/frbweb/logon.do?operation=index")
        csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
        self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "fhrspecialisthome", "NAV_MENU":"fhrspecialisthome", "employeeId":"",csrfTokenKey: csrfTokenValue})

    @task(2)
    def caseTracking(self):
        response = self.client.get("/frbweb/logon.do?operation=index")
        csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
        headers = response.headers
        headers[csrfTokenKey] = csrfTokenValue
        self.client.get("/frbweb/fhrnavigator/case-tracking/get-case-count-list", headers=headers, params={"overrideTimeout": "false", "_": epoch})

    @task
    class performanceSummary(TaskSet):

        @task(1)
        def perfSummary(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2

            self.client.get("/frbweb/fhrnavigator/performance_plan_statuses/fiscal_years", headers=headers)
        
        @task(1)
        def summary_null(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2

            self.client.get("/frbweb/fhrnavigator/performance_plan_statuses/2019/summary/null", headers=headers, params={"tps":int(datetime.datetime.now().strftime("%s")) * 1000})

        @task(1)
        def fiscal_years(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2

            self.client.get("/frbweb/fhrnavigator/performance_plan_statuses/fiscal_years", headers=headers)

        @task(1)
        def by_organization(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2

            self.client.get("/frbweb/fhrnavigator/performance_plan_statuses/2019/summary/null/by_organization", headers=headers, params={"tps":int(datetime.datetime.now().strftime("%s")) * 1000})

        @task(1)
        def supervisors(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2
            
            self.client.get("/frbweb/fhrnavigator/performance_plan_statuses/2019/summary/null/supervisors", headers=headers, params={"tps":int(datetime.datetime.now().strftime("%s")) * 1000})

        @task(1)
        def organizations(self):
            response = self.client.get("/frbweb/logon.do?operation=index")
            csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
            response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "performancesummary", "NAV_MENU":"performancesummary", "employeeId":"",csrfTokenKey: csrfTokenValue})

            headers = response2.headers
            csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
            headers[csrfTokenKey2] = csrfTokenValue2
            
            self.client.get("/frbweb/fhrnavigator/organizations/null", headers=headers)

    @task(2)
    def performance(self):
        response = self.client.get("/frbweb/logon.do?operation=index")
        csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
        headers = response.headers
        headers[csrfTokenKey] = csrfTokenValue
        self.client.get("/frbweb/fhrnavigator/case-tracking/get-cases", headers=headers, params={"caseTypeId":1006, "listTypeForward":"caseTracking", "openOnly":"Y", "_":epoch})

    @task(2)
    def viewAllCases(self):
        response = self.client.get("/frbweb/logon.do?operation=index")
        csrfTokenKey, csrfTokenValue =  csrfTokenParser(response)
        response2 = self.client.post("/frbweb/restricted/submitMenu.do?operation=processLeftMenu", {"NAV_DESTINATION": "assignedCase", "NAV_MENU":"caseTracking", "ctugrid":"", "HRFUNCODE":None,csrfTokenKey: csrfTokenValue})
        headers = response2.headers
        csrfTokenKey2, csrfTokenValue2 =  csrfTokenParser(response2)
        headers[csrfTokenKey2] = csrfTokenValue2
        params = {"openOnly":"Y","toDateFields[0]":"","toDateFields[1]":"","toDateFields[2]":"",	"toDateFields[3]":"",	"toDateFields[4]":"",	"listTypeForward":"assignedCase","charFields[0]":"","charFields[1]":"",	"charFields[2]":"",	"charFields[3]":"",	"charFields[4]":"",	"charFields[5]":"",	"charFields[6]":"",	"charFields[7]":"",	"charFields[8]":"",	"charFields[9]":"",	"dateFields[0]":"",	"dateFields[1]":"",	"dateFields[2]":"",	"dateFields[3]":"",	"dateFields[4]":"",	"numberFields[0]":"","numberFields[1]":"","numberFields[2]":"",	"numberFields[3]":"",	"numberFields[4]":"",	"_": epoch}
        self.client.get("/frbweb/fhrnavigator/case-tracking/get-cases", headers=headers, params=params)

    

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 20000