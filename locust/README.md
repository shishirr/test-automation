###  Installing and running Locust

Install Python 3 on your local machine

https://www.python.org/downloads/

If pip package manager was not installed

https://pip.pypa.io/en/stable/installing/

Then simply run: 

python3 -m pip install locustio

The fedhrFema.py file also uses a scraper, BeautifulSoup4, to pull the csrf token from the page source.

Once you have pip installed you can also install it with pip.

python3 -m pip install beautifulsoup4

### Getting Started

The documentation on Locust is very organized and has lots of good examples. Take a look at the quick start to get a quick script up and running.

https://docs.locust.io/en/stable/index.html

To run the script, just run the following command in the directory with the locust file. Where the example host is the base url of the site you are trying to test and the file name is preceded by -f.

locust -f my_locust_file.py --host=http://example.com

If it runs successfully, you should be able to get to the web GUI at localhost:8089 where it asks you how many users you want to simulate and how fast do you want it to spawn them.