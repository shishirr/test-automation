ECHO OFF
ECHO Batch file to run burp jar file in windows with recommended arguments
JAVA -jar -Xmx2G -Djava.awt.headless=true burpsuite_pro_v2020.5.1jar --project-file=fedhr.burp --auto-repair --config-file=burp-proj-fedhr.json --user-config-file=burp-user_windows-fedhr.json --unpause-spider-and-scanner --proxy-port 4646 --shutdown-port 4444 -p -v
PAUSE