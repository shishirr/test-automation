﻿### Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}

### JAVA_PATH to use
$javapath = "C:\Program Files\Java\Amazon Corretto 8\jdk1.8.0_222"
$path2add = $javapath + '\bin;'
$path2delete = "C:\ProgramData\Oracle\Java\javapath;"

$oldValue = [Environment]::GetEnvironmentVariable('JAVA_HOME', 'Machine')
Write-Host "old JAVA_HOME: $oldValue"
[Environment]::SetEnvironmentVariable('JAVA_HOME', $javapath,'Machine')
$newValue = [Environment]::GetEnvironmentVariable('JAVA_HOME', 'Machine')
Write-Host "new JAVA_HOME: $newValue"

### delete unnecessary entries from the user path
$userPath = (Get-Item -path "HKCU:\Environment" ).GetValue('Path', '', 'DoNotExpandEnvironmentNames')
Write-Host "`nuser path: $($userPath.replace(";", ";`n"))"

if ($userPath.contains($path2delete)) {
    $userPath = $userPath.replace($path2delete, '')
}
if ($userPath.contains($oldValue + '\bin;')) {
    $userPath = $userPath.replace($oldValue + '\bin;', '')
}

$value = [Environment]::GetEnvironmentVariable('path', 'User')
Write-Host "current user path: $($value.replace(";", ";`n"))"

### modify the system path
$systemPath = (Get-Item -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" ).
  GetValue('Path', '', 'DoNotExpandEnvironmentNames')
Write-Host "`nsystem path: $($systemPath.replace(";", ";`n"))"

if ($systemPath.contains($path2delete)) {
    $systemPath = $systemPath.replace($path2delete, '')
}
if ($systemPath.contains($oldValue + '\bin;')) {
    $systemPath = $systemPath.replace($oldValue + '\bin;', '')
}

if (!$systemPath.contains('%JAVA_HOME%\bin;')) {
    if (!$systemPath.contains($path2add)) {
        $systemPath = $path2add + $systemPath
        [Environment]::SetEnvironmentVariable('Path', $systemPath, 'Machine')
        Write-Host ">> Added to path!`n"
    }
}
else {
    Write-Host ">> No change required.`n"
}
$value = [Environment]::GetEnvironmentVariable('path', 'Machine')
Write-Host "current system path: $($value.replace(";", ";`n"))"
Read-Host "`nPress Enter key to exit"
