# Burp

#### Installing Burp
Install Burp Professional version from the [user account](https://portswigger.net/users) provided by Econsys.

#### Setting up Burp for scanning
1. Chekc out this git repo.
2. Open the Burp Pro app and load the project from the file (**.burp** file)
3. Change the proxy setting from your OS and set up a manual proxy for localhost (127.0.0.1) at port 8080.
4. Create a **New Live Task** from the dashboard having the suite scope set.
5. Now start the task (hit the play button).
6. Run your automation suite. 

Burp will scan all in-scope traffic and list any vulnerabilities.

#### Generating report
- Once the automation test suite finished running, generate a report by clicking *View  Details >>*  in the task box.
- Click on the *Issue activity* tab.
- Export the issues by selecting all of them, and then on the right-click menu, select *Report selected issues*

<br>
----
[![N|Solid](https://fhrnavigator.com/frbweb/images/logo-fedhr-nav.png)](https://www.fedhrnavigator.com)