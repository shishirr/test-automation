ECHO OFF
ECHO Batch file to generate reports from the project file with recommended arguments
JAVA -jar -Xmx2G -Djava.awt.headless=true burpsuite_pro_v2020.8.1.jar --project-file=fedhr.burp --auto-repair --config-file=burp-proj-fedhr.json --user-config-file=burp-user_windows-fedhr.json -htmlreport report.htm -xmlreport report.xml
PAUSE