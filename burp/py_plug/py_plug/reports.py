from burp import IBurpExtender
from java.io import File


class BurpExtender(IBurpExtender):

    def registerExtenderCallbacks(self, callbacks):
        callbacks.setExtensionName("Headless Burp Report")
        args = callbacks.getCommandLineArguments()
        if '-htmlreport' in args or '-xmlreport' in args:
            issues = callbacks.getScanIssues('')            
            if '-htmlreport' in args:
                output = File(args[args.index('-htmlreport') + 1])
                callbacks.generateScanReport('HTML', issues, output)
                self.log(callbacks, "HTML report file '" + output.getName() + "' created!")
            if '-xmlreport' in args:
                output = File(args[args.index('-xmlreport') + 1])
                callbacks.generateScanReport('XML', issues, output)
                self.log(callbacks, "XML report file '" + output.getName() + "' created!")
            self.log(callbacks, "Report(s) created, now quitting Burp!")
            callbacks.exitSuite(False)
    
    def log(self, callbacks, message):
        callbacks.issueAlert(message)
        callbacks.printOutput(message)
