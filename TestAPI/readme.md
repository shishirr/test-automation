# Welcome to EconSys QA

## This is a TestAPI site!
This API is used to access database in the test environment.

## How to setup the project in local environment?
1. Download Python 3.7 using Anaconda Distribution.
2. (Optional) Install OpenSSL.
2. From terminal or command-prompt, navigate to the project folder and run following commands in order -
\
`$ pip install virtualenv` --- to use a Python virtual env
\
`$ virtualenv venv` --- the environment will be initialized in 'venv' folder
\
`$ source venv/bin/activate` (or `$ venv\Scripts\activate`)  to get into virtualenv shell
\
`$ pip install flask sqlalchemy flask_restful cx-Oracle` --- these are the required modules
\
`$ pip freeze` --- to check installed modules

After project setup, `pip` should indicate the following modules installed in the virtual env.
```
aniso8601==6.0.0
Click==7.0
cx-Oracle==7.1.2
Flask==1.0.2
Flask-RESTful==0.3.7
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.1
pytz==2018.9
six==1.12.0
SQLAlchemy==1.3.1
Werkzeug==0.15.0
```

### Available Endpoints
    /adminsWoROB
    /adminsWithROB
    /agencies
    /employees
    /<account_id>/employees

### JSON Data Format
##### User accounts
```
"data": [
    {
      "account_id": 218, 
      "client_code": "AKRR", 
      "first_name": "First 1001897", 
      "last_name": "Last 1001897", 
      "password": "user1001897", 
      "role_name": "Administrator", 
      "username": "user1001897"
    }]
```
#### Agencies
```
    "data": [
    {
      "account_id": 100, 
      "client_code": "FEMA"
    }]
```

### How to insert data?
* `\employees` can be used to insert data via HTTP POST method using a JSON payload.
```
    {
      "account_id": 224, 
      "user_name": "Mahbub Rahman", 
      "user_pass": "<md5hash>", 
      "user_email": "testuser@company.com"
    }
```