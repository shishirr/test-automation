from flask import jsonify
from flask_restful import Resource
from database.db import db_engine

agencyList = """
SELECT URL_CODE AS client_code
	,account_id AS account_id
FROM cp_account_settings
WHERE url_code IS NOT NULL
ORDER BY account_id ASC
"""


class Agencies(Resource):
    def get(self):
        conn = db_engine.connect()
        query = conn.execute(agencyList)
        result = {'data': [dict(zip(tuple(query.keys()), i))
                           for i in query.cursor]}
        return jsonify(result)