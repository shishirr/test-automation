from flask import request, jsonify
from flask_restful import Resource
from database.db import db_engine

filteredEmployeesWithROB = """
SELECT a.URL_CODE AS client_code
		,a.account_id AS account_id
		,u.user_name AS Username
		,u.user_name AS Password
		,u.USER_FNAME AS FIRST_NAME
		,u.USER_LNAME AS LAST_NAME
		,r.role_name
		,g.group_name
		,u.user_id
		,DECODE((
				SELECT MAX(1)
				FROM employee_data e
				WHERE u.user_id = e.employee_id
				), NULL, 'No', 'Yes') AS is_employee
		,DENSE_RANK() OVER (
			PARTITION BY a.URL_CODE
			,r.role_name ORDER BY u.user_id
				,a.URL_CODE
				,r.role_name
			) AS rnk
	FROM sa_account sa
	INNER JOIN cp_account_settings a ON sa.account_id = a.account_id
	INNER JOIN cp_user u ON a.account_id = u.account_id
	INNER JOIN cp_USER_GROUP_ROLE ugr ON u.user_id = ugr.user_id
	INNER JOIN cp_group_role gr ON gr.group_role_id = ugr.group_role_id
	INNER JOIN cp_group g ON g.group_id = gr.group_id
	INNER JOIN cp_role r ON r.role_id = gr.role_id
	WHERE r.ROLE_NAME = 'Employee'
		AND u.user_name LIKE 'user%'
		AND u.USER_PW IS NOT NULL
		AND sa.acct_status = 'O'
		AND user_status = 'O'
		AND u.USER_EMAIL IS NOT NULL
		AND u.rofb_accept_date IS NOT NULL
		AND u.INITIALIZED_Y_N = 'Y'
		AND g.GROUP_NAME != 'Former Employees'
		AND sa.account_id = {}
		AND DECODE((
				SELECT MAX(1)
				FROM employee_data e
				WHERE u.user_id = e.employee_id
				), NULL, 'No', 'Yes') = 'Yes'
	ORDER BY a.URL_CODE
		,r.ROLE_NAME
"""

class FilteredEmployees(Resource):
    def get(self, account_id):
        conn = db_engine.connect()
        try:
            query = conn.execute(filteredEmployeesWithROB.format(int(account_id)))
        except ValueError:
            return {'error': 'invalid literal', 'suggestion': "provide a 3-digit agency code"}
        result = {'data': [dict(zip(tuple(query.keys()), i))
                           for i in query.cursor]}
        return jsonify(result)