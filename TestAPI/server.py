from flask import Flask
from flask_restful import Api
from resources.hr_admin_rob import HRAdminWithROB
from resources.hr_admin_wo_rob import HRAdminWoROB
from resources.agencies import Agencies
from resources.employees import Employees
from resources.filtered_employees import FilteredEmployees

# Basic Help URLs using Flask
#############################
app: Flask = Flask(__name__)


@app.route('/')
def root():
    return app.send_static_file('welcome.html')


@app.route('/projects/')
def projects():
    return app.send_static_file('projects.html')


@app.route('/test')
def test():
    return "<h3>API is live!</h3>"


# API routes
############
api = Api(app)

api.add_resource(HRAdminWoROB, '/adminsWoROB')
api.add_resource(HRAdminWithROB, '/adminsWithROB')
api.add_resource(Agencies, '/agencies')
api.add_resource(Employees, '/employees')
api.add_resource(FilteredEmployees, '/<account_id>/employees')

if __name__ == '__main__':
    app.run(port='5000', debug=True)
