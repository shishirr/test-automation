import sqlalchemy.engine
from sqlalchemy import create_engine
from database import settings

db_engine: sqlalchemy.engine.Engine = create_engine(
    '{dialect+driver}://{username}:{password}@{host}:{port}/{sid}'.format_map(
        settings.FEDHR03
    ))
