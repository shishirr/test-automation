WITH user_role_view
AS (
    SELECT sa.account_id,acct_status                --sa_account
            ,UPPER(A.url_code) AS url_code          --cp_account_settings
        ,u.user_id,user_name,user_pw,user_email,email_validated_date,INITIALIZED
            ,user_status,status_date,force_pw_change,pw_create_date,pw_locked
            ,user_lname,user_fname,rofb_accept_date --cp_user
        ,ugr.user_group_role_id                     --cp_user_group_role
        ,gr.group_role_id                           --cp_group_role
        ,G.group_id,group_name                      --cp_group
        ,R.role_id,role_name,role_type              --cp_role
        ,DECODE((
                SELECT 1
                FROM employee_data E
                WHERE u.user_id = E.employee_id
                ), NULL, 'No', 'Yes') AS is_employee
        ,DENSE_RANK() OVER (
            PARTITION BY A.url_code,R.role_type,R.role_name
            ORDER BY A.url_code,R.role_name,u.user_id
            ) AS rnk
    FROM sa_account sa
        INNER JOIN cp_account_settings A ON A.account_id = sa.account_id
        INNER JOIN cp_user u ON u.account_id = A.account_id
        INNER JOIN cp_user_group_role ugr ON ugr.user_id = u.user_id
        INNER JOIN cp_group_role gr ON gr.group_role_id = ugr.group_role_id
        INNER JOIN cp_group G ON (
            G.GROUP_ID = gr.GROUP_ID
            AND G.account_id = sa.account_id
        )
        INNER JOIN cp_role R ON (
            R.role_id = gr.role_id
            AND R.account_id = sa.account_id
        )
    WHERE sa.acct_status = 'O'                      -- account is open
        AND UPPER(sa.account_type) NOT LIKE '%SBX%' -- account is not of a 'sandbox' type
    )
    ,role_priv_view
AS (
    SELECT rf.role_function_id,role_id,include                     --cp_role_function
        ,F.function_id,function_name,function_cd,function_comment  --ap_function
    FROM cp_role_function rf
        INNER JOIN ap_function F ON F.function_id = rf.function_id
    WHERE include = 'Y' -- putting the 'where' clause here to reduce the dataset
        AND function_cd IN ('CP','LOGIN') -- privileges we are looking for
    )
 
SELECT account_id
    ,url_code
    ,role_name
    ,user_name
    ,user_name AS user_pw
FROM user_role_view urv
    INNER JOIN role_priv_view rpv ON urv.role_id = rpv.role_id
WHERE url_code IS NOT NULL
--    AND role_name IN ('Administrator', 'Employee') -- roles we are looking for
    AND user_name IS NOT NULL
--    AND user_pw = get_pw_hash(user_name) -- otherwise we can't login as username
    AND acct_status = 'O' -- client account is open
    AND user_status = 'O' -- user account is open
    AND pw_locked IS NULL
    AND force_pw_change = 'N'
    AND initialized IS NOT NULL
    AND email_validated_date IS NOT NULL
--    AND user_email IS NOT NULL -- optional
--    AND rofb_accept_date IS NOT NULL -- optional
    AND LOWER(group_name) NOT LIKE '%former%' -- exclude former employees
    AND url_code NOT IN ('ARCPTEST','HIRINGTEST') -- exclude test client accounts
    AND rnk = 1 -- get just one user for each role
GROUP BY account_id, url_code, role_name, user_name
ORDER BY account_id ASC,role_name ASC