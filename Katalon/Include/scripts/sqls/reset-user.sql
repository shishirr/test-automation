UPDATE cp_user
SET USER_PW = get_pw_hash(:p_user_name)
    ,LAST_LOGIN_DATE = sysdate
    ,SUSPENSE_DATE = NULL
    ,PW_CREATE_DATE = sysdate
    ,EMAIL_VALIDATED_DATE = sysdate
    ,EMAIL_VALIDATION_CODE = NULL
    ,user_email = :p_user_name || '@econsys.com'
    ,FORCE_PW_CHANGE = 'N'
    ,user_status = 'O'
    ,LOGIN_FAILURE_COUNT = 0
    ,PW_LOCKED = NULL
WHERE user_name = :p_user_name