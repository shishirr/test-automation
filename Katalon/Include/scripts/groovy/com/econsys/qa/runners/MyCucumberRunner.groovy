package com.econsys.qa.runners
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.runner.RunWith

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import internal.GlobalVariable


@RunWith(Cucumber.class)
@CucumberOptions(
features = "Include/features", glue = "Include/scripts/groovy",
//tags = "@regression and (@PDPlus and @positive)",	// using a tag expression
tags = "@test",
//tags = ["@run", "@positive"],				// using a String array
dryRun = true,							// useful to see which scenario would get run
//strict = true,							// mark as fail when there are undefined or pending steps
plugin = ["pretty",
	"junit:Reports/cucumber_runner/cucumber.xml",
	"html:Reports/cucumber_runner",
	"json:Reports/cucumber_runner/cucumber.json"]
)
public class MyCucumberRunner
{

	@BeforeClass
	public static void setUp()
	{
		KeywordUtil.logInfo("> Starting a Cucumber test run...")
		KeywordUtil.logInfo("> ")
		KeywordUtil.logInfo("> Test agency being used\t: " + GlobalVariable.agency)
		KeywordUtil.logInfo("> Test environment being used\t: " + GlobalVariable.env)
	}

	@AfterClass
	public static void tearDown()
	{
		WebUI.closeBrowser()
		KeywordUtil.logInfo("*** This is the END of the Cucumber test run! ***")
	}
}
