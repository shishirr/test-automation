package com.econsys.qa.steps.auth
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable


public class Login {
	String loginPageTitle = "FedHR: Login"
	String expectedFailedMsg = "Invalid username and/or password"
	String expectedLockoutMsg = "Too many invalid login attempts"
	String expectedUsernameValidationMsg = "Username is required"
	String expectedPasswordValidationMsg = "Password is required"
	List<String> validLoginAcceptedPage = Arrays.asList(
	'FedHR: Enter/Change email address',
	'FedHR Rules of behavior',
	'FedHR: Select a Role',
	'FedHR: Home/EBC',
	'FedHR: Home')

	@Given("a user navigated to the FedHR login page for an agency")
	public void a_user_navigated_to_the_FedHR_login_page_for_an_agency() {
		WebUI.callTestCase(findTestCase('Test Helper Tasks/Navigate to FedHR Login'), [:])
	}

	@Given("the user provided a valid username\\/password combo for the agency")
	public void the_user_provided_a_valid_username_password_combo_for_the_agency() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'),
				findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1))
		if (GlobalVariable.siteURL) {
			WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), 'TEmp0r@r!1')
		} else {
			WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'),
					findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 2))
		}
	}

	@When("the user attemps to log in")
	public void the_user_logs_in() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/button_Login'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/button_Login'))
	}

	@Then("the system wil allow the user login")
	public void the_system_wil_allow_the_user_login() {
		WebUI.delay(1)
		try {
			assert validLoginAcceptedPage.contains(WebUI.getWindowTitle()) : 'Login failed! Either wrong credentials or wronng role configuration.'
		}
		catch (AssertionError err) {
			WebUI.takeScreenshot()
			throw err
		}
	}

	@Given("the user provided an invalid username\\/password combo for the agency")
	public void the_user_provided_an_invalid_username_password_combo_for_the_agency() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'),
				findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), 'invalid_password')
	}

	@Then("the system will deny the user login")
	public void the_system_will_deny_the_user_login() {
		WebUI.delay(1)
		assert WebUI.getWindowTitle().equals(loginPageTitle)
	}

	@Then("the system will notify the user of logon denial")
	public void the_system_will_notify_the_user_of_logon_denial() {
		assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Login/div_Validation message')).contains(expectedFailedMsg)
	}

	@Given("the user provided a valid username for the agency")
	public void the_user_provided_a_valid_username_for_the_agency() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/input_username'), 5)
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'),
				findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1))
	}

	@Given("the user provided an invalid password for the username")
	public void the_user_provided_an_invalid_password_for_the_username() {
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), 'invalid_password')
	}

	@When("the user attemps to log in for {int} times consecutively")
	public void the_user_attemps_to_log_in_for_times_consecutively(Integer int1) {
		for (int i = 1; i <= int1; i++) {
			WebUI.scrollToElement(findTestObject('/FedHR/Page_FedHR Login/button_Login'), 5)
			WebUI.click(findTestObject('FedHR/Page_FedHR Login/button_Login'))
			WebUI.delay(1)
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
			the_user_provided_an_invalid_password_for_the_username()
		}
	}

	@Then("the system will notify the user to reset the user password")
	public void the_system_will_notify_the_user_to_reset_the_user_password() {
		assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Login/div_Validation message')).contains(expectedLockoutMsg)
	}

	@Given("the user didn't provide the password for the login combo")
	public void the_user_didn_t_provide_the_password_for_the_login_combo() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'),
				findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), '')
	}

	@Then("the system will notify the user of missing username data")
	public void the_system_will_notify_the_user_of_missing_username_data() {
		if(WebUI.verifyAlertPresent(5)) {
			assert WebUI.getAlertText().equals(expectedUsernameValidationMsg)
			WebUI.acceptAlert()
		}
	}

	@Given("the user didn't provide the username for the login combo")
	public void the_user_didn_t_provide_the_username_for_the_login_combo() {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'), 5)
		WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'), '')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1))
	}

	@Then("the system will notify the user of missing password data")
	public void the_system_will_notify_the_user_of_missing_password_data() {
		if(WebUI.verifyAlertPresent(5)) {
			assert WebUI.getAlertText().equals(expectedPasswordValidationMsg)
			WebUI.acceptAlert()
		}
	}
}