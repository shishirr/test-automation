package com.econsys.qa.steps.vacancyannouncement
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class ManageVacancyEmail
{
	String email1 = 'regression_test_1@econsys.com', email2 = 'regression_test_2@econsys.com'
	String emailEntered, innerText_divEmailNoti

	def navigate_to_vacancy_announcement_email_notification()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'), 2)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'))
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Announcement Template'), 'Blank VA Template',
				false)
		WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))
		WebUI.delay(2)
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/strong_Email Notification'), 2)
	}

	@Given("HR Manager opened the vacancy email list")
	public void hr_Manager_opened_the_vacancy_email_list()
	{
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'), 2)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'))
		assert WebUI.getWindowTitle().contains('Manage Vacancy Email') : "It is not the vacancy email management page!"
	}

	@When("the manager adds a few email addresses to the list")
	public void the_manager_adds_a_few_email_addresses_to_the_list()
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/input_Add Email Address'), "${email1}, ${email2}")
		WebUI.click(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/input_email_Add_button'))
	}

	@Then("the email addresses will be added to the notify list")
	public void the_email_addresses_will_be_added_to_the_notify_list()
	{
		emailEntered = WebUI.getText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/div_email_ids_container'))
		assert emailEntered.contains(email1) && emailEntered.contains(email2)
	}

	@Then("the addresses will be offered in the vacancy announcement email notification")
	public void the_addresses_will_be_offered_in_the_vacancy_announcement_email_notification()
	{
		navigate_to_vacancy_announcement_email_notification()
		innerText_divEmailNoti = WebUI.getText(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_emailAddress_notification'))
		assert innerText_divEmailNoti.contains(email1) && innerText_divEmailNoti.contains(email2)
	}

	@When("the manager deletes an email address he addded")
	public void the_manager_deletes_an_email_address_he_addded()
	{
		// deleting the first one added from the list
		WebUI.click(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/button_email_deleteIcon', [('email') : email1]))
	}

	@Then("the deleted email address will no longer exist in the notify list")
	public void the_deleted_email_address_will_no_longer_exist_in_the_notify_list()
	{
		emailEntered = WebUI.getText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/div_email_ids_container'))
		assert !emailEntered.contains(email1) && emailEntered.contains(email2)
	}

	@Then("the address will not be offered in the vacancy announcement email notification")
	public void the_address_will_not_be_offered_in_the_vacancy_announcement_email_notification()
	{
		navigate_to_vacancy_announcement_email_notification()
		innerText_divEmailNoti = WebUI.getText(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_emailAddress_notification'))
		assert !innerText_divEmailNoti.contains(email1) && innerText_divEmailNoti.contains(email2)
	}
}