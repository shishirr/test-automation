package com.econsys.qa.steps.casetracking
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.findWebElement

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.econsys.qa.utils.TestObjUtils
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.SelectorMethod
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.exception.StepFailedException

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

/**
 * The step definitions for 'Add Forms to a Case' feature.
 */
public class AddFormsToCase
{
	int column
	String xpathCaseHasNoForms, xpathCaseHasForms
	String formToAdd = 'SF 52'
	String noFormsConfiguredMsg = "No forms for selected Activity."

	def initLocator()
	{
		column = WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/input_toggleSelectAllDisplayedCheckbox'), 1) ? 4 : 3
		xpathCaseHasNoForms = "(//tr[td[$column][not(contains(normalize-space(text()), 'Recruitment Request')) and count(button[. = 'View Forms']) = 0]])//td/strong/a[1]"
		xpathCaseHasForms = "(//tr[td[$column][child::button[. = 'View Forms']]])//td/strong/a[1]"
	}

	@Given("a case in FedHR without any form package")
	public void a_case_in_FedHR_without_any_form_package()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		initLocator()
		if(WebUI.waitForElementNotPresent(TestObjUtils.getMyTestObject("my-case-locator", "xpath", xpathCaseHasNoForms), 2))
		{
			// go to 'New Case'
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_New Case'), 2)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_New Case'))

			// fill out the form
			// this particular order of form filling is important for this step definition
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), 2)
			int maxIndex_CaseType = TestObjUtils.getOptionsCount(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'))
			boolean keepLooping = true
			for (int currIndex_CaseType = 1; currIndex_CaseType < maxIndex_CaseType; currIndex_CaseType++)
			{
				WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), currIndex_CaseType)
				WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 'last')
				WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Search'))
				WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Select'))

				int maxIndex_Activity = TestObjUtils.getOptionsCount(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'))
				for (int currIndex_Activity = 1; currIndex_Activity < maxIndex_CaseType; currIndex_Activity++)
				{
					WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), currIndex_Activity)
					WebUI.delay(1)
					WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), 1)
					TestObject formList = findTestObject('FedHR/Page_FedHR New Case/span-activity_form_ids')
					if (!WebUI.getText(formList).equals(noFormsConfiguredMsg))
					{
						List<WebElement> checkedBoxes = findWebElement(formList).findElements(By.xpath("./div/input[@checked and @type='checkbox']"))
						JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getWebDriver()
						for (WebElement elem : checkedBoxes)
						{
							js.executeScript("arguments[0].removeAttribute('disabled');" + "arguments[0].removeAttribute('checked');", elem)
						}
						keepLooping = false
						break
					}
				}
				if (!keepLooping) break
			}

			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 2, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 'N', true)

			// create case
			WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Next'))
			WebUI.delay(2)

			WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/fieldset_Case Tracking Informa'), 2)
		}
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
	}

	@Given("HR user opened an existing case without form package")
	public void an_HR_user_opened_an_existing_case_without_form_package()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		TestObject to = TestObjUtils.getMyTestObject("my-case-locator", "xpath", xpathCaseHasNoForms)
		WebUI.scrollToElement(to, 2)
		WebUI.click(to)
	}

	@Given("opened the addable form list for the case type\\/activity pair")
	public void opened_the_addable_form_list_for_the_case_type_activity_combo()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/button_Add Forms'))
	}

	@When("the user adds a form {string} from the form list")
	public void the_user_adds_a_form_from_the_form_list(String string)
	{
		TestObject to = findTestObject('FedHR/Page_FedHR Case Details/input_checkbox_form1 (modal)')
		if (!string.isEmpty())
		{
			formToAdd = string
			String xpathFormToSelect = "//form/div/label[contains(normalize-space(text()), '${formToAdd}')]"
			to.setSelectorValue(SelectorMethod.XPATH, xpathFormToSelect)
		}
		boolean exceptionHandled = false
		try
		{
			formToAdd = WebUI.getText(to)
			WebUI.click(to)
			formToAdd = formToAdd.substring(0, formToAdd.indexOf(' - '))
		}
		catch (StepFailedException sfe)
		{
			KeywordUtil.logInfo("Apparantly no form is configured for the case-activity pair!")
			try
			{
				WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/a_click here_Form Search'))
			}
			catch (StepFailedException ex)
			{
				exceptionHandled = true
				KeywordUtil.logInfo('Missing form add permission: "workfolder.add.form"')
			}
			if (!exceptionHandled)
			{
				WebUI.setText(findTestObject('FedHR/Page_FedHR Case Details/input_Enter form number or name_keyword'), formToAdd)
				WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/input_Enter form number or name_srch'))
				WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/a_Select_Form Name'))
				WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/input_Add selected forms_add'))
			}
			exceptionHandled = true
		}
		if (!exceptionHandled)
		{
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Details/input_Submit'), 2)
			WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/input_Submit'))
			WebUI.delay(2)
			WebUI.acceptAlert()
		}
	}

	@Then("the user should be able to see the new form added to the case")
	public void the_user_should_be_able_to_see_the_new_form_added_to_the_case()
	{
		// look for the added form in Forms Manager
		TestObject to = findTestObject('FedHR/Page_FedHR Folder Contents/a_(Form Number)', [('formNumber') : formToAdd])
		WebUI.scrollToElement(to, 2)
		WebUI.click(to)

		if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Folder Contents/button_No'), 2, FailureHandling.OPTIONAL))
		{
			WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_No'), FailureHandling.CONTINUE_ON_FAILURE)
		}

		// look for the form number on the form; uses implicit variation of the 'verifyElementPresent' assert
		WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/text_(Form Name)', [('formName') : formToAdd]), 2, FailureHandling.OPTIONAL)

		// look for the added form in Case Details
		WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))
		WebUI.delay(2)
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Folder Contents/a_ViewUpdate Case Link'), 2)
		WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/a_ViewUpdate Case Link'))
		WebUI.delay(1)

		WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Details/a_(Form Number)_FormsFolder', [('formNumber') : formToAdd]), 2)
	}

	@Given("a case in FedHR with a form package")
	public void a_case_in_FedHR_with_a_form_package()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		initLocator()
		if(WebUI.waitForElementNotPresent(TestObjUtils.getMyTestObject("my-case-locator", "xpath", xpathCaseHasForms), 2))
		{
			// go to 'New Case'
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_New Case'), 2)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_New Case'))

			// fill out the form
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), 2)
			int maxIndex_CaseType = TestObjUtils.getOptionsCount(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'))
			boolean keepLooping = true
			for (int currIndex_CaseType = 1; currIndex_CaseType < maxIndex_CaseType; currIndex_CaseType++)
			{
				WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), currIndex_CaseType)
				int maxIndex_Activity = TestObjUtils.getOptionsCount(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'))
				for (int currIndex_Activity = 1; currIndex_Activity < maxIndex_CaseType; currIndex_Activity++)
				{
					WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), currIndex_Activity)
					WebUI.delay(1)
					WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), 1)
					TestObject formList = findTestObject('FedHR/Page_FedHR New Case/span-activity_form_ids')
					if (!WebUI.getText(formList).equals(noFormsConfiguredMsg))
					{
						WebUI.check(findTestObject('FedHR/Page_FedHR New Case/input_checkbox_form1'))
						keepLooping = false
						break
					}
				}
				if (!keepLooping) break
			}
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 2)
			WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 'last')
			WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Search'))
			WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Select'))
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 2, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 'N', true)

			// create case
			WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Next'))
			WebUI.delay(2)

			WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/fieldset_Case Tracking Informa'), 2)
		}
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
	}

	@Given("HR user opened an existing case with form package")
	public void an_HR_user_opened_an_existing_case_with_form_package()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		TestObject to = TestObjUtils.getMyTestObject("my-case-locator", "xpath", xpathCaseHasForms)
		WebUI.scrollToElement(to, 2)
		WebUI.click(to)
	}

	@Given("opened the form folder contents for the case")
	public void opened_the_form_folder_contents_for_the_case()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/button_Manage Forms'))
	}

	@When("the user adds a form {string}")
	public void the_user_adds_a_form(String string)
	{
		if (!string.isEmpty()) formToAdd = string
		else KeywordUtil.logInfo("No form name provided! Defaulting to the form 'SF 52'...")

		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Folder Contents/a_Add a Form to this Folder'), 2)
		WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/a_Add a Form to this Folder'))
		try
		{
			WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Search forms'))
		}
		catch (WebElementNotFoundException ex)
		{
			KeywordUtil.logInfo("Apparantly no form is configured for the case-activity pair!")
		}
		WebUI.setText(findTestObject('FedHR/Page_FedHR Folder Contents/input_FormSearchField'), formToAdd)
		WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Search'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Add'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Close'))
		WebUI.delay(1)
	}
}
