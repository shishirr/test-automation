package com.econsys.qa.steps.vacancyannouncement

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class ViewVAInUSAJobs
{
	String vaNumberOnForm = GlobalVariable.testVASeq, vaNumberOnUSAJobs

	@Given("HR user posted a new vacancy announcement")
	public void hr_user_posted_a_new_vacancy_announcement()
	{
		WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Compose and Save a VA'), [:])
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement'), 2)
		vaNumberOnForm = WebUI.getAttribute(findTestObject('Object Repository/FedHR/Page_Job Announcement/input_AnnouncementRegister'), 'value')
		WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'POST_ON_USAJOBS', false)
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)
		WebUI.delay(2)
		assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement')).contains('POSTED')
	}

	@When("the HR user views the USAJobs posting from FedHR")
	public void the_HR_user_views_the_USAJobs_posting_from_FedHR()
	{
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'VIEW_ON_USAJOBS', false)
	}

	@Then("the HR user should be able to see the job posting on USAJOBS")
	public void the_HR_user_should_be_able_to_see_the_job_posting_on_USAJOBS()
	{
		WebUI.switchToWindowTitle('USAJOBS - Job Announcement')
		WebUI.click(findTestObject('Object Repository/FedHR/Page_USAJOBS - Job Announcement/a_Overview'))
		vaNumberOnUSAJobs = WebUI.getText(findTestObject('FedHR/Page_USAJOBS - Job Announcement/p_joa-number'))
		WebUI.verifyMatch(vaNumberOnForm, vaNumberOnUSAJobs, false)
	}

	@Given("HR user amended a posted vacancy announcement")
	public void hr_user_amended_a_posted_vacancy_announcement()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Vacancy Announcements'), 2)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Vacancy Announcements'))
		WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Search for a VA'), [:])
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'AMEND', false)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_Job Announcement/button_Yes'))
		WebUI.delay(2)
		WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'REPOST_ON_USAJOBS', false)
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)
		WebUI.delay(2)
	}

	@When("the HR user cancels the announcement")
	public void the_HR_user_cancels_the_announcement()
	{
		WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'CANCEL', false)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_Job Announcement/button_Yes'))
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)
		WebUI.delay(2)
		WebUI.verifyElementText(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement'), "Announcement # - ${vaNumberOnForm} - CANCELLED")
	}

	@Then("the HR user will not be able to see the job posting on USAJOBS")
	public void the_HR_user_will_not_be_able_to_see_the_job_posting_on_USAJOBS()
	{
		WebUI.verifyOptionNotPresentByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'VIEW_ON_USAJOBS', false, 2)
	}
}
