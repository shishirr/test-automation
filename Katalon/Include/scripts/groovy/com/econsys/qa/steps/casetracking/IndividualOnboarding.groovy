package com.econsys.qa.steps.casetracking
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.Scenario
import cucumber.api.java.After

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

/**
 * The step definitions for 'Individual On-boarding' feature.
 */
class IndividualOnboarding {

	String testBirthYear = '10/10/2000'
	String ssn = new Random().nextLong().abs().toString().substring(0, 9)

	@Given("a manager opened Individual On-Boarding from the homepage")
	public void a_manager_opened_Individual_On_Boarding_from_the_homepage() {
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Individual On-boarding'))
	}

	@Given("the manager filled in all the required fields on Employee Setup view")
	public void the_manager_filled_in_all_the_required_fields_on_Employee_Setup_view() {
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Last Name_lastName'), 'Last')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ First Name_firstName'),
				'First')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Middle Initial_middleNam'),
				'M')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Date of Birth_birthDate'),
				testBirthYear)
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_eeSsn'), ssn)
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_eeReSsn'), ssn)
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_E-mail Address_emailAddr'),
				'fgyamfi@econsys.com')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Employee Title_empTitle'),
				'Tester')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Employing Agency_employi'),
				'Fema')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Phone (Home)_homePhone'),
				'2434866301')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 1_addr1'),
				'101 Main st')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_City_city'), 'Falls Church')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_State_state'), 'VA')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Zip Code_postalCode'), '22442')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 1_workaddr1'),
				'Econsys')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 2_workaddr2'),
				'1 duke St')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_City_workcity'), 'Fall Church')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_State_workstate'), 'VA')
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Zip Code_workpostalCode'),
				'45684')
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/option_Executive Resources (EX'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Organization_organizati'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/a_Select_EmpOrg'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Supervisor_supervisorLoo'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Enter employee name and'),
				'LAST')
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/div_Enter employee name and cl'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Enter employee name and _1'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/a_Select_EmpSearch'))
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_LAST 1000015 FIRST 10000')
				, 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_LAST 1000015 FIRST 10000'))
	}

	@Given("all other required fields are filled on the On-boarding view")
	public void all_other_required_fields_are_filled_on_the_On_boarding_view() {
		WebUI.setText(findTestObject('FedHR/Page_FedHR On-boarding/input_Effective Date of Employ'), 'TODAY')
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_SelectOn-boarding'), '561',
				true)
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_SelectNew Appointment T'),
				'9711', true)
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_YesNo'), '2', true)
	}

	@When("the manager saves the case")
	public void the_manager_saves_the_case() {
		WebUI.click(findTestObject('FedHR/Page_FedHR On-boarding/input_Print_btnstandard'))
	}

	@Then("a new On-boarding case should be created under On-boarding case tracking")
	public void a_new_On_boarding_case_should_be_created_under_On_boarding_case_tracking() {
		// TODO: needs fix here
		WebUI.click(findTestObject('FedHR/Page_FedHR Personal Information/a_Home'))

		WebUI.setText(findTestObject('FedHR/Page_FedHR Home/input_Search_searchString'), ssn)

		WebUI.click(findTestObject('FedHR/Page_FedHR Home/i_Search_fa fa-search fa-2x co'))

		WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Date of Birth_birthDate'),
				'value', testBirthYear, 10)
	}
}