package com.econsys.qa.steps.pdplus
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.DateUtils
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class PDPlusSearch
{

	@Given("HR manager navigated to the PD+ search page")
	public void hr_manager_navigated_to_the_PD_search_page()
	{
		// navigation order matters here
		String currentPageTitle = WebUI.getWindowTitle()
		if (currentPageTitle.contains("FedHR: Position Description"))
		{
			WebUI.click(findTestObject('FedHR/Page_FedHR Position Description/a_Done'))
		}
		if (currentPageTitle.contains("FedHR: Standard Position")
		|| currentPageTitle.contains("FedHR: Individual Position"))
		{
			WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))
			WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)
			WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))
		}
		else
		{
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_PD Library'), 10)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_PD Library'))
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)
			WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))
		}
	}

	@Given("he selected {string} positions from the library")
	public void he_selected_positions_from_the_library(String string)
	{
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR PD Library/select_PositionLibrary'), string, false)
	}

	@Given("provided Position Title search filter value {string}")
	public void provided_Position_Title_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Title_positionT'), string)
	}

	@When("he searches for the positions")
	public void he_searches_for_the_positions()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))
	}

	@Then("the search results will include position record with the Position Title filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Position_Title_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Position_Title_filter_value(string)
	}

	@Given("provided PD Number search filter value {string}")
	public void provided_PD_Number_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Number_position'), string)
	}

	@Then("the search results will include position record with the PD Number filter value {string}")
	public void the_search_results_will_include_position_record_with_the_PD_Number_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_PD_Number_filter_value(string)
	}

	@Given("provided Pay Plan search filter value {string}")
	public void provided_Pay_Plan_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Pay Plan_payPlan'), string)
	}

	@Then("the search results will include position record with the Pay Plan filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Pay_Plan_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Pay_Plan_filter_value(string)
	}

	@Given("provided Series search filter value {string}")
	public void provided_Series_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_occSeriesCode_Field'), string)
	}

	@Then("the search results will include position record with the Series filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Series_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Series_filter_value(string)
	}

	@Given("provided Grade search filter value {string}")
	public void provided_Grade_search_filter_value(String string)
	{
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_SelectLowerGrade'), string, true)
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_SelectHigherGrade'), string, true)
	}

	@Then("the search results will include position record with the Grade filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Grade_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Grade_filter_value(string)
	}

	@Given("provided Organization Name search filter value {string}")
	public void provided_Organization_Name_search_filter_value(String string)
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/input_Org_Lookup_Button'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/input_Org_Lookup_Button'))
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Organizations_filterKeyword'), string)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/input_Organizations_Search'))
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/a_Select_Org', [('org') : string]))
	}

	@Then("the search results will include position record with the Organization Name filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Organization_Name_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Organization_Name_filter_value(string)
	}

	@Given("provided Status search filter value {string}")
	public void provided_Status_search_filter_value(String string)
	{
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR PD Library/select_Status_ActiveInactive'), string, true)
	}

	@Then("the search results will include position record with the Status filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Status_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Status_filter_value(string)
	}

	@Given("provided PD Text search filter value {string}")
	public void provided_PD_Text_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_PD Text_pd'), string)
	}

	@Then("the search results will include position record with the PD Text filter value {string}")
	public void the_search_results_will_include_position_record_with_the_PD_Text_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_PD_Text_filter_value(string)
	}

	@Given("provided Incumbent search filter value {string}")
	public void provided_Incumbent_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Incumbent_incumbent'), string)
	}

	@Then("the search results will include position record with the Incumbent filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Incumbent_filter_value(String string)
	{
		// Searching by 'incumbent' returns only individual positions
		the_search_results_will_include_individual_position_record_with_the_Incumbent_filter_value(string)
	}

	@Given("provided Last Modified After Date search filter value {string}")
	public void provided_Last_Modified_After_Date_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/input_Last Modified After'), string)
	}

	@Then("the search results will include position record with the Last Modified After Date filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Last_Modified_After_Date_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Last_Modified_After_Date_filter_value(string)
	}

	@Given("provided Until search filter value {string}")
	public void provided_Until_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/input_Until_toModifiedDate'), string)
	}

	@Then("the search results will include position record with the Until filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Until_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Until_filter_value(string)
	}

	@Given("provided Group \\(POI) search filter value {string}")
	public void provided_Group_POI_search_filter_value(String string)
	{
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR PD Library/select_POI'), string, false)
	}

	@Then("the search results will include position record with the Group \\(POI) filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Group_POI_filter_value(String string)
	{
		the_search_results_will_include_standard_position_record_with_the_Group_POI_filter_value(string)
	}

	@Given("provided Budget Number search filter value {string}")
	public void provided_Budget_Number_search_filter_value(String string)
	{
		WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Budget Number_Field'), string)
	}

	@Then("the search results will include position record with the Budget Number filter value {string}")
	public void the_search_results_will_include_position_record_with_the_Budget_Number_filter_value(String string)
	{
		// Searching by 'budget number' returns only individual positions
		the_search_results_will_include_individual_position_record_with_the_Budget_Number_filter_value(string)
	}

	@Then("the search results will include standard position record with the Position Title filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Position_Title_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		String posTitleInRecord = WebUI.getAttribute(findTestObject('FedHR/Page_FedHR Standard Position/input_Position Title_positionTitle'), 'value')
		assert posTitleInRecord.equalsIgnoreCase(string)
	}

	@Then("the search results will include standard position record with the PD Number filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_PD_Number_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Standard Position/input_StandardMaster Position'), 'value',
				string, 10)
	}

	@Then("the search results will include standard position record with the Pay Plan filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Pay_Plan_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Pay Plan'), string, false, 10)
	}

	@Then("the search results will include standard position record with the Series filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Series_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Standard Position/span_occSeriesCodeText'), string)
	}

	@Then("the search results will include standard position record with the Grade filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Grade_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Grade'), string, false, 10)
	}

	@Then("the search results will include standard position record with the Organization Name filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Organization_Name_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		String orgInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/div_Organization'))
		assert orgInRecord.contains(string)
	}

	@Then("the search results will include standard position record with the Status filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Status_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Status'), string, false, 10)
	}

	@Then("the search results will include standard position record with the PD Text filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_PD_Text_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Position Description'))
		String pdTextInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Position Description/body_Position Description'))
		assert pdTextInRecord.contains(string)
	}

	@Then("the search results will include standard position record with the Last Modified After Date filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Last_Modified_After_Date_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		String lastModifiedDateInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Standard Position/span_Last Modified Date'))
		assert DateUtils.compareDates(string, lastModifiedDateInRecord) < 1
	}

	@Then("the search results will include standard position record with the Until filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Until_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		String untilDateInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Standard Position/span_Last Modified Date'))
		assert DateUtils.compareDates(string, untilDateInRecord) > -1
	}

	@Then("the search results will include standard position record with the Group \\(POI) filter value {string}")
	public void the_search_results_will_include_standard_position_record_with_the_Group_POI_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))
		WebUI.verifyOptionSelectedByLabel(findTestObject("FedHR/Page_FedHR Standard Position/select_POI"), string, false, 10)
	}

	@Then("the search results will include individual position record with the Position Title filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Position_Title_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String posTitleInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_positionTitleText'))
		assert posTitleInRecord.equalsIgnoreCase(string)
	}

	@Then("the search results will include individual position record with the PD Number filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_PD_Number_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String std = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_stdPositionNumberText'))
		String ind = WebUI.getAttribute(findTestObject('FedHR/Page_FedHR Individual Position/input_indPositionNumber'),
				'value')
		String pdNumInRecord = std + '-' + ind
		assert pdNumInRecord.equalsIgnoreCase(string)
	}

	@Then("the search results will include individual position record with the Pay Plan filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Pay_Plan_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Individual Position/span_payPlanText'), string)
	}

	@Then("the search results will include individual position record with the Series filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Series_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Individual Position/span_occSeriesCodeText'), string)
	}

	@Then("the search results will include individual position record with the Grade filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Grade_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Individual Position/span_gradeText'), string)
	}

	@Then("the search results will include individual position record with the Organization Name filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Organization_Name_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String orgInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/div_Organization'))
		assert orgInRecord.contains(string)
	}

	@Then("the search results will include individual position record with the Status filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Status_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Individual Position/select_Status'), string, false, 10)
	}

	@Then("the search results will include individual position record with the PD Text filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_PD_Text_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Individual Position/a_Position Description'))
		String pdTextInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Position Description/body_Position Description'))
		assert pdTextInRecord.contains(string)
	}

	@Then("the search results will include individual position record with the Incumbent filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Incumbent_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String incumbentInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_incumbentText'))
		assert incumbentInRecord.contains(string)
	}

	@Then("the search results will include individual position record with the Last Modified After Date filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Last_Modified_After_Date_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String lastModifiedDateInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_Last Modified Date'))
		assert DateUtils.compareDates(string, lastModifiedDateInRecord) < 1
	}

	@Then("the search results will include individual position record with the Until filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Until_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		String untilDateInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_Last Modified Date'))
		assert DateUtils.compareDates(string, untilDateInRecord) > -1
	}

	@Then("the search results will include individual position record with the Group \\(POI) filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Group_POI_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Individual Position/span_POI'), string)
	}

	@Then("the search results will include individual position record with the Budget Number filter value {string}")
	public void the_search_results_will_include_individual_position_record_with_the_Budget_Number_filter_value(String string) {
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)
		WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))
		WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Individual Position/input_pinBudgetNumber'), 'value',
				string, 10)
	}
}