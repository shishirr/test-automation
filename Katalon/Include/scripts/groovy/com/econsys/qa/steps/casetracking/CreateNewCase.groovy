package com.econsys.qa.steps.casetracking
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

/**
 * The step definitions for 'Create New Case' feature.
 */
class CreateNewCase
{

	@Given("HR user is on the FedHR HR homepage")
	def HR_user_is_on_the_FedHR_HR_homepage()
	{
		boolean isHomePage = WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Home/a_Home'))
		assert isHomePage
	}

	@Given("he navigates to the {string} page")
	def he_navigates_to_the_page(String string)
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_New Case'))
		WebUI.delay(1)
		String pageTitle = WebUI.getWindowTitle()
		assert pageTitle.contains((CharSequence) string)
	}

	@Given("fills out all the required fields")
	def fills_out_all_the_required_fields()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), 2)
		WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), '1')
		WebUI.delay(1)
		WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), '1')
		WebUI.delay(1)
		WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), '1')
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_hr'), 5)
		WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_hr'), 'Comments from HR')
		WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 'last')
		WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Search'))
		WebUI.delay(2)
		WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Select'))
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 10)
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 'N', true)
	}

	@When("he creates the case")
	def he_creates_the_case()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Next'))
	}

	@Then("a case should be created successfully")
	def a_case_should_be_created_successfully()
	{
		WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/fieldset_Case Tracking Informa'), 10)
	}
}