package com.econsys.qa.steps.vacancyannouncement
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.TestObjUtils
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable


public class CreateRRLinkedVA
{
	private String templateName = 'Blank VA Template', recruitReqNum = ''
	private int expectedNumOfQuestions, actualNumOfQuestions

	@Given("a vacancy announcement template named {string}")
	public void a_vacancy_announcement_template_named(String string)
	{
		// empty input check
		if(string?.trim()?.isEmpty()) {
			string = templateName
		} else {
			templateName = string
		}

		// Check whether a 'Blank VA Template' is present
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'), 3)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'))
		TestObject to = findTestObject('FedHR/Page_FedHR Vacancy Announcement - M/a_Blank VA Template')
		if (WebUI.waitForElementPresent(to, 2))
		{
			WebUI.scrollToElement(to, 3)
			TestObjUtils.highlightElement(to)
			WebUI.delay(3)
			KeywordUtil.markPassed('A blank VA template already exists! Skipping a blank template creation process...')
		} else {
			WebUI.callTestCase(findTestCase('Test Helper Tasks/Create a blank VA template'), [:])
		}
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Home'), 3)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
	}

	@Given("a recruitment request for a position with a questionnaire")
	public void a_recruitment_request_for_a_position_with_a_questionnaire()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Recruitment'))
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'), 1)
		WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'))
		if (WebUI.waitForElementNotPresent(findTestObject('FedHR/Page_FedHR Recruitment/li_Applicant Questionnaire'), 1)) {

			// TODO need to be completed after KAT-59

		} else {
			KeywordUtil.markPassed('Current recruitment request has an Applicant Questionnaire!')
		}
	}

	@Given("a Hiring Specialist opened a recruitment request with a questionnaire")
	public void a_Hiring_Specialist_opened_a_recruitment_request_with_a_questionnaire()
	{
		// Open the latest recruitment request
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Recruitment'))
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'), 1)
		WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'))
		recruitReqNum = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/span_RequestNumber'))

		// Take a look at the applicant questionnaire and note the number of the questions
		WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/li_Applicant Questionnaire'))
		List l = TestObjUtils.getHtmlTableRows(findTestObject('FedHR/Page_FedHR Applicant Questionnaire/table_Applicant Questionnaire'),
				'TBODY')
		expectedNumOfQuestions = l.size()
	}

	@Given("chose to create a vacancy announcement against the recruitment request")
	public void chose_to_create_a_vacancy_announcement_against_the_recruitment_request()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Applicant Questionnaire/a_RecruitmentRequestNum'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Vacancy Announcements'))
		WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/a_Create Announcement'))
	}

	@Given("provided a vacancy announcement template named {string}")
	public void provided_a_vacancy_announcement_template_named(String string)
	{
		// empty input check
		if(string?.trim()?.isEmpty()) {
			string = templateName
		} else {
			templateName = string
		}

		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Blank VA Template'), string,
				false)
	}

	@When("the vacancy announcement is created")
	public void the_vacancy_announcement_is_created()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))
		WebUI.delay(2)
	}

	@Then("a new RR linked draft vacancy announcement will be shown")
	public void a_new_draft_vacancy_announcement_will_be_shown()
	{
		assert WebUI.getText(findTestObject('FedHR/Page_Job Announcement/h2_Announcement')).contains('DRAFT')
		assert WebUI.getText(findTestObject('FedHR/Page_Job Announcement/h3_Recruitment Request')).contains(recruitReqNum)
	}

	@Then("announcement will include the questionnaire from the recrutiment request")
	public void announcement_will_include_the_questionnaire_from_the_recrutiment_request()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 1)
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'View applicant questionnaire',
				false)
		WebUI.switchToWindowTitle('FedHR: Vacancy Announcement - Applicant Questionnaire')
		WebUI.delay(2)
		List l = TestObjUtils.getHtmlTableRows(findTestObject('FedHR/Page_FedHR Vacancy Announcement - Applicant Questionnaire/table_Applicant Questionnaire'),
				'TBODY')
		actualNumOfQuestions = l.size()
		assert expectedNumOfQuestions == actualNumOfQuestions
	}
}
