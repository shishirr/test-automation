package com.econsys.qa.steps.report
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.findWebElement

import org.openqa.selenium.WebElement

import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.PendingException
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class ManageReportAccess
{
	private String reportRole = 'Administrator', reportName = 'Audit Log - Logins'

	@Given("HR user opened the page to manage report access")
	public void hr_user_opened_the_page_to_manage_report_access()
	{
		try
		{
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'), 1)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'))
		} catch (StepFailedException sfe)
		{
			KeywordUtil.logInfo("Current role can not manage report access! Check role privilege.")
			throw sfe
		}
	}

	@Given("the user chose {string} role for report access modification")
	public void the_user_chose_role_for_report_access_modification(String string)
	{
		// if (null == string || string.trim().isEmpty()) {
		if(string?.trim()?.isEmpty()) {
			string = reportRole // default role
		} else {
			reportRole = string
		}
		WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Manage Report Access/select_Manage Report Access Role'),
				reportRole, false)
	}

	@When("the role is enabled with a {string} report")
	public void the_role_is_enabled_with_a_report(String string)
	{
		// if (null == string || string.trim().isEmpty()) {
		if(string?.trim()?.isEmpty()) {
			string = reportName // default report
		} else {
			reportName = string
		}
		TestObject to = findTestObject('FedHR/Page_FedHR Manage Report Access/input_((reportName))_rpt_chkbox', ['reportName' : reportName])
		WebUI.scrollToElement(to, 1)
		if (findWebElement(to).isSelected()) {
			KeywordUtil.logInfo("The report is already enabled for the ${reportRole} role!")
		} else {
			WebUI.check(to)
		}
		WebUI.click(findTestObject('FedHR/Page_FedHR Manage Report Access/input_Save_button'))
		WebUI.delay(1)
	}

	@Then("the system will notify the user of report access update")
	public void the_system_will_notify_the_user_of_report_access_update()
	{
		assert WebUI.getAlertText().contains("Updated successfully!")
		WebUI.delay(1)
		WebUI.acceptAlert()
	}

	@Then("the report can be accessed for the selected role")
	public void the_report_can_be_accessed_for_the_selected_role()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Reports'), 1)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Reports'))
		TestObject to = findTestObject('FedHR/Page_FedHR Reports/a_((reportName))', ['reportName' : reportName])
		WebUI.scrollToElement(to, 1)
		WebUI.click(to)
		WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Reports/button_Download Report'), 1)
	}

	@When("the role is disabled with a {string} report")
	public void the_role_is_disabled_with_a_report(String string)
	{
		// if (null == string || string.trim().isEmpty()) {
		if(string?.trim()?.isEmpty()) {
			string = reportName // default report
		} else {
			reportName = string
		}
		TestObject to = findTestObject('FedHR/Page_FedHR Manage Report Access/input_((reportName))_rpt_chkbox', ['reportName' : string])
		WebUI.scrollToElement(to, 1)
		if (!findWebElement(to).isSelected()) {
			KeywordUtil.logInfo("The report is already disabled for the ${reportRole} role!")
		} else {
			WebUI.uncheck(to)
		}
		WebUI.click(findTestObject('FedHR/Page_FedHR Manage Report Access/input_Save_button'))
		WebUI.delay(1)
	}

	@Then("the report can not be accessed for the selected role")
	public void the_report_can_not_be_accessed_for_the_selected_role()
	{
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Reports'), 1)
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Reports'))
		TestObject to = findTestObject('FedHR/Page_FedHR Reports/a_((reportName))', ['reportName' : reportName])
		WebUI.verifyElementNotPresent(to, 1)
	}
}