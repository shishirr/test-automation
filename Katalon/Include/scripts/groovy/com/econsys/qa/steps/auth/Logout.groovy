package com.econsys.qa.steps.auth
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.List

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Logout
{
	Login loginSteps = new Login()
	List<String> validFedHRAuthPage = Arrays.asList('FedHR: Login', 'FedHR Navigator: Logged Out')

	@Given("a user was logged into the FedHR")
	public void a_user_was_logged_into_the_FedHR()
	{
		try
		{
			assert !validFedHRAuthPage.contains(WebUI.getWindowTitle())
		}
		catch (AssertionError err)
		{
			loginSteps.a_user_navigated_to_the_FedHR_login_page_for_an_agency()
			loginSteps.the_user_provided_a_valid_username_password_combo_for_the_agency()
			loginSteps.the_user_logs_in()
		}
	}

	@When("the user attemps to log out")
	public void the_user_attemps_to_log_out()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Logged Out/a_Logout'))
	}

	@Then("the system wil log the user out of FedHR")
	public void the_system_wil_log_the_user_out_of_FedHR()
	{
		WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Logged Out/h2_Goodbye'))
	}
}