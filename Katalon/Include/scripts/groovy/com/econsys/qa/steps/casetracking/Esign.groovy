package com.econsys.qa.steps.casetracking
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.ActionUtils
import com.econsys.qa.utils.TestObjUtils
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable as GlobalVariable


public class Esign
{
	int count = 0
	String formToUse = 'SF 52'
	String xpath_esign = "//input[@type='button' and (@value='e-Sign' or @value='Erase e-Sign')]"
	String currEsignXpath = ""
	String signerCredentials = findTestData('Data Files/FedHR user accounts').getValue(GlobalVariable.agency, 1)
	List<TestObject> testObjList = new ArrayList<TestObject>()
	AddFormsToCase routine_AddForm = new AddFormsToCase()
	TestObject caseWithFormsPackage

	@Given("A non-recruitment case with an e-signable form {string}")
	public void a_non_recruitment_case_with_an_e_signable_form(String string)
	{
		if (!string.isEmpty()) formToUse = string
		else KeywordUtil.logInfo("No form name provided! Defaulting to the form 'SF 52'...")

		// check whether a case with forms package (non-recruitment) owned by me exists
		// if there is none create one
		routine_AddForm.a_case_in_FedHR_with_a_form_package()
		caseWithFormsPackage = TestObjUtils.getMyTestObject("my-case-locator", "xpath", routine_AddForm.xpathCaseHasForms)

		// now open the first case with forms package
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		WebUI.scrollToElement(caseWithFormsPackage, 2)
		WebUI.click(caseWithFormsPackage)

		if(!WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Case Details/a_(Form Number)_FormsFolder', [('formNumber') : formToUse]), 1))
		{
			routine_AddForm.opened_the_form_folder_contents_for_the_case()
			routine_AddForm.the_user_adds_a_form(formToUse)
			routine_AddForm.the_user_should_be_able_to_see_the_new_form_added_to_the_case()
		}
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
	}

	@Given("HR user opened an e-signable form")
	public void hr_user_opened_an_e_signable_form()
	{
		WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))
		WebUI.scrollToElement(caseWithFormsPackage, 2)
		WebUI.click(caseWithFormsPackage)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Details/a_(Form Number)_FormsFolder', [('formNumber') : formToUse]))

		count = ActionUtils.execXpath("count($xpath_esign)", "NUMBER_TYPE", null)
		assert count > 0 : "No e-signable item exists on this form!"
	}

	@When("he e-signs all applicable items on the form")
	public void he_e_signs_all_applicable_items_on_the_form()
	{
		testObjList.clear()
		for (int i=1; i<=count; i++) {
			currEsignXpath = "($xpath_esign)[$i]"
			TestObject to = TestObjUtils.getMyTestObject("button-0$i", "xpath", currEsignXpath)
			WebUI.delay(1)
			WebUI.scrollToElement(to, 2)

			if (WebUI.getAttribute(to, 'value') == 'e-Sign')
			{
				testObjList.add(to)
				WebUI.delay(1)
				WebUI.click(to)
				WebUI.setText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/input_Password_Field'), signerCredentials)
				WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/button_Submit'))
			}
		}
	}

	@Then("the e-signature section should include the HR user's name")
	public void the_e_signature_section_should_include_the_HR_user_s_name()
	{
		/* using Iterable.forEach() with Groovy Closure */

		testObjList.forEach({ to ->
			WebUI.delay(1)
			WebUI.scrollToElement(to, 2)
			String signedBy = WebUI.getText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/span_SignatureText', [('esignButton') : currEsignXpath]))

			assert signedBy.startsWith('Electronically signed by:') && signedBy.contains(signerCredentials.substring(4))
		})
	}

	@Then("user should be given an option to erase that e-sign")
	public void user_should_be_given_an_option_to_erase_that_e_sign()
	{
		/* using enhanced for loop */

		for (TestObject to : testObjList) {
			WebUI.scrollToElement(to, 2)
			WebUI.verifyElementAttributeValue(to, 'value', 'Erase e-Sign', 1)
		}
	}

	@When("he erase e-signs from applicable items on the form")
	public void he_erase_e_signs_from_applicable_items_on_the_form()
	{
		testObjList.clear()
		for (int i=1; i<=count; i++) {
			currEsignXpath = "($xpath_esign)[$i]"
			TestObject to = TestObjUtils.getMyTestObject("button-0$i", "xpath", currEsignXpath)
			WebUI.delay(1)
			WebUI.scrollToElement(to, 2)

			if (WebUI.getAttribute(to, 'value') == 'Erase e-Sign')
			{
				testObjList.add(to)
				WebUI.delay(1)
				WebUI.click(to)
				WebUI.setText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/input_Password_Field'), signerCredentials)
				WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/button_Submit'))
				WebUI.delay(1)
				WebUI.scrollToElement(to, 2)
				WebUI.verifyElementAttributeValue(to, 'value', 'e-Sign', 1)
				String signedBy = WebUI.getText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/span_SignatureText', [('esignButton') : currEsignXpath]))

				assert signedBy.equals("")
			}
		}
	}

	@Then("the e-signature section should be empty")
	public void the_e_signature_section_should_be_empty()
	{
		/* using an Iterator explicitly */

		Iterator<TestObject> iterator = testObjList.iterator()
		//ListIterator<TestObject> iterator = testObjList.listIterator()

		while (iterator.hasNext())
		{
			TestObject to = iterator.next()
			WebUI.delay(1)
			WebUI.scrollToElement(to, 2)
			String signedBy = WebUI.getText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/span_SignatureText', [('esignButton') : currEsignXpath]))

			assert signedBy.equals("")
		}
	}

	@Then("user should be given an option to e-sign it again")
	public void user_should_be_given_an_option_to_e_sign_it_again()
	{
		/* using Stream.forEach() with Groovy Closure */

		testObjList.stream().forEachOrdered({ to ->
			WebUI.scrollToElement(to, 2)
			WebUI.verifyElementAttributeValue(to, 'value', 'e-Sign', 1)
		})
	}
}