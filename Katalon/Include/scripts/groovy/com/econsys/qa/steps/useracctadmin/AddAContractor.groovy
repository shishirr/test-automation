package com.econsys.qa.steps.useracctadmin
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class AddAContractor {

	String identifier = new Random().nextLong().abs().toString().substring(0, 4)
	String lastName = 'LAST' + identifier
	String firstName = 'FIRST' + identifier
	String email =  'tester' + identifier + '@econsys.com'
	String userName = 'tester' + identifier
	String pass = 'UNITEDstates1776!&&^'
	String fullName = lastName + ', ' + firstName

	@Given("HR Manager navigated to {string} page")
	public void hr_Manager_navigated_to_page(String string) {
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/a_User Account Administration'))
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Add New Contractor'), 10)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Add New Contractor'))
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/input_YesRadioButton'))
		assert WebUI.getWindowTitle().trim().contains(string) : "Navigation failed!"
	}

	@And("filled out all the required fields")
	public void filled_out_all_the_required_fields() {
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_lastName_User'), lastName)
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_First Name_User'), firstName)
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_User E-Mail_userEmail'),
				email)
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Login username'), userName)
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_One time password'), pass)
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_(Deactivate user  s access on)_suspenseDate'),
				'next 6 month')
		WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR Create New User Account/select_Group'), 0)
		WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create New User Account/select_role_Administrator'), 10)
		WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create New User Account/select_role_Administrator'),
				'Y', true)
	}

	@When("the HR Manager saves the user info")
	public void the_HR_Manager_saves_the_case() {
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Save'), 10)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Save'))
	}

	@Then("the new user should be searchable in FedHR")
	public void the_new_user_should_be_searchable_in_FedHR() {
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/a_User Account Administration'))
		WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/input_Search_searchText'), lastName)
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Search'), 10)
		WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Search'))
		WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/LastNameAndFirstName'), 10)
		String compareUsername = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/LastNameAndFirstName'))
		WebUI.verifyMatch(fullName, compareUsername, false)
	}
}