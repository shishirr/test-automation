package com.econsys.qa.hooks

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.CookieUtils
import com.econsys.qa.utils.DBUtils
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.Scenario
import cucumber.api.java.Before
import internal.GlobalVariable

/**
 * Class with <b>@Before</b> hook methods which gets executed before each scenario.<br>
 * Use tag expression and order number to control the hook execution.<br>
 * Note that <u>lower</u> order numbers are run first.
 * @author MRahman
 */
public class BeforeScenarioHook
{
	/**
	 * Checks browser state before every scenario.
	 * @param scenario
	 */
	@Before(order = 1)
	public void beforeEveryScenarios(Scenario scenario)
	{
		WebUI.callTestCase(findTestCase('Test Helper Tasks/Init web browser'), [:], FailureHandling.STOP_ON_FAILURE)

		//		Runtime.getRuntime().addShutdownHook(new Thread()
		//				{
		//					public void run()
		//					{
		//						DriverFactory.getWebDriver().quit()
		//					}
		//				})
	}

	@Before(value = "not (@Auth or @emp)")
	public void beforeHRScenario()
	{
		// we need to be on the HR home page before a feature scenario starts
		if(WebUI.getWindowTitle().contains("FedHR: Home"))
		{
			// check if we have the correct HR role already set
			if(!WebUI.getText(findTestObject('FedHR/Page_FedHR Home/p_Role HR')).contains('Administrator'))
			{
				WebUI.callTestCase(findTestCase('Test Cases/Test Helper Tasks/Choose a user role'), [('isRoleSwitching') : true])
			}
		}
		// we are not on the HR home page
		// consider this as a 'logged out' situation and start login routine
		else
		{
			CookieUtils.deleteAllCookies()
			WebUI.callTestCase(findTestCase('Access Control/Login as Admin'), null)
		}
	}

	@Before(value = "not @Auth and @emp")
	public void beforeEmpScenario()
	{
		// we need to be on the employee home page before a feature scenario starts
		if(WebUI.getWindowTitle().contains("FedHR: Home/EBC"))
		{
			// check if we have the correct employee role already set
			if(!WebUI.getText(findTestObject('FedHR/Page_FedHR HomeEBC/p_Role Employee')).contains('Employee'))
			{
				WebUI.callTestCase(findTestCase('Test Cases/Test Helper Tasks/Choose a user role'), [('isRoleSwitching') : true, ('role') : 'Employee'])
			}
		}
		// we are not on the employee home page
		// consider this as a 'logged out' situation and start login routine
		else
		{
			CookieUtils.deleteAllCookies()
			WebUI.callTestCase(findTestCase('Access Control/Login as Employee'), null)
		}
	}

	@Before("@Auth and @rollback")
	public void beforeLoginScenarioRollback()
	{
		if (!GlobalVariable.siteURL) {
			DBUtils.openDBConnection()
			String user = "'" + findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1) + "'"
			String sql = (new File('Include/scripts/sqls/reset-user.sql')).text
			String stmt = sql.replace(':p_user_name', user)
			assert DBUtils.executeUpdate(stmt) == 1 : "One data row should have had updated!"
			KeywordUtil.logInfo("Login account has been reset!")
			DBUtils.closeDBConnection()
		}
	}

	@Before("@CT and @rollback and @formfolder")
	public void beforeAddFormWithFolderScenario()
	{
	}

	@Before("@CT and @rollback and @noformfolder")
	public void beforeAddFormWithNoFolderScenario()
	{
	}
}
