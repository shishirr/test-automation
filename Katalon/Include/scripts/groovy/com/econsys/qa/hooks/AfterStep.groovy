package com.econsys.qa.hooks

import cucumber.api.Scenario
import cucumber.api.java.AfterStep

public class AfterStepHook
{
	@AfterStep
	public void doSomethingAfterStep(Scenario scenario)
	{
	}
}
