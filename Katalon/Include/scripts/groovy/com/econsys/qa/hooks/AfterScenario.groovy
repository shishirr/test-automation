package com.econsys.qa.hooks

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.nio.file.Files

import com.econsys.qa.cucumber.TestData
import com.econsys.qa.utils.DBUtils
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.Scenario
import cucumber.api.java.After
import internal.GlobalVariable as GlobalVariable

/**
 * Class with <b>@After</b> hook methods which gets executed after each scenario.<br>
 * Use tag expression and order number to control the hook execution.<br>
 * Note that <u>higher</u> order numbers are run first.
 * @author MRahman
 */
public class AfterScenarioHook {
	/**
	 * Checks scenario status after a test and add screenshot to the report if it's failed.
	 * @param scenario
	 */
	@After
	public void afterEveryScenarios(Scenario scenario) {
		if (scenario.isFailed() /* && !TestData.instance.prop.getProperty("browserType").contains('HEADLESS') */) {
			byte[] screenshot
			try {
				String file = WebUI.takeScreenshot()
				screenshot = Files.readAllBytes(new File(file).toPath())
			}
			catch (Exception somePlatformsDontSupportScreenshots) {
				scenario.write(somePlatformsDontSupportScreenshots.getMessage())
			}
			scenario.embed(screenshot, "image/png")	// embedding is reporter-dependent
			// try html or json reporter
		}
	}

	@After("@Auth and @login")
	public void afterLoginScenario() {
		WebUI.callTestCase(findTestCase('Test Helper Tasks/Init login user'), [('role'): 'Administrator'])
	}

	@After("@Auth and @rollback")
	public void afterLoginScenarioRollback() {
		if (!GlobalVariable.siteURL) {
			DBUtils.openDBConnection()
			String user = "'" + findTestData('FedHR user accounts').getValue(GlobalVariable.agency, 1) + "'"
			String sql = (new File('Include/scripts/sqls/reset-user.sql')).text
			String stmt = sql.replace(':p_user_name', user)
			assert DBUtils.executeUpdate(stmt) == 1 : "One data row should have updated!"
			KeywordUtil.logInfo("Login account has been reset!")
			DBUtils.closeDBConnection()
		}
	}

	@After(value = "not (@Auth or @emp)", order = 1)
	public void afterHRScenario() {
		// check if we are already on the HR home page
		if(!WebUI.getWindowTitle().contains("FedHR: Home"))
		{
			// if on a form, exit using 'Previous' button
			if(WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2) == true)
			{
				WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2)
				WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))
			}

			// get to the HR home page
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Home'), 2)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
		}
	}

	@After(value = "@emp and not @Auth")
	public void afterEmpScenario()
	{
		// check if we are already on the employee home page
		if(!WebUI.getWindowTitle().contains("FedHR: Home/EBC"))
		{
			// if on a form, exit using 'Previous' button
			if(WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2) == true)
			{
				WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2)
				WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))
			}

			// get to the 'Home' page
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'), 2)
			WebUI.click(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'))
		}
	}

	@After("@CT and @rollback and @formfolder")
	public void afterAddFormWithFolderScenario()
	{
	}

	@After("@CT and @rollback and @noformfolder")
	public void afterAddFormWithNoFolderScenario()
	{
	}

	@After(value = "@VA", order = 9)
	public void afterVAScenario()
	{
		String title = WebUI.getWindowTitle()
		if (title.equals('Job Announcement Preview')) WebUI.closeWindowTitle(title)
		if (title.equals('USAJOBS - Job Announcement')) WebUI.closeWindowTitle(title)
		if (title.equals('FedHR: Vacancy Announcement - Applicant Questionnaire')) WebUI.closeWindowTitle(title)
		WebUI.switchToWindowTitle('Job Announcement')
		WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'), 10)
		WebUI.click(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'))
	}
}
