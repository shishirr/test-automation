// TIP: Edit this file in VS Code
// all 'arguments' var will be replaced at runtime by the caller method

const style = `
.highlightElement {
	animation: highlightElement 0.2s 5;
}
@keyframes highlightElement {
	from { outline: solid 5px #FC5185; color: red;	}
	to { outline: solid 5px #FC5185; color: yellow;	}
}
`
let styleSheet = document.createElement("style")
styleSheet.type = "text/css"
styleSheet.innerText = style
document.head.appendChild(styleSheet)

for (let element of arguments) {
	console.log("Element to be highlighted:" + element)
	element.classList.add('highlightElement')
}
