@UAA
Feature: Add A Contractor
  Contractors are short or long term hires that are assigned to a specific group
  based on their role. They are not fulltime employees but can perform the 
  task just like their fulltime colleagues.

  @positive
  Scenario: FedHR Manager creates a new contractor user account
    Given HR Manager navigated to "Create New User Account" page
    And filled out all the required fields
    When the HR Manager saves the user info
    Then the new user should be searchable in FedHR