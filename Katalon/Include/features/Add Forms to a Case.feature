@CT @regression
Feature: Add A Form To an Existing Case
  A case may not have the necessary form(s) needed at the case creation.
  Priviledged HR users are able to add new forms to an existing case on-demand.

  @positive @formfolder
  Scenario: HR user should be able to add a form to an existing case
    with forms package

    Given a case in FedHR with a form package
    And HR user opened an existing case with form package
    And opened the form folder contents for the case
    When the user adds a form "SF 3106A"
    Then the user should be able to see the new form added to the case

  @positive @noformfolder
  Scenario: HR user should be able to add a form to an existing case
    without forms package

    Given a case in FedHR without any form package
    And HR user opened an existing case without form package
    And opened the addable form list for the case type/activity pair
    When the user adds a form "" from the form list
    #NOTE: form list may or may not be configured for the case/activity pair
    Then the user should be able to see the new form added to the case
