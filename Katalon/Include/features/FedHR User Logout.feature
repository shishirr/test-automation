@Auth @regression
Feature: FedHR User Logout
  By logging out, users can end their authorized browser session of the FedHR.

  @positive @logout
  Scenario: Users can log out of FedHR
    Given a user was logged into the FedHR
    When the user attemps to log out
    Then the system wil log the user out of FedHR