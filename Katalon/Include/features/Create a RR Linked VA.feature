@VA @regression
Feature: Create a Recruitment Request Linked Vacancy Announcement
  Vacancy Announcement allows a Hiring Specialist prepare job information for
  position vacancies in an agency. In order to screen applicants using a
  questionnaire, a job announcement is needed to be created from a recruitment
  request. Once prepared, the draft vacancy announcements are then posted on
  USAJobs.gov hiring site.

  Background: Feature requires a VA template and a recruitment request
    Given a vacancy announcement template named "Blank VA Template"
    And a recruitment request for a position with a questionnaire

  Scenario: Hiring Specialist is able to create a draft vacancy announcement
    with an applicant questionnaire (recruitment request linked VA)

    Given a Hiring Specialist opened a recruitment request with a questionnaire
    And chose to create a vacancy announcement against the recruitment request
    And provided a vacancy announcement template named "Blank VA Template"
    When the vacancy announcement is created
    Then a new RR linked draft vacancy announcement will be shown
    And announcement will include the questionnaire from the recrutiment request
