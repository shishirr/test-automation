@VA @regression
Feature: View a VA in USAJobs
  Using this functionality, HR user can jump to a vacancy announcement posted
  on USAJobs directly from FedHR.

  Scenario: HR user should be able to view a posted announcement on USAJobs
    Given HR user posted a new vacancy announcement
    When the HR user views the USAJobs posting from FedHR
    Then the HR user should be able to see the job posting on USAJOBS

  Scenario: HR user should be able to view an amended announcement on USAJobs
    Given HR user amended a posted vacancy announcement
    When the HR user views the USAJobs posting from FedHR
    Then the HR user should be able to see the job posting on USAJOBS

  Scenario: HR should not be able to view a canceled announcement on USAJobs
    Given HR user posted a new vacancy announcement
    When the HR user cancels the announcement
    Then the HR user will not be able to see the job posting on USAJOBS