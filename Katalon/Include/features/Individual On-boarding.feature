@Run @CaseTracking
Feature: Individual On-boarding
         HR specialist use individual on-boarding feature to create a new employee record.

  @Valid
  Scenario: Manager can create an on-boarding case using new employee information
    Given a manager opened Individual On-Boarding from the homepage
    And the manager filled in all the required fields on Employee Setup view
    And all other required fields are filled on the On-boarding view
    When the manager saves the case
    Then a new On-boarding case should be created under On-boarding case tracking

  @Validation
  Scenario: The SSN needs to be unique in order to add an employee record
    Given a manager opened Individual On-Boarding from the homepage
    And the manager filled in all the required fields on Employee setup view
    But the manager used a duplicate SSN
    When the manager proceeds to save the case
    Then the user would be notified of a duplicate SSN error

  @Validation
  Scenario Outline: A manager cannot create an onboarding case with any missing required data
    Given a manager opened Individual On-Boarding from the homepage
    And the manager filled in all the required fields on Employee setup view
    But left the "<datafield>" field blank
    When the manager proceeds to save the case
    Then the user would be notified of a missing data error

    Examples: 
      | datafield              |
      | Social Security Number |
      | DOB                    |
