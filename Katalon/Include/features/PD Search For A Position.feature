@Run @PDPlus
Feature: Search for a position
  PD+ library module stores all type of positions such as Standard and Individual positions, which a HR manager can use for the hiring process.  ​
  PD+ library has various search filters which can be used to find the desired position from the library.

  @Valid
  Scenario Outline: Users can search for all positions using any of the search filters
    Given HR manager navigated to the PD+ search page
    And he selected "All" positions from the library
    And provided <filter> search filter value "<value>"
    When he searches for the positions
    Then the search results will include position record with the <filter> filter value "<value>"

    Examples: 
      | filter                   | value        |
      | Position Title           | cook         |
      | PD Number                |       000225 |
      | Pay Plan                 | NA           |
      | Series                   |         7404 |
      | Grade                    |           08 |
      | Organization Name        | NSA Bethesda |
      | Status                   | Active       |
      | PD Text                  | account      |
      | Incumbent                | Last 1001040 |
      | Last Modified After Date | 04/04/2018   |
      | Until                    | 04/06/2018   |
      | Group (POI)              | CNIC (POI )  |
      | Budget Number            | C-101        |

  @Validation
  Scenario Outline: Users can search for standard positions using any of the applicable search filters
    Given HR manager navigated to the PD+ search page
    And he selected "Standard" positions from the library
    And provided <filter> search filter value "<value>"
    When he searches for the positions
    Then the search results will include standard position record with the <filter> filter value "<value>"

    Examples: 
      | filter                   | value        |
      | Position Title           | cook         |
      | PD Number                |       000225 |
      | Pay Plan                 | NA           |
      | Series                   |         7404 |
      | Grade                    |           08 |
      | Organization Name        | NSA Bethesda |
      | Status                   | Active       |
      | PD Text                  | account      |
      | Last Modified After Date | 04/04/2018   |
      | Until                    | 04/06/2018   |
      | Group (POI)              | CNIC (POI )  |

  @Validation
  Scenario Outline: Users can search for individual positions using any of the search filters
    Given HR manager navigated to the PD+ search page
    And he selected "Individual" positions from the library
    And provided <filter> search filter value "<value>"
    When he searches for the positions
    Then the search results will include individual position record with the <filter> filter value "<value>"

    Examples: 
      | filter                   | value                |
      | Position Title           | recreation assistant |
      | PD Number                | 000012-00000039      |
      | Pay Plan                 | NF                   |
      | Series                   |                 0189 |
      | Grade                    |                   02 |
      | Organization Name        | NSA Annapolis        |
      | Status                   | Active               |
#     | PD Text                  | management           |			
      | Incumbent                | Last 1001040         |
#     | Last Modified After Date | 04/14/2019           |
      | Until                    | 04/16/2019           |
#*    | Group (POI)              | CNIC (POI )					|
      | Budget Number            | C-101                |

#		application defect exists for disabled parameters
#* 	value given and value received are not the same for NDW