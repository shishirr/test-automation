@VA @regression
Feature: Manage Vacancy Email
  Using this feature, HR Manager manages a list of email contacts who they keep 
  notified of any new vacancies by sending the USAJobs URL of a job posting.

  Scenario: HR Manager can add new email addresses to the vacancy email list
    Given HR Manager opened the vacancy email list
    When the manager adds a few email addresses to the list
    Then the email addresses will be added to the notify list
    And the addresses will be offered in the vacancy announcement email notification

  Scenario: HR Manager can remove an email address from the vacancy email list
    Given HR Manager opened the vacancy email list
    When the manager deletes an email address he addded
    Then the deleted email address will no longer exist in the notify list
    And the address will not be offered in the vacancy announcement email notification