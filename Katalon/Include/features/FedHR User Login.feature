@Auth @regression
Feature: FedHR User Login by Username/Password
  FedHR Navigator needs to identify its users before providing access to the
  functionalities allowed for the user. Users can prove their identity by using
  a username/password combination which validates the provided data with the
  user records in the agency.

  @validation @rollback
  Scenario: Users need to reset their passwords for providing wrong passwords
    three times consecutively.

    Given a user navigated to the FedHR login page for an agency
    And the user provided a valid username for the agency
    But the user provided an invalid password for the username
    When the user attemps to log in for 3 times consecutively
    Then the system will notify the user to reset the user password
    And the system will deny the user login

  @validation @ux
  Scenario Outline: Users are required to provide both username and password
    Given a user navigated to the FedHR login page for an agency
    And the user didn't provide the <datafield> for the login combo
    When the user attemps to log in
    Then the system will notify the user of missing <datafield> data

    Examples: 
      | datafield |
      | password  |
      | username  |

  @negative
  Scenario: Users are denied login for providing invalid username
    and passowrd combination.

    Given a user navigated to the FedHR login page for an agency
    And the user provided an invalid username/password combo for the agency
    When the user attemps to log in
    Then the system will deny the user login
    And the system will notify the user of logon denial
    
  @positive @login
  Scenario: Users can log into FedHR by providing valid username and password
    combination.

    Given a user navigated to the FedHR login page for an agency
    And the user provided a valid username/password combo for the agency
    When the user attemps to log in
    Then the system wil allow the user login