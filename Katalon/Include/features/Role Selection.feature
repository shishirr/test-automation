@RoleSelection
Feature: Role Selection
  FedHR role determines what a user with the right login credentials can do in the system from a 
  functionality standpoint. These roles are filtered based on organizational and functionality access.

  @positive
  Scenario Outline: FedHR users needs to select a role in order to be taken to their designated homepage
    Given FedHR user is on the role selection page
    When the user selects <role> role
    Then the user should be taken to the <role> homepage

    Examples: 
      | role          |
      | Administrator |
      | Employee      |