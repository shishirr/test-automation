@CT @regression
Feature: Create a New Case
  HR Manager can create a new case for the employee with the relevant case types
  based on their needs and requirement.

  @positive
  Scenario: HR user can create a case using all the required case information
    Given HR user is on the FedHR HR homepage
    And he navigates to the "New Case" page
    And fills out all the required fields
    When he creates the case
    Then a case should be created successfully