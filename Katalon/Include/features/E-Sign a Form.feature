@CT @regression
Feature: E-sign a form
  HR Manager uses e-signature on a form to approve a request
  or review of a process.

  Background: Only a handful selection of forms can be e-signed
    Given A non-recruitment case with an e-signable form "SF 52"

  @positive
  Scenario: HR user e-signs a form
    Given HR user opened an e-signable form
    When he e-signs all applicable items on the form
    Then the e-signature section should include the HR user's name
    And user should be given an option to erase that e-sign

  @negative
  Scenario: HR user erase e-signs from a form
    Given HR user opened an e-signable form
    When he erase e-signs from applicable items on the form
    Then the e-signature section should be empty
    And user should be given an option to e-sign it again