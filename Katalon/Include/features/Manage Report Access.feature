@Report @regression
Feature: Manage Report Access
  Privileged HR users can enable or disable access to a report for any HR roles
  available in the system.

  Scenario: HR user can enable a report for a role
    Given HR user opened the page to manage report access
    And the user chose "Administrator" role for report access modification
    When the role is enabled with a "Audit Log - Logins" report
    Then the system will notify the user of report access update
    And the report can be accessed for the selected role

  Scenario: HR user can disable a report for a role
    Given HR user opened the page to manage report access
    And the user chose "Administrator" role for report access modification
    When the role is disabled with a "Audit Log - Logins" report
    Then the system will notify the user of report access update
    And the report can not be accessed for the selected role