import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.TestObjUtils
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class SpecificTestListener
{
	String currentTestCaseId = null
	List<String> tcRequiresBlankVATemplate = Arrays.asList(
		'Create a draft VA',
		'Compose and Save a VA',
		'Create Vacancy Announcement',
		'Create a RR Linked VA'
		)

	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def beforeVATestCase(TestCaseContext testCaseContext)
	{
		currentTestCaseId = testCaseContext.getTestCaseId()
		String testCaseName = currentTestCaseId.substring(currentTestCaseId.lastIndexOf('/') + 1)

		if(tcRequiresBlankVATemplate.contains(testCaseName))
		{
			// Check whether a 'Blank VA Template' is present
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'), 3)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'))
			TestObject to = findTestObject('FedHR/Page_FedHR Vacancy Announcement - M/a_Blank VA Template')
			if (WebUI.waitForElementPresent(to, 2))
			{
				WebUI.scrollToElement(to, 3)
				TestObjUtils.highlightElement(to)
				WebUI.delay(3)
				KeywordUtil.markPassed('A blank VA template already exists! Skipping a blank template creation process...')
			} else {
				WebUI.callTestCase(findTestCase('Test Helper Tasks/Create a blank VA template'), [:])
			}
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Home'), 3)
			WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
		}
	}
}