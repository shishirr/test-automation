import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.econsys.qa.utils.CookieUtils
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


/**
 * Defines the common test listener or hooks used in Katalon.
 * Uses {@link com.kms.katalon.core.context.TestCaseContext} and {@link com.kms.katalon.core.context.TestSuiteContext}
 */
public class CommonTestListener
{
	List<String> tcAuthNotApplicable 	= Arrays.asList('/Miscellaneous/', '/Test Helper Tasks/', '/Access Control/')
	List<String> tcRequiresEmpAuth 		= Arrays.asList('/Employee/')
	String roleType = 'hr' // role type needed for most FedHR functionalities

	String currentTestCaseId = null, currentTestSuiteId = null
	boolean isTestSuiteExecuted = false

	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def beforeEveryTestCase(TestCaseContext testCaseContext)
	{
		currentTestCaseId = testCaseContext.getTestCaseId()
		
		// need to figure out the applicable role for the current test case
		tcAuthNotApplicable.each { folder ->
			if(currentTestCaseId.contains(folder)) {
				roleType = 'none'
			}
		}
		for (folder in tcRequiresEmpAuth) {
			roleType = currentTestCaseId.contains(folder) ? 'emp' : roleType
		}

		WebUI.callTestCase(findTestCase('Test Helper Tasks/Init web browser'), [:])
		if (roleType == 'hr')
		{
			// we need to be on the HR home page before a test case starts
			if(WebUI.getWindowTitle().contains("FedHR: Home"))
			{
				// check if we have the correct HR role already set
				if(!WebUI.getText(findTestObject('FedHR/Page_FedHR Home/p_Role HR')).contains('Administrator'))
				{
					WebUI.callTestCase(findTestCase('Test Cases/Test Helper Tasks/Choose a user role'), [('isRoleSwitching') : true])
				}
			}
			// we are not on the HR home page
			// consider this as a 'logged out' situation and start login routine
			else
			{
				CookieUtils.deleteAllCookies()
				WebUI.callTestCase(findTestCase('Access Control/Login as Admin'), null)
			}
		}
		else if(roleType == 'emp')
		{
			// we need to be on the employee home page before a test case starts
			if(WebUI.getWindowTitle().contains("FedHR: Home/EBC"))
			{
				// check if we have the correct employee role already set
				if(!WebUI.getText(findTestObject('FedHR/Page_FedHR HomeEBC/p_Role Employee')).contains('Employee'))
				{
					WebUI.callTestCase(findTestCase('Test Cases/Test Helper Tasks/Choose a user role'), [('isRoleSwitching') : true, ('role') : 'Employee'])
				}
			}
			// we are not on the employee home page
			// consider this as a 'logged out' situation and start login routine
			else
			{
				CookieUtils.deleteAllCookies()
				WebUI.callTestCase(findTestCase('Access Control/Login as Employee'), null)
			}
		}
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def afterEveryTestCase(TestCaseContext testCaseContext)
	{
		if (currentTestSuiteId != null)
		{
			if (roleType == 'hr')
			{
				// check if we are already on the HR home page
				if(!WebUI.getWindowTitle().contains("FedHR: Home"))
				{
					// if on a form, exit using 'Previous' button
					if(WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2) == true)
					{
						WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2)
						WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))
					}

					// get to the HR home page
					WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Home'), 2)
					WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))
				}
			}
			else if(roleType == 'emp')
			{
				// check if we are already on the employee home page
				if(!WebUI.getWindowTitle().contains("FedHR: Home/EBC"))
				{
					// if on a form, exit using 'Previous' button
					if(WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2) == true)
					{
						WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'), 2)
						WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))
					}

					// get to the 'Home' page
					WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'), 2)
					WebUI.click(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'))
				}
			}
		}
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@BeforeTestSuite
	def beforeEveryTestSuite(TestSuiteContext testSuiteContext)
	{
		currentTestSuiteId = testSuiteContext.getTestSuiteId()
	}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def afterEveryTestSuite(TestSuiteContext testSuiteContext)
	{
		String status = testSuiteContext.getStatus()
		isTestSuiteExecuted = status.equalsIgnoreCase('COMPLETE') || status.equalsIgnoreCase('ERROR') ? true : false
	}
}