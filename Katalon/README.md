# KATALON

[![N|Solid](https://fhrnavigator.com/frbweb/images/logo-fedhr-nav.png)](https://www.fedhrnavigator.com)

Katalon is the tool we're using to automate quick regression scripts in FedHR Navigator.

##### Features!
  - GUI based tool
  - Integrates with Git (as you see here)
  - Possible to use it in CI/CD environment.