package com.econsys.qa.cucumber

import com.kms.katalon.core.configuration.RunConfiguration

/**
 * Test data that would be shared between test steps.
 * Only one instance is allowed per test run.
 */
@Singleton(strict = false)
public class TestData
{
	private static final Properties prop = new Properties()
	private TestData()
	{
		prop.setProperty("browserType", RunConfiguration.getExecutionProperties()
				.get("drivers").get("system").get("WebUI").get("browserType"))
		prop.setProperty("isLoggedIntoFedHR", 'false')
	}

	/**
	 * Singleton in Java
	 */
	//	private static volatile TestData INSTANCE
	//	private TestData() {}
	//	public static TestData getInstance() {
	//		if(null == INSTANCE) {
	//			synchronized(this) {	// TestData.class
	//				if(null == INSTANCE) {
	//					INSTANCE = new TestData()
	//				}
	//			}
	//		}
	//		return INSTANCE;
	//	}

}
