package com.econsys.qa.utils

import org.openqa.selenium.Cookie
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.driver.DriverFactory

public class CookieUtils
{

	private static final WebDriver driver = DriverFactory.getWebDriver()

	@Keyword
	public static Set<Cookie> getCookies()
	{
		return driver.manage().getCookies()
	}

	@Keyword
	public static Cookie getCookieNamed(String name)
	{
		return driver.manage().getCookieNamed(name)
	}

	@Keyword
	public static void deleteCookieNamed(String name)
	{
		driver.manage().deleteCookieNamed(name)
	}

	@Keyword
	public static void deleteAllCookies()
	{
		driver.manage().deleteAllCookies()
	}
}
