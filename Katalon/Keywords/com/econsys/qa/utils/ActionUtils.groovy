package com.econsys.qa.utils
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

class ActionUtils
{

	/**
	 * Click an element using Javascript
	 * @param to Katalon test object
	 */
	@Keyword
	public static void clickUsingJS(TestObject to)
	{
		try
		{
			WebElement element = WebUiCommonHelper.findWebElement(to, 10)
			KeywordUtil.logInfo("Clicking element using JS...")
			WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element))
			KeywordUtil.markPassed("Element has been clicked!")
		} catch (WebElementNotFoundException e)
		{
			KeywordUtil.markFailed("Element is not found.")
		} catch (Exception e)
		{
			KeywordUtil.markFailed("Fail to click on the element using JS.")
		}
	}

	/**
	 * Evaluate XPATH expression using javascript.
	 * @param xpath String
	 * @param RESULT_TYPE [ NUMBER_TYPE | STRING_TYPE | BOOLEAN_TYPE ]
	 * @param list any applicable args using <i>Arrays.asList
	 * @return result from xpath
	 */
	@Keyword
	public static Object execXpath(String xpath, String RESULT_TYPE, List list)
	{
		def propMapXPathResult = ['NUMBER_TYPE': 'numberValue','STRING_TYPE': 'stringValue', 'BOOLEAN_TYPE': 'booleanValue']
		String propXPathResult = propMapXPathResult[RESULT_TYPE]
		String jsCmd = "return document.evaluate(\"$xpath\", document, null, XPathResult.$RESULT_TYPE, null).$propXPathResult;"
		return WebUI.executeJavaScript(jsCmd, list)
	}

	/**
	 * Execute a batch file situated in the KS project directory.
	 * @param batchFile (String) e.g. "myfile.bat"
	 */
	@Keyword
	public static void runBatchFile(String batchFile)
	{
		String bf = RunConfiguration.getProjectDir() + '/' + batchFile
		KeywordUtil.logInfo("Running batch file: " + bf)
		Runtime.runtime.exec(bf)
	}

	/**
	 * Stop test execution at a certain step. Useful for inspection/research.
	 */
	@Keyword
	public static void forceStop(String message)
	{
		if(message) {
			throw new StepErrorException(message)
		}
		else {
			throw new StepErrorException("Execution STOPPED!")
		}
	}
}