package com.econsys.qa.utils
import static com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.findWebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.RemoteWebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject


public class TestObjUtils
{

	/**
	 * Create a test object dynamically
	 * @param objId an object identifier
	 * @param selectorType usually it's "xpath"
	 * @param selectorValue the expression for the selector
	 * @return a Katalon test object
	 */
	@Keyword
	public static TestObject getMyTestObject(String objId, String selectorType, String selectorValue)
	{
		TestObject to = new TestObject(objId)
		to.addProperty(selectorType, ConditionType.EQUALS, selectorValue)
		return to
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object representing a HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	public static List<WebElement> getHtmlTableRows(TestObject table, String outerTagName)
	{
		WebElement mailList = findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}

	/**
	 * Get the number of options in a Select WebElement.
	 * @param selectObj Katalon test object representing a Select
	 * @return integer value of the options count in the given Select
	 */
	@Keyword
	public static int getOptionsCount(TestObject selectObj)
	{
		Select select = new Select(findWebElement(selectObj))
		return select.getOptions().size()
	}

	/**
	 * Get all attributes of a TestObject and their values in a Map.
	 * @param to Katalon test object
	 * @return Map<String, Object> object with String attributes and their values
	 */
	@Keyword
	public static Map<String, Object> getAllAttributes(TestObject to)
	{
		WebElement elem = findWebElement(to)
		return ((RemoteWebElement) elem).toJson()
	}

	/**
	 * A visual indicator of located elements.
	 * @param to Katalon test object
	 */
	@Keyword
	public static void highlightElement(TestObject to)
	{
		String jsSnippet = new File('Include/scripts/javascript/Higilighter.js').text

		WebElement elem = findWebElement(to)
		WebUI.executeJavaScript(jsSnippet, Arrays.asList(elem))
	}
	
	/**
	 * A custorm scrolling method
	 * @param to Katalon test object
	 */
	@Keyword
	public static void scrollIntoView(TestObject to)
	{
		WebElement elem = findWebElement(to)
		WebUI.executeJavaScript('arguments[0].scrollIntoView(false);', Arrays.asList(elem))
	}
}
