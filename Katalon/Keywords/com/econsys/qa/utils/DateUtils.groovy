package com.econsys.qa.utils

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

public class DateUtils
{
	/**
	 * Get current date
	 * @return a date String in 'MM/dd/yyyy' format
	 */
	@Keyword
	public static String getCurrentDate()
	{
		LocalDate today = LocalDate.now()
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy")
		String text = today.format(dtf)
		
		KeywordUtil.logInfo("getCurrentDate(): Today is " + text)
		return text
	}

	/**
	 * Get current date and time
	 * @return a date-time String in 'MM/dd/yyyy HH:mm:ss a' format
	 */
	@Keyword
	public static String getCurrentDateTime()
	{
		LocalDateTime now = LocalDateTime.now()
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss a")
		String text = now.format(dtf)
		
		KeywordUtil.logInfo("getCurrentDateTime(): The timestamp is " + text)
		return text
	}

	/**
	 * This keyword provides simple addition and subtraction arithmetic to a date (MM/dd/yyyy)
	 * @param givenDate the date to which calculation will be done 
	 * @param op addition or subtraction operator character (+/-)
	 * @param amount the amount which will be added or subtracted
	 * @param unit a time unit from the list ['DAYS', 'MONTHS', 'WEEKS', 'YEARS']
	 * @return the calculated date
	 */
	@Keyword
	public static String getCalculatedDate(String givenDate, String op, int amount, String unit)
	{
		unit = unit.toUpperCase()
		assert ['DAYS', 'MONTHS', 'WEEKS', 'YEARS'].contains(unit) : "unit '${unit}' is not acceptable"
		assert ['+', '-'].contains(op) : "'${op}' is not an acceptable operator"

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern('MM/dd/yyyy')
		LocalDate date = LocalDate.parse(givenDate, dtf)
		LocalDate calculatedDate = op == '+' ?
				date.plus(amount, ChronoUnit.valueOf(unit)) : date.minus(amount, ChronoUnit.valueOf(unit))

		KeywordUtil.logInfo("${givenDate} ${op} ${amount} ${unit} = ${calculatedDate}")
		return calculatedDate.format(dtf)
	}

	/**
	 * This keyword provides addition and subtraction arithmetic to the current date
	 * @param op addition or subtraction operator character (+/-)
	 * @param amount the amount which will be added or subtracted
	 * @param unit a time unit from the list ['DAYS', 'MONTHS', 'WEEKS', 'YEARS']
	 * @return the calculated date
	 */
	@Keyword
	public static String getCalculatedDate(String op, int amount, String unit)
	{
		String today = LocalDate.now().format(DateTimeFormatter.ofPattern('MM/dd/yyyy'))
		return getCalculatedDate(today, op, amount, unit)
	}

	/**
	 * This keyword will compare between two dates (MM/dd/yyyy)
	 * @param date1 the first date to be compared
	 * @param date2 the second date to be compared
	 * @return the difference between two dates in signed integer number;<br>
	 * value is positive when second date is after the first date, and vice varsa.
	 */
	@Keyword
	public static int compareDates(String date1, String date2)
	{
		Date parsedDate1= Date.parse("MM/dd/yyyy", date1)
		Date parsedDate2= Date.parse("MM/dd/yyyy", date2)
		int diff = parsedDate2.compareTo(parsedDate1)
		
		KeywordUtil.logInfo("Date comparison: ${date1} - ${date2} = ${diff}")
		return diff
	}

	/**
	 * This keyword will change the format of a date String
	 * @param givenDate the date in String
	 * @param pattern the date format to be used in String
	 * @return a formatted date String; an empty String on failure
	 */
	@Keyword
	public static String formatDateString(String givenDate, String pattern)
	{
		String output = ''
		try
		{
			Date d = new Date(givenDate)
			SimpleDateFormat sdf = new SimpleDateFormat(pattern)
			output = sdf.format(d)
			KeywordUtil.markPassed("The date ${givenDate} is formatted to ${output}")
		} catch (Exception e)
		{
			KeywordUtil.markFailed("Error formatting the date ${givenDate} using pattern ${pattern}.")
		}
		return output
	}
}
