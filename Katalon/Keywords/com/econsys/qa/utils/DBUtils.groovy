package com.econsys.qa.utils

import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.sql.Statement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.db.DatabaseSettings
import com.kms.katalon.core.db.ListStringResultSetHandler
import com.kms.katalon.core.db.SqlRunner
import com.kms.katalon.core.testdata.DBData
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable as GlobalVariable

/**
 * Database query helper class.
 * In most cases, the {@link #getQueryResults(java.lang.String)} would be enough to fetch results from the db.
 */
public class DBUtils
{

	private static Connection connection = null

	/**
	 * Open and return a connection to database using the Katalon project settings
	 * @return an instance of java.sql.Connection
	 */
	@Keyword
	private static Connection getDBConnection()
	{
		DatabaseSettings dbs = new DatabaseSettings(RunConfiguration.getProjectDir())
		dbs.setUser(GlobalVariable.env)
		return dbs.getDatabaseConnection().getConnection()
	}

	/**
	 * Execute the given SQL SELECT query.
	 * Don't use this method for non-SELECT queries.
	 * @param query the SQL query to run
	 * @return a database table in a {@link java.util.List} form
	 */
	@Keyword
	public static List<List<String>> getQueryResults(String query)
	{
		SqlRunner sr = new SqlRunner(getDBConnection(), query, (Object[]) null)
		List<List<String>> convertedResultset = sr.query(new ListStringResultSetHandler())
		if(sr.isConnectionAlive()) sr.close()
		return convertedResultset
	}

	/**
	 * Open and return a connection to database using the given parameters.
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 * @deprecated
	 */
	@Keyword @Deprecated
	public static Connection openDBConnection(String dbUrl, String username, String pass)
	{
		if(connection != null && !connection.isClosed())
		{
			return connection
		}
		
		// Method 1 - by using DriverManager
		if(dbUrl.contains("jdbc:oracle")) {
			Class.forName("oracle.jdbc.OracleDriver")
		}
		connection = DriverManager.getConnection(dbUrl, username, pass)
		connection.setAutoCommit(true)

		// Method 2 - by using DataSource (and skipping the JNDI related stuffs)
		//		DataSource ds
		//		if(dbUrl.contains("jdbc:oracle")) {
		//			ds = new OracleDataSource()
		//			ds.setURL(dbUrl)
		//		}
		//		connection = ds.getConnection(username, pass)

		DatabaseMetaData md = connection.getMetaData()
		KeywordUtil.logInfo('Connected Database: ' + md.getDatabaseProductVersion())
		KeywordUtil.logInfo('Database Connection Driver: ' + md.getDriverName() + ' (' + md.getDriverVersion() + ')')
		KeywordUtil.logInfo('Auto-commit is set to ' + connection.getAutoCommit())
		return connection
	}

	/**
	 * Open and return a connection to database using the Katalon project settings.
	 * This is the preferred way to connect to the test database.
	 * @return an instance of java.sql.Connection
	 */
	@Keyword
	public static Connection openDBConnection() {
		if(connection != null && !connection.isClosed()) {
			return connection
		}

		connection = getDBConnection()
		connection.setAutoCommit(true)

		DatabaseMetaData md = connection.getMetaData()
		KeywordUtil.logInfo('Database Connection Driver: ' + md.getDriverName() + ' (' + md.getDriverVersion() + ')')
		KeywordUtil.logInfo('Auto-commit is set to ' + connection.getAutoCommit())

		return connection
	}

	/**
	 * Execute a SQL query on database.
	 * Don't use this method for non-SELECT queries.
	 * @param queryString SQL query string
	 * @return query result as an instance of {@link java.sql.ResultSet}
	 * @see {@link java.sql.Statement#executeQuery(java.lang.String)}
	 */
	@Keyword
	public static ResultSet executeQuery(String queryString) {
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(queryString)
		return rs
	}

	/**
	 * Execute non-SELECT SQL (usually INSERT/UPDATE/DELETE or DDL) on database.
	 * @param queryString a SQL statement
	 * @return false with non-SELECT SQL; true when there are query results
	 * @see {@link java.sql.Statement#execute(java.lang.String)}
	 */
	@Keyword
	public static boolean execute(String queryString) {
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)
		return result
	}

	/**
	 * Execute non-SELECT SQL (usually INSERT/UPDATE/DELETE) on database.
	 * Use this to get the affected row count.
	 * @param queryString a SQL statement
	 * @return row count for the DML; 0 for the DDL
	 * @see {@link java.sql.Statement#executeUpdate(java.lang.String)}
	 */
	@Keyword
	public static int executeUpdate(String queryString) {
		Statement stm = connection.createStatement()
		int rowCount = stm.executeUpdate(queryString)
		return rowCount
	}

	/**
	 * Closes the connection
	 */
	@Keyword
	public static void closeDBConnection() {
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}

	/**
	 * SQL ResultSet handler.
	 * @param rs the query result as {@link java.sql.ResultSet}
	 * @return a String {@link java.util.List} representation of the query result
	 * @throws SQLException
	 */
	private static List<List<String>> handle(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData()
		int numberOfColumns = rsmd.getColumnCount()
		List<List<String>> result = new ArrayList<List<String>>()
		List<String> row = new ArrayList<String>()

		// add column header from the query
		for (int i = 1; i <= numberOfColumns; i++) {
			row.add(rsmd.getColumnLabel(i))
		}
		result.add(row)

		// add data rows
		while (rs.next()) {
			row = new ArrayList<String>()
			for (int i = 1; i <= numberOfColumns; i++) {
				row.add(rs.getString(i))
			}
			result.add(row)
		}
		return result
	}

	/**
	 * A fix for <code>DBData.getValue(String, int)</code> method.<br>
	 * This was later fixed in Katalon version 7.0
	 * (see <a href = "https://docs.katalon.com/katalon-studio/new/version-70.html#fixes">changelog</a>)
	 * @param td DBData a AbstractTestData of type DBData
	 * @param columnName String the column name in the SQL ResultSet
	 */
	@Keyword
	public static int getDBDataColIndex(DBData td, String columnName) {
		return td.rsHandler.columnNames.indexOf(columnName) + TestData.BASE_INDEX
	}
}