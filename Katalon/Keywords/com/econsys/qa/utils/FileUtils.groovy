package com.econsys.qa.utils

import com.kms.katalon.core.annotation.Keyword

public class FileUtils
{

	/**
	 * Save properties to a file.
	 * @param path file path
	 * @param prop the Properties object that needs to be persisted
	 */
	@Keyword
	public static void writeToConfigFile(String path, Properties prop)
	{
		new File(path).withWriter
		{ writedata ->
			prop.store(writedata, "Config saved on " + DateUtils.getCurrentDate())
		}
	}

	/**
	 * Read properties from a file.
	 * @param path file path
	 * @return properties from the file
	 */
	@Keyword
	public static Properties readConfigFile(String path)
	{
		Properties prop = new Properties()
		new File(path).withReader
		{ readdata ->
			prop.load(readdata)
		}
		return prop
	}


	/**
	 * Save binary data to a file.
	 * @param path file path
	 * @param obj the object that needs to be persisted
	 */
	@Keyword
	public static void writeToFile(String path, Object obj)
	{
		new File(path).append(new ObjectInputStream(obj))
	}

	/**
	 * Read data as text from a file.
	 * @param path file path
	 */
	@Keyword
	public static String readFileAsText(String path)
	{
		return new File(path).getText()
	}
}
