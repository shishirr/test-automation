<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_State</name>
   <tag></tag>
   <elementGuidId>e588b077-3f3d-4689-9386-ef1bb5f5a6b5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dsState</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Select...
                    Alabama
                    Alaska
                    Arizona
                    Arkansas
                    California
                    Colorado
                    Connecticut
                    Delaware
                    District Of Columbia
                    Florida
                    Georgia
                    Hawaii
                    Idaho
                    Illinois
                    Indiana
                    Iowa
                    Kansas
                    Kentucky
                    Louisiana
                    Maine
                    Maryland
                    Massachusetts
                    Michigan
                    Minnesota
                    Mississippi
                    Missouri
                    Montana
                    Nebraska
                    Nevada
                    New Hampshire
                    New Jersey
                    New Mexico
                    New York
                    North Carolina
                    North Dakota
                    Ohio
                    Oklahoma
                    Oregon
                    Pennsylvania
                    Rhode Island
                    South Carolina
                    South Dakota
                    Tennessee
                    Texas
                    Utah
                    Vermont
                    Virginia
                    Washington
                    West Virginia
                    Wisconsin
                    Wyoming
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dsState&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//select[@id='dsState']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//span[@id='st']/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State :'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Country :'])[1]/following::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search for'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//form/span/select</value>
   </webElementXpaths>
</WebElementEntity>
