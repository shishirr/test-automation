<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_qual4_chkbox</name>
   <tag></tag>
   <elementGuidId>ff3c22c8-50b4-4e9b-bb47-92d12a32a39c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > div:nth-child(11) > div.fade.in.modal > div > div > div.modal-body > div:nth-child(3) > div > div.row.top-margin-sm > div > table > tbody > tr:nth-child(3) > td:nth-child(6) > div > label > input[type=&quot;checkbox&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@type = 'checkbox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;fontawesome-i2svg-active gr__fedhr03_econsys_com fontawesome-i2svg-complete&quot;]/body[@class=&quot;modal-open&quot;]/div[9]/div[@class=&quot;fade in modal&quot;]/div[@class=&quot;modal-lg modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;row top-margin-sm&quot;]/div[@class=&quot;col-xs-12&quot;]/table[@class=&quot;table table-striped table-bordered table-condensed table-hover&quot;]/tbody[1]/tr[3]/td[6]/div[@class=&quot;checkbox all-margin-no&quot;]/label[1]/input[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>(//input[@type='checkbox'])[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spec Exp'])[2]/following::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Financial Management Specialist'])[3]/following::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[4]/preceding::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Financial Management Specialist'])[4]/preceding::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[3]/td[6]/div/label/input</value>
   </webElementXpaths>
</WebElementEntity>
