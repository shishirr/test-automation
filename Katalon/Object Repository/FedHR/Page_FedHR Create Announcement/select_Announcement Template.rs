<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Announcement Template</name>
   <tag></tag>
   <elementGuidId>0784cb05-e0ba-4979-a652-dc34723b6b7e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Vacancy Announcement Template, required</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>templateId</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>templateId</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						
		    			17.
		    			CNIC HQ Millington, TN- Washington, DC
		    			CNRE Naples CYP WW
		    			CNRE Naples Local
		    			CNRE Naples WW
		    			CNRE Naples WW
		    			CNRE Rota LOCAL
		    			CNRE Rota WW
		    			CNRE Sigonella Local
		    			CNRE Souda Bay, Greece WW
		    			CNRE Souda Bay, Greece local
		    			CNREURAFSWA BAH Local
		    			CNREURAFSWA BAH WW
		    			CNREURAFSWA CLDJ TDY
		    			CNRH
		    			CNRJ LOCAL Vacancy Announcements
		    			CNRJ Worldwide Announcements
		    			CNRK Worldwide Announcements
		    			CNRNW Announcements
		    			CNRSE CYP Caregiver Announcements
		    			CNRSE CYP Leader Announcement
		    			CNRSE N9
		    			CNRSE N9 General Announcements for NSGB
		    			CNRSE N9 General Announcements no PCS
		    			CNRSE N9 General Announcements with PCS
		    			CNRSE N9 General Announcements with PCS
		    			CNRSE N91 Counselor
		    			CNRSE SARC Position
		    			Educational Aide / Technician
		    			JRM
		    			MidAtlantic CYP Announcements
		    			MidAtlantic Educational Aide/Technician Announcements
		    			MidAtlantic MWR Announcements
		    			MidAtlantic N91 Announcements
		    			MidAtlantic NGIS Announcements
		    			MidAtlantic NWW N95 Announcements
		    			MidAtlantic QRP Announcements
		    			N926 CYP 13-01
		    			N926 CYP 13-02
		    			N926 CYP 13-03
		    			N926 CYP Program Assistant (CYI - CYII)
		    			NDW Announcements - (JBAB 2018)
		    			NDW Announcements - Annapolis (2018)
		    			NDW Announcements - CYP 2/3/4
		    			NDW Announcements - Dahlgren (2018)
		    			NDW Announcements - Indian Head (2018)
		    			NDW Announcements - Pax River (2018)
		    			NDW Announcements - Solomons (2018)
		    			NRSW Southwest Region
		    			NRSW Southwest Region
		    			NSA Monterey
		    			NSAB Announcements
		    			NSAB CDC Aid/Technician (Bethesda)
		    			NSAB-CYP Announcements (Bethesda)
		    			NSAB-CYP Announcements (Bethesda)
		    			NSAB-CYP Announcements (Bethesda)
		    			Singapore Area Coordinator
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			Testing with Katalon
		    			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;templateId&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='templateId']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='maincolumn']/form/div[2]/fieldset/div/span/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='* Announcement Template:'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Announcement Information'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recruitment Request'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
