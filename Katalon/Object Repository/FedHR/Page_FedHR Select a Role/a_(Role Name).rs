<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_(Role Name)</name>
   <tag></tag>
   <elementGuidId>27bab217-b0e0-4b27-95a8-a24e46c2b417</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[starts-with(@title, 'Login as ${roleName} for') and (contains(text(), '${roleName} for') or contains(., '${roleName} for'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onmouseover</name>
      <type>Main</type>
      <value>javascript: window.status='Role(s)';</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Login as ${roleName} for</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${roleName} for</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rolesSelection&quot;)/div[@class=&quot;formFields&quot;]/div[@class=&quot;row odd&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
