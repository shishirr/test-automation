<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Employee</name>
   <tag></tag>
   <elementGuidId>6e8b25e5-f82e-4501-9efc-c5cd5d061fd6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[starts-with(@title, 'Login as Employee for')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onmouseover</name>
      <type>Main</type>
      <value>javascript: window.status='Role(s)';</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Login as Employee for</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Employee for</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rolesSelection&quot;)/div[@class=&quot;formFields&quot;]/div[@class=&quot;row odd&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
