<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_VA Search Results</name>
   <tag></tag>
   <elementGuidId>55e2ce30-1307-4c3a-8c33-1d81642d2d8b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>results</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                   Next
                    Displaying page 1 of 46  
        
        
       
         
              
                
                    Annc. #
    Recruitment Request #                    Job Title
                    Create Date
                    Pay Plan
                    
                    
                    Series
                    
                    
                    Grades
                    
                    # Apps
                    Status
    Owner                    Actions
                
            
            
                      
                          
                            
                                VA-19-17863
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17861
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17865
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17864
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17859
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17860
                            
                        
                               
                        
                            
                        
                        
                            03/07/2019
                        
                            AC
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17858-Katalon
                            
                        
                               
                        
                            QA Analyst
                        
                        
                            03/04/2019
                        
                            GS
                        
                            0510
                        
                            09/10/11
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17855
                            
                        
                               
                        
                            
                        
                        
                            03/04/2019
                        
                            NF
                        
                            
                        
                            
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
                      
                          
                            
                                VA-19-17857-Katalon
                            
                        
                               
                        
                            QA Analyst
                        
                        
                            03/04/2019
                        
                            GS
                        
                            0510
                        
                            09/10/11
                        
                             - 
                        
                            Draft
    
                            Last 710187, First 710187
                        
                            Preview
                        
                    
            
        
      
      
        
                 Go to page: 
                 
                  1
                  2
                  3
                  4
                  5
                  6
                  7
                  8
                  9
                  10
                  11
                  12
                  13
                  14
                  15
                  16
                  17
                  18
                  19
                  20
                  21
                  22
                  23
                  24
                  25
                  26
                  27
                  28
                  29
                  30
                  31
                  32
                  33
                  34
                  35
                  36
                  37
                  38
                  39
                  40
                  41
                  42
                  43
                  44
                  45
                  46
              
           
         
          You have 451 vacancy announcements.
      </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='results']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='maincolumn']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[4]/div/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
