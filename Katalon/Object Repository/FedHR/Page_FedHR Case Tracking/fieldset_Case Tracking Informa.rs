<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldset_Case Tracking Informa</name>
   <tag></tag>
   <elementGuidId>9241e4e6-2ca7-45ea-8d93-f4c917c3f493</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//fieldset[@id = 'altrows']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>fieldset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>altrows</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  		Case Tracking Information

		
				Employee:
				
					Last 770098, First 770098 M
				
			
			
			Owner: 
			Last 710187, First 710187 
		

		
			Case Type, Activity: 
			Benefits, FERS-Election 
		

		
			Last Detail/Step: 
			Forms Submitted 
		

		
			Last Updated By: 
			
				Last 710187, First 710187
				8/31/2018 1:34 PM
			
		

		
			Comments for Employee: 
			Case created by Last 710187, First 710187 
		

		
				HR Comments: 
				 
			
		
				HR Only Comments: 
				 
			
		
					Hidden from Employee: 
					
						
										No
									
			    
		    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;altrows&quot;)</value>
   </webElementProperties>
</WebElementEntity>
