<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_DHS 307 FEMA</name>
   <tag></tag>
   <elementGuidId>e3a6f997-f60f-4db7-a08a-1eb925573d2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='aside2']/div/div[2]/ul/li[2]/strong/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@title = 'Edit form DHS 307 FEMA in Forms Manager' and (text() = 'DHS 307 FEMA' or . = 'DHS 307 FEMA')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Edit form DHS 307 FEMA in Forms Manager</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript: editFormGivenIdFromCT('1848706');</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>52</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DHS 307 FEMA</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;aside2&quot;)/div[@class=&quot;aside_box&quot;]/div[@class=&quot;body&quot;]/ul[@class=&quot;bottom-margin-xxl&quot;]/li[2]/strong[1]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <value>//a[contains(text(),'DHS 307 FEMA')]</value>
   </webElementXpaths>
</WebElementEntity>
