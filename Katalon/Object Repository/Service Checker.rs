<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This is a web request object to get the deployed FedHR app version info in an environment.</description>
   <name>Service Checker</name>
   <tag></tag>
   <elementGuidId>b3290c10-86f0-48d5-b0f1-2a727f592fc7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://${env}.econsys.com/frbweb/fhrnavigator/status/${agency}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.env</defaultValue>
      <description>The test database/environment</description>
      <id>1b2e72db-71f1-4517-b024-111117de722a</id>
      <masked>false</masked>
      <name>env</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.agency</defaultValue>
      <description>The test agency account</description>
      <id>fa4a0fa6-ddce-404b-b314-eccdf6c25ea8</id>
      <masked>false</masked>
      <name>agency</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()
ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

def variables = request.getVariables()
def env = variables.get('env')
def agency = variables.get('agency')

def jsonSlurper = new JsonSlurper()
def jsonResponse = jsonSlurper.parseText(response.getResponseText())

assert jsonResponse instanceof Map
assert jsonResponse.component instanceof List&lt;Map>

Map map = jsonResponse
assert map.size() == 4

assert map.containsKey('hostName')
assert GlobalVariable.envType.equals('prod') ?
	map.get('hostName').contains('prod') : map.get('hostName')

assert map.containsKey('url')
def expectedUrl = GlobalVariable.siteURL ?
	&quot;https://${GlobalVariable.siteURL}/frbweb/fhrnavigator/status/${agency}&quot;
	: &quot;https://${env}.econsys.com/frbweb/fhrnavigator/status/${agency}&quot;
WS.verifyElementPropertyValue(response, 'url', expectedUrl)

assert map.containsKey('component')
WS.verifyElementsCount(response, 'component', 9)
def componentsToCheck = ['DATABASE', 'CALC_ENGINE', 'FILE_SERVICE', 'REPORT_SERVICE', 'EMAIL']
def comp = jsonResponse.component.findAll { componentsToCheck.contains(it.name) }
assert comp.every { it.status == 'UP' }
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
