<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PD Plus</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>340ec2dd-2b5d-45bf-8ced-3b2edc9e20b4</testSuiteGuid>
   <testCaseLink>
      <guid>7d8ae872-c216-48f7-b7e4-2d1b3b018113</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PD Plus/Create Standard Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>678f486c-b2d8-4d84-afef-1159ff8172e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PD Plus/Create Individual Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2724b29d-d3cd-4e89-9f6d-0a19bdbaed4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PD Plus/PD Plus Search</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
