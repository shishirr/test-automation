<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Authentication (HR)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>d1fa56cc-828a-4855-8410-90d5cfae0a64</testSuiteGuid>
   <testCaseLink>
      <guid>be5208a1-dc8b-4ed5-a6bf-9705dc853c59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Access Control/Login as Admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0799ca98-2202-4358-bfaf-d7b5948b0778</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Helper Tasks/Choose a user role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>true</value>
         <variableId>87f54fa6-7e92-41a2-9291-8526ad1bc20e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Employee&quot;</value>
         <variableId>0639377b-f03f-4c1c-ab37-b80e31fbbf15</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6cf302ab-7fad-4e9a-b8e2-6e02cabd0a50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Helper Tasks/Log out of FedHR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
