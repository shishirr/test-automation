<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Vacancy Announcement</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>53aee7ac-7a86-4e4b-b8ca-3ca83c9a99e6</testSuiteGuid>
   <testCaseLink>
      <guid>e1f82150-7e85-47cb-be81-6df9555ac6f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Create a draft VA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1556a80d-14e5-4d6f-88f9-03219f2ce8e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Compose and Save a VA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb3b84c9-e026-417c-96d8-fde1d099c798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Fill Out the VA form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>deb7c48a-71d7-4f6f-a2d6-90c8f8304688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Post a VA to USAJobs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d4bb9ce-d08b-4107-b440-4bbbf7385759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Preview a VA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aec3d73-9268-4c80-a15b-9a20a70e82bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Copy a Posted VA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01dbdffa-5e03-480f-be9a-8697b582e52b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Search for a VA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b0d8ac1-4f5b-4494-ad82-f9cc67991d7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/View a Posted On USAJOBS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>076b0041-42af-4576-85b2-b792bab3fec7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Hiring/Vacancy Announcement/Manage Vacancy Email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
