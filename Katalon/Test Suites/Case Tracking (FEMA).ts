<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Case Tracking (FEMA)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>394809e4-01cd-4630-862d-13e777b9bd9e</testSuiteGuid>
   <testCaseLink>
      <guid>5ead3b14-66ef-4da7-a0a8-35592b19ebe6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Case Tracking/Fema_Performance/Fema - DHS 306 Performance form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34adfa62-eb01-447a-8fe8-1432ebaf7d60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Case Tracking/Fema_Performance/Fema - DHS 307 Performance form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe1b7f03-357d-4567-a1ec-33f60295dae5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Case Tracking/Fema_Performance/Fema - DHS 308 Performance form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d1469ed-2888-4bf6-9213-a85651e0f095</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Case Tracking/Fema_Performance/Fema - DHS 309 Performance form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c06d0ea7-5721-48bc-b201-e42d621bb058</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Case Tracking/Fema_Performance/Fema - DHS 310 Performance form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8bf20b6-c491-4c85-9348-253d0585e49c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Case Tracking/Individual On-Boarding</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
