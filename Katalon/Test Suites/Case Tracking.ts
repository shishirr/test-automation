<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Case Tracking</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>5db98fe3-e42a-4385-b8e6-c7583d3fc507</testSuiteGuid>
   <testCaseLink>
      <guid>a4878eb3-b863-4d58-ba90-b676292eece1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Case Tracking/Create New Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>755bbe7a-0983-44a7-8a01-ab555b73c463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Case Tracking/Add A Form To An Existing Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a00139d6-5b0d-4381-8f6c-cafc59a77936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Case Tracking/Close And Reopen A Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc2b095b-f43c-42df-8b1e-15fee1001924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Case Tracking/Forms/E-Sign a Form</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
