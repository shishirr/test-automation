<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>FedHR Regression (Cucumber)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>5</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>99234d62-375c-422f-9045-bbc3e926f749</testSuiteGuid>
   <testCaseLink>
      <guid>bd23e535-0184-4ee5-bd92-11ea34c27ad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-1 FedHR User Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9fd98cb-1c3d-4b05-a8a1-ebb59d7b4bd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-52 Add a Contractor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73ea0df8-c3a5-444f-b9e9-b4d249bbdf08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-8 Create a New Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd060b15-1c69-4bfa-b790-1528de8d19fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-46 Add Forms to a Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>734cb3b6-722e-4314-9ce1-b423f8a9d290</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-49 E-Sign a Form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9c1f3ff-292f-4939-9e10-e53374a99cd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscellaneous/Cucumber/Feature Runner/KAT-55 FedHR User Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
