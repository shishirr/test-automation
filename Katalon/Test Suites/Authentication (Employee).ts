<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Authentication (Employee)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>70999dd5-e671-4d35-9219-90662e5475d9</testSuiteGuid>
   <testCaseLink>
      <guid>328334be-020d-43b8-bd5e-d308b14bfc00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Access Control/Login as Employee</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>895aa03b-976a-4735-8f06-b26d878afe11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Helper Tasks/Choose a user role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>true</value>
         <variableId>87f54fa6-7e92-41a2-9291-8526ad1bc20e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0639377b-f03f-4c1c-ab37-b80e31fbbf15</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6cf302ab-7fad-4e9a-b8e2-6e02cabd0a50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Helper Tasks/Log out of FedHR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
