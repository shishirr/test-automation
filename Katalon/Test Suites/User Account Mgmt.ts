<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>User Account Mgmt</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>b46ea8dd-2dd7-4017-9cc3-7f30c4f5b9da</testSuiteGuid>
   <testCaseLink>
      <guid>7a00ff43-0ee7-407c-9e5f-330d7e79e3cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Account Mgmt/Add a Contractor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38bb2350-a04a-4321-ac39-f49550513bc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User Account Mgmt/View Supervisor History</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
