import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String identifier = new Random().nextLong().abs().toString().substring(0, 4)

String lastName = 'LAST' + identifier

String firstName = 'FIRST' + identifier

String email = ('tester' + identifier) + '@econsys.com'

String userName = 'tester' + identifier

String pass = 'UNITEDstates1776!&&^'

String fullName = (lastName + ', ') + firstName

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/a_User Account Administration'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Add New Contractor'), 
    2)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Add New Contractor'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/input_YesRadioButton'))

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_lastName_User'), lastName)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_First Name_User'), firstName)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_User E-Mail_userEmail'), 
    email)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Login username'), userName)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_One time password'), pass)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_(Deactivate user  s access on)_suspenseDate'), 
    'next 6 month')

WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR Create New User Account/select_Group'), 0)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create New User Account/select_role_Administrator'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create New User Account/select_role_Administrator'), 'Y', true)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Save'), 10)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Create New User Account/input_Save'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/a_User Account Administration'))

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/input_Search_searchText'), 
    lastName)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Search'), 10)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/button_Search'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/LastNameAndFirstName'), 
    10)

String compareUsername = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR User Account Administration/LastNameAndFirstName'))

WebUI.verifyMatch(fullName, compareUsername, false)

