import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String expectedSupervisorName = 'Last 1036287, First 1036287'

String expectedEffectiveDate = '03/31/2019'

not_run: String expectedLastEdtiedBy = 'Last 710187, First 710187'

not_run: String expectedEditDate = '05/01/2019'

WebUI.setText(findTestObject('FedHR/Page_FedHR Home/input_EmployeeSearch'), 'Last 1000791')

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/i_Search_Icon'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Employee Search/a_EmployeeName'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/input_SupervisorHistoryBut'))

'Looking up expected data row by \'Effective Date\''
WebUI.verifyElementPresent(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/div_EffectiveDate', [
            ('eDate') : expectedEffectiveDate]), 10)

supervisorName = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/div_SupervisorName', 
        [('eDate') : expectedEffectiveDate]))

WebUI.verifyMatch(supervisorName.trim(), expectedSupervisorName, false)

not_run: lastEdtiedBy = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/div_LastEditedBy', 
        [('eDate') : expectedEffectiveDate]))

not_run: WebUI.verifyMatch(lastEdtiedBy.trim(), expectedLastEdtiedBy, false)

not_run: editDate = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/div_EditDate', 
        [('eDate') : expectedEffectiveDate]))

not_run: WebUI.verifyMatch(editDate.trim(), expectedEditDate, false)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Personal Information/span_CloseButton'))
