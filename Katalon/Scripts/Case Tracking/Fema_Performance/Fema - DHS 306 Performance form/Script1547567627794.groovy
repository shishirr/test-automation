import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_New Case'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_New Case'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), 10)

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), '1006', false)

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), '15003', false)

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), '32339', false)

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_emp'), 'Testing')

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_hr'), 'Testing')

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 'Last 631660')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 'N', false)

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Select'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Performance/a_DHS 306 FEMA'), 10)

WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Performance/a_DHS 306 FEMA'), 'DHS 306 FEMA')

String whatDhsForm = WebUI.getText(findTestObject('FedHR/Page_FedHR Performance/a_DHS 306 FEMA'))

KeywordUtil.logInfo(whatDhsForm + ' is the form user631660 needs based on grade and supervisory code')

