import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String formName = 'REQUEST FOR PERSONNEL ACTION'

String formNumber = 'SF 52'

WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'))

'Check if the case already has forms or not'
if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Case Details/button_Add Forms'), 10, FailureHandling.OPTIONAL)) {
    
	WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/button_Add Forms'))

    WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/input_checkbox_form1 (modal)'))

    WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Details/input_Submit'), 10)

    WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/input_Submit'))

    WebUI.delay(2)
	
	WebUI.acceptAlert()
	
} else {

    WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Details/button_Manage Forms'), 10)

    WebUI.click(findTestObject('FedHR/Page_FedHR Case Details/button_Manage Forms'))
	
}

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Folder Contents/a_Add a Form to this Folder'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/a_Add a Form to this Folder'))

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Search forms'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Folder Contents/input_FormSearchField'), formNumber)

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Add'))

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_Close'))

WebUI.delay(1)

// also works as an implicit assert
WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/a_(Form Number)', [('formNumber') : formNumber]))

'Check to see if there is a \"Lock forms\" popup and click \'No\' to it if it is there'
if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Folder Contents/button_No'), 10, FailureHandling.OPTIONAL)) {
    
	WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/button_No'), FailureHandling.CONTINUE_ON_FAILURE)
}

// also works as an implicit assert
WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/text_(Form Name)', [('formName') : formName]), 10, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_(Common)/input_PreviousBut'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Folder Contents/a_ViewUpdate Case Link'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Folder Contents/a_ViewUpdate Case Link'))

WebUI.delay(1)

// also works as an implicit assert
WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Details/a_(Form Number)_FormsFolder', [('formNumber') : formNumber]), 10)