import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

String val_CaseType = null
String val_Activity = null
String val_Step = null
String val_FormName = null

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_New Case'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_New Case'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), 10)

'Adding delay to make sure the menu drop-down gets updated properly'
WebUI.delay(1)

if (val_CaseType == null) {
	
	WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), '1')
	
} else {

	WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Type'), val_CaseType, false)

}

'Adding delay to make sure the menu drop-down gets updated properly'
WebUI.delay(1)

if (val_Activity == null) {
	
	WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), '1')
	
} else {

	WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Activity'), val_Activity, false)

}

'Adding delay to make sure the menu drop-down gets updated properly'
WebUI.delay(1)

if (val_Step == null) {
	
	WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), '1')
	
} else {

	WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR New Case/select_Select One_Case Step'), val_Step, false)

}

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/span-activity_form_ids'), 10)

'Check to see if the Case Type and Step combo has options for seleting forms'
if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR New Case/input_checkbox_form1'), 10, FailureHandling.OPTIONAL)) {
	
	if (val_FormName != null) {
		
		WebUI.check(findTestObject('FedHR/Page_FedHR New Case/input_checkbox_(Form Name)', [('formName') : val_FormName]))
		
	} else {
	
    	'Check to see if the first form option is already selected'
    	if (WebUI.verifyElementNotChecked(findTestObject('FedHR/Page_FedHR New Case/input_checkbox_form1'), 10, FailureHandling.OPTIONAL) == true) {
		
			WebUI.check(findTestObject('FedHR/Page_FedHR New Case/input_checkbox_form1'))
			
    	}
    }
}

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_emp'), 'Comments for employee', FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_hr'), 10)

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/textarea_comments_hr'), 'Comments from HR')

WebUI.setText(findTestObject('FedHR/Page_FedHR New Case/input_employee'), 'last')

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Select'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR New Case/select_Select One'), 'N', true)

WebUI.click(findTestObject('FedHR/Page_FedHR New Case/button_Next'))

WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/fieldset_Case Tracking Informa'), 10)