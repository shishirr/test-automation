import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'), 10)

firstCaseNumber = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

'Picking the very first \'Assigned to Me\' case'
WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

caseType = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/h2_Header_Case Type'))

if (caseType.contains('Recruitment')) {
    WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/AssignReassignButton'))
} else {
    WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/input_AssignButton'))
}

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/select_EmailYes'), 'Y', false)

alertPresent = WebUI.waitForAlert(10)

if (alertPresent) {
    if (WebUI.getAlertText().contains('does not have an email address')) {
        WebUI.acceptAlert()
		
		if (caseType.contains('Recruitment')) {
			WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/AssignReassignButton'))
		} else {
			WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/input_AssignButton'))
		}
		
        WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/select_EmailYes'), 'N', 
            false)
    } else {
        WebUI.setText(findTestObject('FedHR/Page_FedHR Recruitment/input_Notify By Email_TextField'), 'test@econsys.com')
    }
}

totalOption = WebUI.getNumberOfTotalOption(findTestObject('FedHR/Page_FedHR Recruitment/select_SelectDetailStep'))

WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR Recruitment/select_SelectDetailStep'), new Random().nextInt(totalOption))

WebUI.setText(findTestObject('FedHR/Page_FedHR Case Tracking/textarea_employee_comments'), 'Employee Comments', FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('FedHR/Page_FedHR Recruitment/textarea_Comments_hrcomments'), 'Comments made by HR')

WebUI.setText(findTestObject('FedHR/Page_FedHR Recruitment/textarea_Comments_hronlycomments'), 'HR internal', 
    FailureHandling.OPTIONAL)

if (GlobalVariable.agency == 'NDW') {
    WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Last NameSearchField'), '845084')
} else if (GlobalVariable.agency == 'FEMA') {
    WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Last NameSearchField'), '1007487')
}

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Last NameSearchField'), Keys.chord(Keys.ARROW_DOWN, 
        Keys.ENTER))

WebUI.delay(1)

if (caseType.contains('Recruitment')) {
    WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_SaveButton'))
} else {
    WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/input_SaveButton_CaseTracking'))
}

WebUI.delay(2)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_All CasesButton'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'), 10)

'Picking the very first \'All Cases\' case'
WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Case Tracking/span_Case Number'), 'Case #' + firstCaseNumber)

WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Case Tracking/p_ReadOnlyMarker'), 10)

assignee = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/span_Case Owner'))

if (GlobalVariable.agency == 'NDW') {
    assert assignee.contains('845084')
} else if (GlobalVariable.agency == 'FEMA') {
    assert assignee.contains('1007487')
}