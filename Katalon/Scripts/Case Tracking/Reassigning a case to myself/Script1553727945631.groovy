import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

expectedLastDetail = 'Case Reassigned'


WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_All CasesButton'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'), 10)

firstCaseNumber = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_ReassignButton'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Case Tracking/textarea_employee_comments'), 'Comments by employee', FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('FedHR/Page_FedHR Recruitment/textarea_Comments_hrcomments'), 'Comments from HR')

WebUI.setText(findTestObject('FedHR/Page_FedHR Recruitment/textarea_Comments_hronlycomments'), 'HR internal Comments', FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/select_ Email'), 10)

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/select_ Email'), 'N', true)

if (GlobalVariable.agency == 'NDW') {
	WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Assign this case to_specNameQry'),
		'710187')
} else if (GlobalVariable.agency == 'FEMA') {
	WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Assign this case to_specNameQry'),
		'Lnlnln487003')
}

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/input_Last NameSearchField'), Keys.chord(Keys.ARROW_DOWN,
		Keys.ENTER))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/AssignButton'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Case Tracking/span_Case Number'), 'Case #' + firstCaseNumber)

WebUI.verifyElementNotPresent(findTestObject('FedHR/Page_FedHR Case Tracking/p_ReadOnlyMarker'), 10)

assignee = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/span_Case Owner'))

if (GlobalVariable.agency == 'NDW')	assert assignee.contains('710187')
else if (GlobalVariable.agency == 'FEMA')	assert assignee.contains('Lnlnln487003')

assert WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/span_Last Detail')).trim().contentEquals(expectedLastDetail)