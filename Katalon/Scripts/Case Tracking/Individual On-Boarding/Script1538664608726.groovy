import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

testBirthYear = '10/10/2000'

'Getting a random number'
ssn = new Random().nextLong().abs().toString().substring(0, 9)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Individual On-boarding'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Last Name_lastName'), 'Last')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ First Name_firstName'), 'First')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Middle Initial_middleNam'), 'M')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Date of Birth_birthDate'), testBirthYear)

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_eeSsn'), ssn)

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_eeReSsn'), ssn)

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_E-mail Address_emailAddr'), 'fgyamfi@econsys.com')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Employee Title_empTitle'), 'Tester')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Employing Agency_employi'), 'Fema')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Phone (Home)_homePhone'), '2434866301')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 1_addr1'), '101 Main st')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_City_city'), 'Falls Church')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_State_state'), 'VA')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Zip Code_postalCode'), '22442')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 1_workaddr1'), 'Econsys')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Address Line 2_workaddr2'), '1 duke St')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_City_workcity'), 'Fall Church')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_State_workstate'), 'VA')

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Zip Code_workpostalCode'), '45684')

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/option_Executive Resources (EX'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Organization_organizati'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Organization_organizati'))

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/a_Select_EmpOrg'))

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Supervisor_supervisorLoo'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Enter employee name and'), 'LAST')

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/div_Enter employee name and cl'))

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_Enter employee name and _1'))

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/a_Select_EmpSearch'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_LAST 1000015 FIRST 10000'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_LAST 1000015 FIRST 10000'))

WebUI.setText(findTestObject('FedHR/Page_FedHR On-boarding/input_Effective Date of Employ'), 'TODAY')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_SelectOn-boarding'), '561', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_SelectNew Appointment T'), '9711', false)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR On-boarding/select_YesNo'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR On-boarding/select_YesNo'), '2', false)

WebUI.click(findTestObject('FedHR/Page_FedHR On-boarding/input_Print_btnstandard'))

WebUI.click(findTestObject('FedHR/Page_FedHR Personal Information/a_Home'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Home/input_Search_searchString'), ssn)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/i_Search_fa fa-search fa-2x co'))

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Add New Personal Informa/input_ Date of Birth_birthDate'), 
    'value', testBirthYear, 10)

