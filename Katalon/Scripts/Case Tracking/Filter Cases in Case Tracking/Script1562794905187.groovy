import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys

String searchTrackingNum, 
	searchActivity,
	searchDetailOrStep,
	searchHRComments,
	searchDate,
	searchEmployee,
	searchOwner


WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/strong_All Assigned Cases'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/div_caseTrackingList'), 10)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/a_FirstCaseOnList'))

'Test data collection starts'
WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Case Number'), 10)

searchTrackingNum = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Case Number')).trim().substring(6)

String caseTypeActivity = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Case Type_Activity')).trim()

searchActivity = caseTypeActivity.substring(caseTypeActivity.indexOf(', ') + 2)

searchDetailOrStep = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Last Detail')).trim()

searchHRComments = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_HR Comments')).trim()

String caseLastUpdatedTime = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Last Updated Time')).trim()

'Date format workaround'
searchDate = com.econsys.qa.Util.fomatDateString(caseLastUpdatedTime, 'MM/dd/yyyy')
//searchDate = caseLastUpdatedTime2.substring(0, caseLastUpdatedTime.indexOf(' '))

searchEmployee = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/a_Employee_case tracking info')).trim()

searchOwner = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/span_Case Owner')).trim()

'Test data collection ends'
WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/input_BackButton'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Home'), 10)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Home'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/strong_All Assigned Cases'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/div_caseTrackingList'), 10)

'Search by Activity block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_activity'), searchActivity)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_Activity')).trim().equals(searchActivity)

'Search by Detail/Step block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_step'), searchDetailOrStep)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_DetailStep')).trim().equals(searchDetailOrStep)

'Search by HR Comments block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_commentsHr'), searchHRComments)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_HR Comments')).trim().equals(searchHRComments)

'Search by Date & Time block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

'WORKAROUND step 1'
WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_createdDate'), Keys.chord(Keys.CONTROL,
	'a'))
'WORKAROUND step 2'
WebUI.sendKeys(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_createdDate'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_createdDate'), searchDate)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_DateTime')).trim().contains(searchDate)

'Search by Employee block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_employee'), searchEmployee)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_Employee')).trim().equals(searchEmployee)

'Search by Owner block starts'
WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Clear'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_owner'), searchOwner)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_Owner')).trim().equals(searchOwner)

'Search by Tracking Number block starts'
WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/input_Owner_trackingNumber'), searchTrackingNum)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.delay(1)

assert WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Case Tracking/td_TrackingNum')).trim().equals(searchTrackingNum)
