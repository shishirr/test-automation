import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

caseTrackingNum = ''
expectedLastDetailStepForClosedCase = 'Closed'
expectedLastDetailStepForRopenedCase = 'Case Reopened'


WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

'Using the first assigned case to the test user'
caseTrackingNum = WebUI.getText(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'))

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'))

WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/input_CloseCaseButton'))

'Adding delay to make sure page gets updated properly'
WebUI.delay(3)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/span_LastDetailStep'), 10)

LastDetailStepForClosedCase = WebUI.getText(findTestObject('FedHR/Page_FedHR Recruitment/span_LastDetailStep'))

WebUI.verifyMatch(LastDetailStepForClosedCase.trim(), expectedLastDetailStepForClosedCase, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))

WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Case Tracking/input_Employee_trackingNumber'), caseTrackingNum)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/input_Open Cases Only_checkBox'))

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/td_Closed'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/td_Closed'))

WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/input_Re-Open Case_button'))

'Adding delay to make sure page gets updated properly'
WebUI.delay(3)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Home'))

WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Case Tracking/input_Employee_trackingNumber'), caseTrackingNum)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/button_Submit'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/td_Case Reopened'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/td_Case Reopened'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/span_LastDetailStep'), 10)

LastDetailStepForRopenedCase = WebUI.getText(findTestObject('FedHR/Page_FedHR Recruitment/span_LastDetailStep'))

WebUI.verifyMatch(LastDetailStepForRopenedCase.trim(), expectedLastDetailStepForRopenedCase, false)

