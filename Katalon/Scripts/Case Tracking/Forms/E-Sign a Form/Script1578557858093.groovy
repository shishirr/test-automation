import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.econsys.qa.utils.ActionUtils as ActionUtils
import com.econsys.qa.utils.TestObjUtils as TestObjUtils
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String formToBeOpened = 'SF 52'

String signerCredentials = findTestData('Data Files/FedHR user accounts').getValue(GlobalVariable.agency, 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/strong_Cases Assigned to me'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'), 2)

WebUI.click(findTestObject('FedHR/Page_FedHR Case Tracking/a_CaseTracking_First Case'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Case Details/a_(Form Number)_FormsFolder', [('formNumber') : formToBeOpened]))

String xpath_esign = '//input[@type=\'button\' and (@value=\'e-Sign\' or @value=\'Erase e-Sign\')]'

int count = ActionUtils.execXpath("count($xpath_esign)", 'NUMBER_TYPE', null)

assert count > 0 : 'No e-signable item exists on the form!'

for (int i = 1; i <= count; i++) {
    String currEsignXpath = "($xpath_esign)[$i]"

    TestObject to = TestObjUtils.getMyTestObject("button-0$i", 'xpath', currEsignXpath)

    WebUI.scrollToElement(to, 10)

    if (WebUI.getAttribute(to, 'value') == 'e-Sign') {
        WebUI.delay(1)

        WebUI.click(to)

        WebUI.setText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/input_Password_Field'), signerCredentials)

        WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/button_Submit'))

        WebUI.delay(2)

        WebUI.scrollToElement(to, 10)

        WebUI.verifyElementAttributeValue(to, 'value', 'Erase e-Sign', 10)

        String signedBy = WebUI.getText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/span_SignatureText', [('esignButton') : currEsignXpath]))

        assert signedBy.startsWith('Electronically signed by:') && signedBy.contains(signerCredentials.substring(4))
    }
}

KeywordUtil.logInfo('Now erasing all e-signs on the form...')

for (int i = 1; i <= count; i++) {
    String currEsignXpath = "($xpath_esign)[$i]"

    TestObject to = TestObjUtils.getMyTestObject("button-0$i", 'xpath', currEsignXpath)

    WebUI.scrollToElement(to, 10)

    if (WebUI.getAttribute(to, 'value') == 'Erase e-Sign') {
        WebUI.delay(1)

        WebUI.click(to)

        WebUI.setText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/input_Password_Field'), signerCredentials)

        WebUI.click(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/button_Submit'))

        WebUI.delay(2)

        WebUI.scrollToElement(to, 10)

        WebUI.verifyElementAttributeValue(to, 'value', 'e-Sign', 10)

        String signedBy = WebUI.getText(findTestObject('FedHR/Page_FedHR Forms/Page_SF 52 form/span_SignatureText', [('esignButton') : currEsignXpath]))

        assert signedBy.equals('')
    }
}

