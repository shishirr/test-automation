import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

ResponseObject respService = WS.sendRequestAndVerify(findTestObject('Service Checker'))

WebUI.comment(respService.getResponseText())

ResponseObject respVersion = WS.sendRequestAndVerify(findTestObject('Version Checker'))

KeywordUtil.logInfo(respVersion.getResponseText())

WebUI.navigateToUrl('https://status.fhrnavigator.com')

WebUI.delay(2)

WebUI.takeScreenshot()
