import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'This runs the features selected in the default Cucumber runner class'
CucumberKW.runWithCucumberRunner(com.econsys.qa.runners.MyCucumberRunner.class)