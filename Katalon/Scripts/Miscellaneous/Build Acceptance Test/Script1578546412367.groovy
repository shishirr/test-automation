import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.econsys.qa.utils.DBUtils as DBUtils
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint


WebUI.callTestCase(findTestCase('Miscellaneous/Build Status Test'), [:])

if (GlobalVariable.siteURL) {

	DBUtils.openDBConnection()

	String mergeUsersSql = new File('Include/scripts/sqls/reset-smoketest-users.sql').text

	assert !(DBUtils.execute(mergeUsersSql)) : 'This query should not have a ResultSet!'

	String updateTotpSql = new File('Include/scripts/sqls/turn-totp-off.sql').text

	assert DBUtils.executeUpdate(updateTotpSql) >= 0

	DBUtils.closeDBConnection()

}

WebUI.callTestCase(findTestCase('Test Helper Tasks/Init web browser'), [:])

DBData td = findTestData('FedHR users with roles')

for (int row = 1; row <= td.getRowNumbers(); row++) {
    String agency = td.getValue(DBUtils.getDBDataColIndex(td, 'URL_CODE'), row)

    String username = td.getValue(DBUtils.getDBDataColIndex(td, 'USER_NAME'), row)

    String password = td.getValue(DBUtils.getDBDataColIndex(td, 'USER_PW'), row)

    String role = td.getValue(DBUtils.getDBDataColIndex(td, 'ROLE_NAME'), row)

    String userEmail = 'smoketest@econsys.com'

    'for debugging purpose'
    KeywordUtil.logInfo((('Data row ' + row) + ' : ') + td.getAllData().get(row - 1))

    WebUI.callTestCase(findTestCase('Test Helper Tasks/Navigate to FedHR Login'), [('agency') : agency])

    WebUI.callTestCase(findTestCase('Test Helper Tasks/Submit login credentials'), [('username') : username, ('password') : password])

    WebUI.callTestCase(findTestCase('Test Helper Tasks/Init login user'), [('testUserEmail') : userEmail, ('role') : role])

    WebUI.callTestCase(findTestCase('Test Helper Tasks/Log out of FedHR'), [:])
}

