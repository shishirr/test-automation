import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Test Helper Tasks/Init web browser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://analytics.katalon.com/login?redirect=%2F')

WebUI.setText(findTestObject('Katalon/Page_Katalon Analytics (Beta)/input_username'), 'qateam@econsys.com')

WebUI.setEncryptedText(findTestObject('Katalon/Page_Katalon Analytics (Beta)/input_password'), 'yIlnaVHzk9poZr384zkYiA==')

WebUI.click(findTestObject('Katalon/Page_Katalon Analytics (Beta)/label_Remember me'))

WebUI.click(findTestObject('Katalon/Page_Katalon Analytics (Beta)/button_Sign in'))

WebUI.click(findTestObject('Katalon/Page_Home - Katalon Analytics/a_Project_FedHR Regression'))

WebUI.click(findTestObject('Object Repository/Katalon/Page_Dashboard - Katalon Analytics/span_Executions'))

WebUI.delay(4)

WebUI.click(findTestObject('Katalon/Page_Dashboard - Katalon Analytics/span_logged-in-user'))

WebUI.click(findTestObject('Katalon/Page_Profile - Katalon Analytics/span_Sign Out'))

