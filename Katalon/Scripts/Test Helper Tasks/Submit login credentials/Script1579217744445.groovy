import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


List<String> validLoginAcceptedPage = Arrays.asList(
	'FedHR: Enter/Change email address',
	'FedHR Rules of behavior',
	'FedHR: Select a Role',
	'FedHR: Home/EBC',
	'FedHR: Home')

WebUI.click(findTestObject('FedHR/Page_FedHR Login/a_Username and Password'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/input_username'), 5)

WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_username'), username)

if (GlobalVariable.siteURL) {
	
    WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), 'TEmp0r@r!1')
} else {

    WebUI.setText(findTestObject('FedHR/Page_FedHR Login/input_password'), password)
}

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Login/button_Login'), 5)

WebUI.click(findTestObject('FedHR/Page_FedHR Login/button_Login'))

WebUI.delay(1)

try {
	assert validLoginAcceptedPage.contains(WebUI.getWindowTitle()) : 'Login failed! Either wrong credentials or wronng role configuration.'
}
catch (AssertionError err) {
	WebUI.takeScreenshot()

	throw err
}
