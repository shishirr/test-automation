import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcement - M/a_Create New Template'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Template Name_templateNa'), 'Blank VA Template')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Overview_btn_next'), 5)

CustomKeywords.'com.econsys.qa.utils.ActionUtils.clickUsingJS'(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Overview_btn_next'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Duties_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Qualifica/input_Qual_btn_next'), 5)

CustomKeywords.'com.econsys.qa.utils.ActionUtils.clickUsingJS'(findTestObject('FedHR/Page_FedHR Announcement - Qualifica/input_Qual_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Benefits/input_Benefits_btn_next'), 5)

CustomKeywords.'com.econsys.qa.utils.ActionUtils.clickUsingJS'(findTestObject('FedHR/Page_FedHR Announcement - Benefits/input_Benefits_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_HowTo_btn_save'), 5)

CustomKeywords.'com.econsys.qa.utils.ActionUtils.clickUsingJS'(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_HowTo_btn_save'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/a_Manage Vacancy Announcement'))

WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Vacancy Announcement - M/a_Blank VA Template'), 5)