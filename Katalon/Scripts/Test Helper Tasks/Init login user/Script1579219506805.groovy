import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


List<String> validHomePage = Arrays.asList('FedHR: Home', 'FedHR: Home/EBC')

boolean hasSingleRole = true

String role = role

String currPage = WebUI.getWindowTitle()

while (!(validHomePage.contains(currPage))) {

	switch (currPage) {
		
		case 'FedHR: Enter/Change email address':
		
			WebUI.setText(findTestObject('FedHR/Page_FedHR EnterChange email address/input_Type in your email address_newEmail'),
			testUserEmail)

			WebUI.setText(findTestObject('FedHR/Page_FedHR EnterChange email address/input_Verify your email address_verifyNewEmail'),
					testUserEmail)

			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR EnterChange email address/input_Update E-Mail Address_change'),
					10)

			WebUI.click(findTestObject('FedHR/Page_FedHR EnterChange email address/input_Update E-Mail Address_change'))

			break
			
		case 'FedHR Rules of behavior':
		
			WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Rules of behavior/button_Accept_Rules of behavior'),
			10)

			WebUI.click(findTestObject('FedHR/Page_FedHR Rules of behavior/button_Accept_Rules of behavior'))

			break
			
		case 'FedHR: Select a Role':

			hasSingleRole = false

			WebUI.callTestCase(findTestCase('Test Cases/Test Helper Tasks/Choose a user role'), [('isRoleSwitching') : false, ('role') : role])

			break
			
		case 'FedHR: Login':
		
			WebUI.takeScreenshot()

			KeywordUtil.markErrorAndStop('Login process failed! Either role lacks access or invalid login session.')

		case 'FedHR Navigator: Logged Out':
			
			WebUI.takeScreenshot()
			
			KeywordUtil.markErrorAndStop('Unexpected logout! Check the possibility of concurrent login.')
			
		default:

			WebUI.takeScreenshot()

			KeywordUtil.markErrorAndStop("Unknown '${currPage}' page experienced in the login process.")
	}
	
	currPage = WebUI.getWindowTitle()
}
if (hasSingleRole) {
	
	if (role.equalsIgnoreCase('employee')) {
		
		WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'))

		WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR HomeEBC/p_Role Employee'))
	
	} else {
	
		WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Home/a_Home'))

		assert WebUI.getText(findTestObject('FedHR/Page_FedHR Home/p_Role HR')).contains(role)
	}
}