import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

'This option would not be visible upon first login'
if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Home/a_Change Role'), 5)) {
	
    WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Change Role'))
}

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Select a Role/a_Administrator'), 5)

WebUI.click(findTestObject('FedHR/Page_FedHR Select a Role/a_Administrator'))

WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Home/a_Home'))

assert WebUI.getText(findTestObject('FedHR/Page_FedHR Home/p_Role HR')).contains('Administrator')