import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint


if (GlobalVariable.siteURL) {
	
    WebUI.navigateToUrl((('https://' + GlobalVariable.siteURL) + '/frbweb/logon.do?operation=index&client=') + agency)
	
} else {

    WebUI.navigateToUrl((('https://' + GlobalVariable.env) + '.econsys.com/frbweb/logon.do?operation=index&client=') + agency)
}

'Check if we need to accept DHS govt. warning'
if (['CISA', 'DHSHQ'].contains(agency)) {
	
    if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_NPPD US Govt. Warning/input_agree'), 10)) {
		
        WebUI.delay(2)

        WebUI.scrollToElement(findTestObject('FedHR/Page_NPPD US Govt. Warning/input_agree'), 10)

        WebUI.delay(2)

        WebUI.click(findTestObject('FedHR/Page_NPPD US Govt. Warning/input_agree'))

        WebUI.delay(2)
    }
}

/*****************************************************************************/
// TODO clean this piece after environment fix

if (["USDAOIG", "USCG"].contains(agency)) {
	
	if (WebUI.waitForAlert(2)) {
		
		WebUI.acceptAlert()
	}
}

String currPage = ''

WebUI.delay(2)

try {
    currPage = WebUI.getWindowTitle()
}
catch (StepFailedException sfe) {
    
    if (sfe.getCause() instanceof org.openqa.selenium.UnhandledAlertException) {
		
		WebUI.delay(3)
		
        'alert should get auto-handled by driver capabilities {unhandledPromptBehavior: accept and notify}'
        currPage = WebUI.getWindowTitle()
    }
}
/*****************************************************************************/

try {
    assert currPage.equals('FedHR: Login')
}
catch (AssertionError err) {
	
    WebUI.takeScreenshot()

    throw err
} 

