import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

'This option would not be visible upon first login'
if (WebUI.waitForElementPresent(findTestObject('FedHR/Page_FedHR Home/a_Change Role'), 5)) {
	
    WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Change Role'))
}

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Select a Role/a_Employee'), 5)

WebUI.click(findTestObject('FedHR/Page_FedHR Select a Role/a_Employee'))

WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'))

assert WebUI.getText(findTestObject('FedHR/Page_FedHR HomeEBC/p_Role Employee')).contains('Employee')

