import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (isRoleSwitching) {
	
    WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Change Role'))
}

String role = role

TestObject roleLink = findTestObject('FedHR/Page_FedHR Select a Role/a_(Role Name)', [('roleName') : role])

WebUI.scrollToElement(roleLink, 10)

WebUI.click(roleLink)

WebUI.delay(1)

if (role.equalsIgnoreCase('employee')) {
	
    WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR HomeEBC/a_HomeEBC'))

    WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR HomeEBC/p_Role Employee'))
} else {

    WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Home/a_Home'))

    assert WebUI.getText(findTestObject('FedHR/Page_FedHR Home/p_Role HR')).contains(role)
}

