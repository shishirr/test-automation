import org.openqa.selenium.WebDriver as WebDriver

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebDriver driver = null

try
{	
    driver = DriverFactory.getWebDriver()
}
catch (BrowserNotOpenedException)
{	
	// Setting system property due to a katalon bug in linux env
	String browserType = RunConfiguration.getDriverSystemProperty("WebUI", "browserType")
	switch (browserType)
	{
		case 'FIREFOX_DRIVER':
			System.setProperty("webdriver.gecko.driver", DriverFactory.getGeckoDriverPath())
            break;
//        case 'CHROME_DRIVER':
//            System.setProperty("webdriver.chrome.driver", DriverFactory.getChromeDriverPath())
//            break;
//        case 'HEADLESS_DRIVER':
//            System.setProperty("webdriver.chrome.driver", DriverFactory.getChromeDriverPath())
//            break;
        case 'FIREFOX_HEADLESS_DRIVER':
            System.setProperty("webdriver.gecko.driver", DriverFactory.getGeckoDriverPath())
            break;
        default:
			break;
	}
    WebUI.openBrowser(null)
} 

driver = DriverFactory.getWebDriver()

String browser = DriverFactory.getBrowserVersion(driver)

WebUI.setViewPortSize(1024, 745)

// TODO Need to come back and remove this piece
'hacking for MicrosoftEdge'
if (browser.contains('Edge'))
{
    WebUI.maximizeWindow()
}