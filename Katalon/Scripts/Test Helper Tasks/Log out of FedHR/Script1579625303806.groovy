import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('FedHR/Page_FedHR Logged Out/a_Logout'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('FedHR/Page_FedHR Logged Out/h2_Goodbye'), FailureHandling.CONTINUE_ON_FAILURE)