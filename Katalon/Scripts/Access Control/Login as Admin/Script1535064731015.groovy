import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


WebUI.callTestCase(findTestCase('Test Helper Tasks/Navigate to FedHR Login'), [:])

WebUI.callTestCase(findTestCase('Test Helper Tasks/Submit login credentials'), [:])

WebUI.callTestCase(findTestCase('Test Helper Tasks/Init login user'), [('role'): 'Administrator'])