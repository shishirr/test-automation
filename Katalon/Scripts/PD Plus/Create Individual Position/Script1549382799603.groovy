import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String stdPosition = '000003'

String identifier = new Random().nextLong().abs().toString().substring(0, 5)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Ind. Position'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Ind. Position'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_StdPosLookup'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Individual Positi/input_StdPosSearch_Position Number'), stdPosition)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/a_Std Position_000003'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Individual Positi/input_Individual Position Numb'), identifier)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_OrganizationLookup'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_OrganizationLookup'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/a_Select'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Individual Positi/input_Budget Number_pinBudgetN'), identifier)

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Individual Positi/input_Position Working Title_p'), 'Test Automation Engineer')

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_ReportsToLookup'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Individual Positi/span_Search'), 10)

CustomKeywords.'Util.clickUsingJS'(findTestObject('FedHR/Page_FedHR Create Individual Positi/span_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/div_checkbox_PD Num'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/span_Select'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_Save'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/button_Save'))

String statusMsg = WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Individual Positi/li_status text_created'), 10)

KeywordUtil.logInfo('Status Message: ' + statusMsg)

WebUI.getText(findTestObject('FedHR/Page_FedHR Create Individual Positi/li_status text_created'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Individual Positi/a_PD Library'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/input_Position Title_positionT'), 10)

WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Number_position'), identifier)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/select_AllStandardIndividual'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_AllStandardIndividual'), '2', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

expectedPos = ((stdPosition + '-') + identifier)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Result/a_Position Search Results_row 1', [('pos') : expectedPos]))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Individual Position/input_Individual Position Numb'), 
    10)

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Individual Position/input_Individual Position Numb'), 
    'value', identifier, 10)

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Create Individual Positi/input_Budget Number_pinBudgetN'), 
    'value', identifier, 10)