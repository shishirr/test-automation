import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_PD Library'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_PD Library'))

/* The script will look for a STANDARD POSITION
* for the following filters */
'Standard Position under test'
stdPos = 'Cook, NA-7404-08, #000225'

'Search by Position Title'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Title_positionT'), 'cook')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Standard Position/input_Position Title_positionTitle'), 
    'value', 'Cook', 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Position Number'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Number_position'), '000225')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Standard Position/input_StandardMaster Position'), 'value', 
    '000225', 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Pay Plan'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Pay Plan_payPlan'), 'NA')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Pay Plan'), 'NA', false, 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Occupational Series Code'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_occSeriesCode_Field'), '7404')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyElementText(findTestObject('FedHR/Page_FedHR Standard Position/span_occSeriesCodeText'), '7404')

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Grade'
WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_SelectLowerGrade'), '08', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_SelectHigherGrade'), '08', false)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Grade'), '08', false, 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Organization'
WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/input_Org_Lookup_Button'))

WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Organizations_filterKeyword'), 'NSA Bethesda')

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/input_Organizations_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/a_Select_Org', [('org'): 'NSA Bethesda']))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

orgInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/div_Organization'))

assert orgInRecord.contains('NSA Bethesda')

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Position Status'
WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_Status_ActiveInactive'), 'Y', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyOptionSelectedByLabel(findTestObject('FedHR/Page_FedHR Standard Position/select_Status'), 'Active', false, 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by PD Text'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_PD Text_pd'), 'account')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Position Description'))

pdTextInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Position Description/body_Position Description'))

assert pdTextInRecord.contains('account')

WebUI.click(findTestObject('FedHR/Page_FedHR Position Description/a_Done'))

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Last Modified After'
WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/input_Last Modified After'), '04/04/2018')

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

lastModifiedDate = WebUI.getText(findTestObject('FedHR/Page_FedHR Standard Position/span_Last Modified Date'))

assert CustomKeywords.'com.econsys.qa.utils.DateUtils.compareDates'('04/04/2018', lastModifiedDate) < 1

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Until'
WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/input_Until_toModifiedDate'), '04/06/2018')

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

untilDate = WebUI.getText(findTestObject('FedHR/Page_FedHR Standard Position/span_Last Modified Date'))

assert CustomKeywords.'com.econsys.qa.utils.DateUtils.compareDates'('04/06/2018', untilDate) > -1

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by POI'
WebUI.selectOptionByIndex(findTestObject('FedHR/Page_FedHR PD Library/select_POI'), '1')

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected std position)'))

WebUI.verifyOptionSelectedByIndex(findTestObject('FedHR/Page_FedHR Standard Position/select_POI'), 1, 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

/* The script will look for an INDIVIDUAL POSITION
 * for the following filters
 * because only an individual position will have them */
'Individual Position under test'
indPos = 'Recreation Assistant, NF-0189-02, #000012-00000039'

'Search by Incumbent'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Incumbent_incumbent'), 'Last 1001040')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))

incumbentInRecord = WebUI.getText(findTestObject('FedHR/Page_FedHR Individual Position/span_incumbentText'))

assert incumbentInRecord.contains('Last 1001040')

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

'Search by Budget Number'
WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Budget Number_Field'), 'C-101')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Search'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/a_(expected ind position)'))

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Individual Position/input_pinBudgetNumber'), 'value', 
    'C-101', 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Standard Position/a_Back to Search Results'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Results/button_Change search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

