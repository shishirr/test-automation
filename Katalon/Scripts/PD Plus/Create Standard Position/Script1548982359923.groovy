import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String identifier = new Random().nextLong().abs().toString().substring(0, 6)

String posTitle = 'QA Analyst ' + identifier

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Std. Position'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Std. Position'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Standard Position/input_StandardMaster Position'), identifier)

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Standard Position/input_Position Title_positionT'), posTitle)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Pay Plan'), 'GS', true)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/button_OCC Code Lookup'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Standard Position/input_Enter the Series Code Oc'), '06')

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/input_OCC Search_button'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/a_OCC_link_0306'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Grade'), '10', true)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/button_Org Dept_Lookup'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/a_Select Org_15591'), 10)

CustomKeywords.'com.econsys.qa.utils.ActionUtils.clickUsingJS'(findTestObject('FedHR/Page_FedHR Create Standard Position/a_Select Org_15591'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/span_Select an organization_ui'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Pos Status_ActiveInactive'), 
    'Y', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_POI'), '571', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_PATCO Code'), 'A', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Functional Class'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Functional Class'), '12', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Pos Supvisory Code'), '4', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_FLSA'), 'E', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Early Retirement Indicator'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_Interdisciplinary'), 'Y', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/button_Add Occ. Series'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/button_Add Occ. Series'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Standard Position/input_Enter the Series Code Oc'), '05')

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/input_OCC Search_button_2'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/a_OCC_link_0501'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/select_clasfnStdCode'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Create Standard Position/select_clasfnStdCode'), 'X', true)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/button_Classification Std_Lookup'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/a_Copy'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/span_Classification Standards'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/span_Classification Standards'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Standard Position/button_Save'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Standard Position/li_ConfirmMsg_The Position has been creat'), 
    10)

String statusMsg = WebUI.getText(findTestObject('FedHR/Page_FedHR Create Standard Position/li_ConfirmMsg_The Position has been creat'))

KeywordUtil.logInfo('Status Message: ' + statusMsg)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_PD Library'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Clear'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/input_Position Title_positionT'), 10)

WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Title_positionT'), posTitle)

WebUI.setText(findTestObject('FedHR/Page_FedHR PD Library/input_Position Number_position'), identifier)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR PD Library/select_PositionLibrary'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR PD Library/select_PositionLibrary'), '1', false)

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library/button_Search'))

WebUI.click(findTestObject('FedHR/Page_FedHR PD Library Search Result/a_Position Search Results_row 1', [('pos') : identifier]))

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Standard Position/input_StandardMaster Position'), 'value', 
    identifier, 10)

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Standard Position/input_Position Title_positionTitle'), 
    'value', posTitle, 10)

WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Standard Position/span_Last Modify Date', [('day') : CustomKeywords.'Util.getCurrentDate'()]), 
    10)

