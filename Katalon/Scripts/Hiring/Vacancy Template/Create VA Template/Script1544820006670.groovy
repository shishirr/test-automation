import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Templates'))

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcement - M/a_Create New Template'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Template Name_templateNa'), 'Complete VA template')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Job Title_positionTitle'), 'Tester')

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/button_Lookup'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/a_Select'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_to_salaryLower'), '60000')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_to_salaryUpper'), '70000')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement Template -/select_SelectPer YearPer HourP'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_SelectPer YearPer HourP'), '1', 
    true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_to_salaryRange'), 'Per year')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_01234567891011'), '6', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_01234567891011121314151'), '11', 
    true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_ACADALCRCYEDEFEHESEXFWG'), 'GS', 
    true)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Pay plan Series  Grade_b'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Enter the Series code or'), '050')

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Enter the Series code or_2'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/a_Add'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/button_Cancel'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Pay plan Series  Grade_b2'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/a_Add_06_grade'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/a_Add_07_grade'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/button_Cancel_2'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_NA010203040506070809101'), '10', 
    true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Cyber SecurityData Scien'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Cyber SecurityData Scien'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Delete_btnstandard'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_search for_keyword'), 'Alex')

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_search for_srch'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/a_Add_1'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/button_Cancel'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Alexander Arkansas_dutyS'), '4')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_SelectNot ApplicableCon'), '2', 
    true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_SelectYesNo'), 'True', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement Template -/select_SelectNot RequiredOccas'), '0', 
    true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement Template -/input_YesNo_relocationAuthoriz'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_YesNo_relocationAuthoriz'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Additional relocation ex'), 'Test')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Overview_btn_next'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Overview_btn_next'))

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement Template -/input_Duties_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Qualifica/input_Qual_btn_next'), 10)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Qualifica/input_Qual_btn_next'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement - Qualifica/input_Qual_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Benefits/input_Benefits_btn_next'), 10)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - Benefits/input_Benefits_btn_next'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement - Benefits/input_Benefits_btn_next'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Name_name'), 10)

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Name_name'), 'John Doe')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Address_address'), '101 Main St')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input__city'), 'Alexandria')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/select_AlabamaAlaskaArizonaArk'), 'VA', 
    true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Country_country'), 'United States')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Zipcode_zipcode'), '58745')

WebUI.setText(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_Phone_phone'), '6985468794')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_HowTo_btn_save'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Announcement - How to Ap/input_HowTo_btn_save'))

