import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.SelectorMethod as SelectorMethod

String identifier = new Random().nextLong().abs().toString().substring(0, 4)

String postedVA = 'VA-19-19022'

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Vacancy Announcements'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Vacancy Announcements'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Vacancy Announcements/input_Announcement _vacAnnNumb'), postedVA)

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/input_Display VAs closed more'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/button_Search'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Vacancy Announcements/a_VA_link_to_be_clicked'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/a_VA_link_to_be_clicked'))

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/select_Select One - I want to'), 
    10)

WebUI.selectOptionByIndex(findTestObject('FedHR/Page_Job Announcement/select_Select One - I want to'), 
    1)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicants/a_Add Applicant'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ First Name_firstName'), 
    'Larry')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_Middle Name_middleName'), 
    'A')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ Last Name_lastName'), 
    'Flowers' + identifier)

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_Home Address_contact.add'), 
    '101 Main St')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ CityTown_contact.city'), 
    'Quantico')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/select_State_contact.state'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_State_contact.state'), 
    'VA', true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ PostalZip Code_contact.'), 
    '22222')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Country_contact.country'), 
    'US', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Phone_phone1'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Phone_phone2'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Phone_phone3'), 
    '3', true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ Telephone Numbers_telePh1'), 
    '2589632145')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ Telephone Numbers_telePh2'), 
    '7412589632')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ Telephone Numbers_telePh3'), 
    '1236325986')

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/input_ E-mail Address_contact.'), 
    ('testing' + identifier) + '@gmail.com')

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/input_Save_savePersonalInfo'), 
    10)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Save_savePersonalInfo'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/a_Eligibility  Status'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Citizen_ctznflag'), 
    'Y', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_selectiveService'), 
    '04', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/select_veteransPreference'), 10)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_veteransPreference'), 
    '4660', true)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/input_Geographic Locations_dut'), 
    10)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Geographic Locations_dut'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_basicElig'), 'Y', 
    true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_MQ-SO'), 'Y', true)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Grades Eligible_eligible'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/input_Grades to be Certified_c'), 
    10)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Grades to be Certified_c'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Priority'), '1', 
    true)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Special Eligibility_splE'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Merit Promotion_splEligi'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Military Spouse_splEligi'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_NCR_splEligibility'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Special Hiring Authority'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_VEOA_splEligibility'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Name Request'), 
    'Y', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Reviewed'), 'Y', 
    true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_FedHR Applicant/select_Referred'), 'Y', 
    true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Applicant/textarea_Comments'), 'Testing')

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/input_Save_saveEligibility'))

WebUI.click(findTestObject('FedHR/Page_FedHR Applicant/strong_Back to All Applicants'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicants/a_All'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Applicants/a_All'))

name = (('Flowers' + identifier) + ', Larry A')

TestObject to = findTestObject('FedHR/Page_FedHR Applicants/a_Flowers Larry A', [('text') : name])

WebUI.scrollToElement(to, 10)

WebUI.click(to)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Applicant/input_ E-mail Address_contact.'), 
    10)

WebUI.verifyElementAttributeValue(findTestObject('FedHR/Page_FedHR Applicant/input_ E-mail Address_contact.'), 
    'value', ('testing' + identifier) + '@gmail.com', 10)

