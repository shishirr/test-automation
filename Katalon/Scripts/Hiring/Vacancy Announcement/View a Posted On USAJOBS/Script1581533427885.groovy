import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String vaNumberOnForm, vaNumberOnUSAJobs

WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Compose and Save a VA'), [:])

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement'), 2)

vaNumberOnForm = WebUI.getAttribute(findTestObject('Object Repository/FedHR/Page_Job Announcement/input_AnnouncementRegister'), 'value')

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'POST_ON_USAJOBS', false)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'VIEW_ON_USAJOBS', false)

WebUI.switchToWindowTitle('USAJOBS - Job Announcement')

WebUI.click(findTestObject('Object Repository/FedHR/Page_USAJOBS - Job Announcement/a_Overview'))

vaNumberOnUSAJobs = WebUI.getText(findTestObject('FedHR/Page_USAJOBS - Job Announcement/p_joa-number'))

WebUI.verifyMatch(vaNumberOnForm, vaNumberOnUSAJobs, false)

WebUI.closeWindowTitle('USAJOBS - Job Announcement')

WebUI.switchToWindowTitle('Job Announcement')

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'AMEND', false)

WebUI.click(findTestObject('Object Repository/FedHR/Page_Job Announcement/button_Yes'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'REPOST_ON_USAJOBS', false)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'VIEW_ON_USAJOBS', false)

WebUI.switchToWindowTitle('USAJOBS - Job Announcement')

WebUI.click(findTestObject('Object Repository/FedHR/Page_USAJOBS - Job Announcement/a_Overview'))

vaNumberOnUSAJobs = WebUI.getText(findTestObject('FedHR/Page_USAJOBS - Job Announcement/p_joa-number'))

WebUI.verifyMatch(vaNumberOnForm, vaNumberOnUSAJobs, false)

WebUI.closeWindowTitle('USAJOBS - Job Announcement')

WebUI.switchToWindowTitle('Job Announcement')

WebUI.click(findTestObject('Object Repository/FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'))

WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Compose and Save a VA'), [:])

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement'), 2)

vaNumberOnForm = WebUI.getAttribute(findTestObject('Object Repository/FedHR/Page_Job Announcement/input_AnnouncementRegister'), 'value')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'POST_ON_USAJOBS', false)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'CANCEL', false)

WebUI.click(findTestObject('Object Repository/FedHR/Page_Job Announcement/button_Yes'))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_Processing... - modal-body'), 60)

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/FedHR/Page_Job Announcement/h2_Announcement'), "Announcement # - ${vaNumberOnForm} - CANCELLED")

WebUI.verifyOptionNotPresentByValue(findTestObject('Object Repository/FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'VIEW_ON_USAJOBS', false, 2)