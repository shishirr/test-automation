import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('FedHR/Page_FedHR Vacancy Announcements/input_Announcement _vacAnnNumb'), GlobalVariable.testVASeq)

'optional in most scenarios'
WebUI.check(findTestObject('FedHR/Page_FedHR Vacancy Announcements/input_Display VAs closed more'))

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/button_Search'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Vacancy Announcements/div_VA Search Results'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/a_Annc. Num - link', [('annNo') : GlobalVariable.testVASeq]))

WebUI.waitForElementVisible(findTestObject('FedHR/Page_Job Announcement/h2_Announcement'), 10)

String annInfo = WebUI.getText(findTestObject('FedHR/Page_Job Announcement/h2_Announcement'))

assert annInfo.contains(GlobalVariable.testVASeq)

