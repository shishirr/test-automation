import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Search for a VA'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Copy'))

WebUI.delay(2)

copiedVANum = WebUI.getAttribute(findTestObject('FedHR/Page_Job Announcement/input_announcementNumber'), 'value')

assert !(copiedVANum.contains(GlobalVariable.testVASeq))

copiedVAHeader = WebUI.getText(findTestObject('FedHR/Page_Job Announcement/h2_Announcement'))

assert !(copiedVAHeader.contains(GlobalVariable.testVASeq))

