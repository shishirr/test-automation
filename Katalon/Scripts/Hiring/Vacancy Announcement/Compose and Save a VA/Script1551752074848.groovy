import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'))

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Announcement Template'), 'Blank VA Template', 
    false)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Fill Out the VA form'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Save'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_OK'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('FedHR/Page_Job Announcement/span_field is required'), 10)

