import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'expected: a new VA is created and the form is open'
annNo = WebUI.getAttribute(findTestObject('FedHR/Page_Job Announcement/input_announcementNumber'), 'value')

'storing the value in GlobalVariable for future tests (in memory)'
GlobalVariable.testVASeq = (annNo + '-Katalon')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_announcementNumber'), GlobalVariable.testVASeq)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_jobTitle'), 'QA Analyst')

WebUI.selectOptionByIndex(findTestObject('FedHR/Page_Job Announcement/select_hiringAgency'), 1)

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/select_payPlan'), 2)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_payPlan'), 'GS', false)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Recruit request organiz'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Org. Code Search'), 'ndw')

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Org. Code Search_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Org. Code_radio'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH SERIES CODE'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Series code'), '05')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Series code search_btn'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Series_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_vaGrade'), '(09)|(10)|(11)+', true)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_missionCriticalOccupations'), 'DATA_SCIENTIST', 
    false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input__salaryRangeFrom'), '55000')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input__salaryRangeTo'), '65000')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_salaryBasis'), 'PER_YEAR', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_additionalSalaryBasisDescription'), 'w/ health and transport benefits')

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/input_Open date_openDate'), 2)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Open date_openDate'), CustomKeywords.'com.econsys.qa.utils.DateUtils.getCurrentDate'())

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Close date_closeDate'), CustomKeywords.'com.econsys.qa.utils.DateUtils.getCalculatedDate'(
        '+', 3, 'months'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_closingType'), 'APPLICATION_CUT_OFF', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Max num of applications'), '50')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_serviceType'), 'COMPETITIVE', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_announcementType'), 'CONTINUOUS_ONGOING', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_appointmentType'), 'PERMANENT', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_additionalAppointmentTypeDescription'), 'permanent type job')

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_workSchedule'), 'FULL_TIME', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_additionalWorkScheduleDescription'), 'full-time job')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_agencyMarketingUrl'), 'https://www.opm.gov')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Agency marketing description_ckeditor'), 'agency marketing description goes here')

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/button_SEARCH LOCATIONS'), 2)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH LOCATIONS'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_va_duty_country'), 'US', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_va_duty_state'), '51', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Location_va_duty_location'), 'woodb')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Search'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Duty Location_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Num of vacancy_Location'), '3')

not_run: WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_No_liveWithin_va_commute'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Yes_liveWithin_va_commute'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_liveWithinMaxDistance'), '50')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Yes_teleworkEligible'))

not_run: WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_No_relocationReimbursable'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Yes_relocationReimbursable'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_additionalRelocationReimbursableDescription'), '25% of the relocation expenses will be paid')

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/textarea_dutySummary'), 2)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/textarea_dutySummary'), 'duty summary goes here')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Major duties_ckeditor'), 'Major duities (ckeditor) goes here')

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/select_travelRequired'), 2)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_travelRequired'), 'OCCASIONAL_TRAVEL', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_additionalTravelDescription'), '25% or less travel required')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Yes_supervisory'))

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_promotionPotentialCd'), '11', false)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_va_apply_hiringPath'), 'PUBLIC', false)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_clarificationFromTheAgency'), 'US citizen or LPR only')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Conditions of employment_init'), 'Need to be a self-learner and quick-learner')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH CONDITIONS'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_financial2_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_licenses1_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_preemployment1_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_drug1_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_incentive1_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_otherReq1_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/button_SEARCH QUALIFICATIONS_qual'), 2)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH QUALIFICATIONS_qual'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Series_qualificationSeries'), '05')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_qualification_Search'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_qual3_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_qual4_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_qual5_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH QUALIFICATIONS_edu'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Series_qualificationSeries'), '05')

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_qualification_Search'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_qual6_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_qual7_chkbox'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_How applicants will be evaluated_ckeditor'), 'How applicants will be evaluated info (ckeditor) goes here')

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/select_securityClearance'), 2)

WebUI.selectOptionByValue(findTestObject('FedHR/Page_Job Announcement/select_securityClearance'), 'SENSITIVE_COMPARTMENTED_INFORMATION', 
    false)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Yes_drugTestRequired'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH STANDARD DOCUMEN'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Cover_reqDocSelect'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Resume_reqDocSelect'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_Transcript_reqDocSelect'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_SEARCH ADDITIONAL DOCUM'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_OF-306 Declaration of Fe'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Done'))

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/input_No_displayOpmDefaultBenefitsInfo'))

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Benefits URL'), 'https://www.opm.gov')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Benefits description_ckeditor'), 'Benefits description (ckeditor) goes here')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Details_ckeditor'), 'Details on How To Apply (ckeditor) goes here')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Name _agency_contact_name'), 'John Doe')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Address_agency_address'), '123 Anywhere St')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_City_agency_city'), 'Anytown')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_State_agency_state'), 'PA')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Zip code_agency_zip'), '12345')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Country_agency_country'), 'United States')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Phone_agency_phone'), '1234567890')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Email_agency_email'), 'john@doe.com')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/input_Phone _agency_internal_phone'), '1234567890')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Next steps_ckeditor'), 'Next step (ckeditor) goes here')

WebUI.setText(findTestObject('FedHR/Page_Job Announcement/body_Other information_ckeditor'), 'Other information (ckeditor) goes here')

