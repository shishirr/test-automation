import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'))

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Announcement Template'), 'Blank VA Template', 
    false)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))

GlobalVariable.testVASeq = WebUI.getAttribute(findTestObject('FedHR/Page_Job Announcement/input_announcementNumber'), 'value')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/button_Save as Draft'), 10)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_Save as Draft'))

WebUI.click(findTestObject('FedHR/Page_Job Announcement/button_OK'))

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'), 10)

WebUI.delay(1)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'))

WebUI.callTestCase(findTestCase('Hiring/Vacancy Announcement/Search for a VA'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('FedHR/Page_Job Announcement/h2_Announcement'), 10)

WebUI.verifyElementText(findTestObject('FedHR/Page_Job Announcement/h2_Announcement'), ('Announcement # - ' + GlobalVariable.testVASeq) + 
    ' - DRAFT')

