import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String email1 = 'regression_test_1@econsys.com', email2 = 'regression_test_2@econsys.com'
String emailEntered, innerText_divEmailNoti

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'), 2)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/input_Add Email Address'), "$email1, $email2")

WebUI.click(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/input_email_Add_button'))

emailEntered = WebUI.getText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/div_email_ids_container'))

assert emailEntered.contains(email1) && emailEntered.contains(email2)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'), 2)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'))

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Announcement Template'), 'Blank VA Template', 
    false)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/strong_Email Notification'), 2)

innerText_divEmailNoti = WebUI.getText(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_emailAddress_notification'))

assert innerText_divEmailNoti.contains(email1) && innerText_divEmailNoti.contains(email2)

WebUI.scrollToElement(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'), 2)

WebUI.click(findTestObject('FedHR/Page_Job Announcement/a_Vacancy Announcements_breadcrumb'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'), 2)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Manage Vacancy Email'))

WebUI.click(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/button_email_deleteIcon', [('email') : email1]))

emailEntered = WebUI.getText(findTestObject('FedHR/Page_FedHR Manage Vacancy Email/div_email_ids_container'))

assert !(emailEntered.contains(email1)) && emailEntered.contains(email2)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'), 2)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Create Announcement'))

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Announcement Template'), 'Blank VA Template', 
    false)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_Job Announcement/strong_Email Notification'), 2)

innerText_divEmailNoti = WebUI.getText(findTestObject('Object Repository/FedHR/Page_Job Announcement/div_emailAddress_notification'))

assert !(innerText_divEmailNoti.contains(email1)) && innerText_divEmailNoti.contains(email2)