import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

int expectedNumOfQuestions

int actualNumOfQuestions

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Recruitment'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'))

if (WebUI.waitForElementNotPresent(findTestObject('FedHR/Page_FedHR Recruitment/li_Applicant Questionnaire'), 1)) {

    CustomKeywords.'com.econsys.qa.utils.ActionUtils.forceStop'("Current Recruitment Request does not have a questionnaire!")
	
} else {

    WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/li_Applicant Questionnaire'))

    List l = CustomKeywords.'com.econsys.qa.utils.TestObjUtils.getHtmlTableRows'(findTestObject('FedHR/Page_FedHR Applicant Questionnaire/table_Applicant Questionnaire'), 
        'TBODY')

    expectedNumOfQuestions = l.size()

    WebUI.click(findTestObject('FedHR/Page_FedHR Applicant Questionnaire/a_RecruitmentRequestNum'))
	
	WebUI.delay(1)
	
}

WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Vacancy Announcements'))

WebUI.click(findTestObject('FedHR/Page_FedHR Vacancy Announcements/a_Create Announcement'))

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Create Announcement/select_Blank VA Template'), 'Blank VA Template', 
    false)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Announcement/button_Create'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_Job Announcement/select_jobWant_IWantTo'), 'View applicant questionnaire', 
    false)

WebUI.switchToWindowTitle('FedHR: Vacancy Announcement - Applicant Questionnaire')

WebUI.delay(2)

List l = CustomKeywords.'com.econsys.qa.utils.TestObjUtils.getHtmlTableRows'(findTestObject('FedHR/Page_FedHR Vacancy Announcement - Applicant Questionnaire/table_Applicant Questionnaire'), 
    'TBODY')

actualNumOfQuestions = l.size()

assert expectedNumOfQuestions == actualNumOfQuestions

WebUI.closeWindowTitle('FedHR: Vacancy Announcement - Applicant Questionnaire')

WebUI.switchToWindowTitle('Job Announcement')

