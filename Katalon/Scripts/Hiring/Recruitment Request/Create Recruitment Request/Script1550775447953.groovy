import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String identifier = new Random().nextLong().abs().toString().substring(0, 6)

String dateOfToday = CustomKeywords.'Util.getCurrentDate'()

String expectedCaseType = 'Recruitment'

String expectedCaseActivity = 'Recruitment Request'

String expectedLastDetailStep = 'Initiated'

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Home/a_Create Recruitment Request'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_LookUp_Position_btn'))

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Position Title_field'), 
    'cook')

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Search_btn'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/a_PD_Number_000225'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_LookUp_Duty Location_btn'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_LookUp_Duty Location_btn'))

WebUI.selectOptionByValue(findTestObject('Object Repository/FedHR/Page_FedHR Create Recruitment Reque/select_State'), '51', 
    true)

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_City_field'), 'Alexandria')

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_search location_btn'))

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/a_City_Alexandria'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Create Recruitment Reque/input_Vice_field'), 'Other Opening')

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Create Recruitment Reque/input_Proposed Effective Date_field'), 
    dateOfToday)

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Create Recruitment Reque/textarea_Requester Comments'), 
    'Testing Recruitment Request')

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Appropriation Code_field'), 
    '101')

WebUI.setText(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Budget Number(s)_field'), 
    identifier)

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Add Budget Number(s)_btn'), 10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Add Budget Number(s)_btn'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Save and Next_btn'), 
    10)

WebUI.click(findTestObject('FedHR/Page_FedHR Create Recruitment Reque/input_Save and Next_btn'))

String caseType = WebUI.getText(findTestObject('FedHR/Page_FedHR Recruitment/span_CaseType'))

WebUI.verifyMatch(caseType.trim(), expectedCaseType, false)

String caseActivity = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/span_CaseActivity'))

WebUI.verifyMatch(caseActivity.trim(), expectedCaseActivity, false)

String lastDetailStep = WebUI.getText(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/span_LastDetailStep'))

WebUI.verifyMatch(lastDetailStep.trim(), expectedLastDetailStep, false)

WebUI.verifyElementPresent(findTestObject('Object Repository/FedHR/Page_FedHR Recruitment/span_BudgetNum', [('bud_num') : identifier]), 
    10)