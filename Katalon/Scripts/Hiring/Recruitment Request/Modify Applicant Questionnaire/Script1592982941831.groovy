import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Recruitment'))

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Recruitment/a_Recruitment_First_Case'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Applicant Questionnaire/a_Job Analysis_link'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Job Analysis/a_Search'), 1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Job Analysis/a_Search'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Search for KSAs and Questions/input_Search'),
		1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Search for KSAs and Questions/input_Search'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Search Results for KSAs and Questions/input_FirstKSAcheckBox'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Search Results for KSAs and Questions/input_AddSelectedRoles'),
		1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Search Results for KSAs and Questions/input_AddSelectedRoles'))

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Confirm Selection for KSAs and Questions/input_ConfirmSelection'),
		1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Confirm Selection for KSAs and Questions/input_ConfirmSelection'))

WebUI.setText(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/textarea_View PD_taskOrDuty'),
		'Testing')

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/input_AddGradeButton'), 1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/input_AddGradeButton'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/a_Add_1'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/span_Cancel_ui-button'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/select_Source'), 1)

WebUI.selectOptionByIndex(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/select_SelectKSA_Type'),
		2)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/SaveAndNext'), 1)

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Add KSA and Question/SaveAndNext'))

WebUI.click(findTestObject('Object Repository/FedHR/Page_FedHR Job Analysis/a_ Back'))