import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.findWebElement

import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String reportRole = 'Administrator'
String reportName = 'Audit Log - Logins'
TestObject to

'Enabling a report script starts'
WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'))

WebUI.delay(1)

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Manage Report Access/select_Manage Report Access Role'), reportRole, 
    false)

to = findTestObject('FedHR/Page_FedHR Manage Report Access/input_((reportName))_rpt_chkbox', [('reportName') : reportName])

WebUI.scrollToElement(to, 1)

if (findWebElement(to).isSelected()) {
    KeywordUtil.logInfo("The report is already enabled for the ${reportRole} role!")
} else {
    WebUI.check(to)
}

WebUI.click(findTestObject('FedHR/Page_FedHR Manage Report Access/input_Save_button'))

WebUI.delay(1)

assert WebUI.getAlertText().contains('Updated successfully!')

WebUI.delay(1)

WebUI.acceptAlert()

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Reports'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Reports'))

to = findTestObject('FedHR/Page_FedHR Reports/a_((reportName))', [('reportName') : reportName])

WebUI.scrollToElement(to, 1)

WebUI.click(to)

WebUI.verifyElementPresent(findTestObject('FedHR/Page_FedHR Reports/button_Download Report'), 1)

'Disabling a report script starts'
WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Manage Report Access'))

WebUI.delay(1)

WebUI.selectOptionByLabel(findTestObject('FedHR/Page_FedHR Manage Report Access/select_Manage Report Access Role'), reportRole, 
    false)

to = findTestObject('FedHR/Page_FedHR Manage Report Access/input_((reportName))_rpt_chkbox', [('reportName') : reportName])

WebUI.scrollToElement(to, 1)

if (!(findWebElement(to).isSelected())) {
    KeywordUtil.logInfo("The report is already disabled for the ${reportRole} role!")
} else {
    WebUI.uncheck(to)
}

WebUI.click(findTestObject('FedHR/Page_FedHR Manage Report Access/input_Save_button'))

WebUI.delay(1)

assert WebUI.getAlertText().contains('Updated successfully!')

WebUI.delay(1)

WebUI.acceptAlert()

WebUI.scrollToElement(findTestObject('FedHR/Page_FedHR Home/a_Reports'), 1)

WebUI.click(findTestObject('FedHR/Page_FedHR Home/a_Reports'))

to = findTestObject('FedHR/Page_FedHR Reports/a_((reportName))', [('reportName') : reportName])

WebUI.verifyElementNotPresent(to, 1)

