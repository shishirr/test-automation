import login from "../common/login";
import logout from "../common/logout";
import selectRole from "../common/selectRole";
import visitAs from "../common/visitAs";
import acceptRules from "../common/acceptRules";
import createCase from "../common/createCase";

describe("FEMA Smoke Test", function() {
  const currentDate = new Date();

  it("runs a search as an admin for their own user", function() {
    cy.task("queryAdminCreds", 1).then(result => {
      const cred = result.filter(row => row.account_id === 100)[0];
      const id = cred.username.replace(/\D/g, "");
      visitAs(cred.client_code);
      login(cred);
      selectRole(`Administrator for ${cred.client_code.toUpperCase()}`);
      cy.get("#head_search_employee_name").type(`LAST ${id}`);
      cy.get("#head_search_employee_submit > i").click();
      cy.get("#employeetable").within(() => {
        cy.contains(`LAST ${id}`).click();
      });
      cy.contains("Personal Information");
      cy.contains("Employee Setup");
      logout();
    });
  });

  it("Create a new case and update the created case", function() {
    let caseNumber;

    cy.task("queryAdminCreds", 1).then(result => {
      const cred = result.filter(row => row.account_id === 100)[0];
      visitAs(cred.client_code);
      login(cred);
      selectRole(`Administrator for ${cred.client_code.toUpperCase()}`);
      createCase(currentDate,1);
      cy.contains(`Performance Appraisal - ${currentDate.getFullYear()}`);
      cy.get(
        "#maincolumnfixed > form:nth-child(14) > div > h3.first-child > span"
      ).then($elem => {
        caseNumber = $elem.text().replace(/\D/g, "");
        cy.contains("Home").click();
        cy.get("#navItem_1006 > a").click()
        cy.contains(`${caseNumber}`).click();
        cy.contains(`Case #${caseNumber}`);

        cy.get('input[title="Add Step/Comment/Reassign"]').click();
        cy.get("#chkEmail").select("No");
        cy.get("#activity").select(
          `Performance Appraisal - ${currentDate.getFullYear() - 1}`
        );
        cy.get("#comments").type("test update case comments");
        cy.get("#hrcomments").type("test update case hr comment");
        cy.get("#step").select("Progress Reviews");
        cy.get('input[title="Save"]').click();

        cy.contains(`Case #${caseNumber}`);
        cy.contains("test update case comments");
        cy.contains("test update case hr comment");
        cy.contains("Progress Reviews");
        cy.contains(`Performance Appraisal - ${currentDate.getFullYear() - 1}`);
      });
      logout();
    });
  });

  it("create two back to back cases and checks form data submission is isolated", function() {
    cy.task("queryAdminCreds", 1).then(result => {
      const cred = result.filter(row => row.account_id === 100)[0];
      visitAs(cred.client_code);
      login(cred);
      selectRole(`Administrator for ${cred.client_code.toUpperCase()}`);
      createCase(currentDate, 1);
      cy.contains(`Performance Appraisal - ${currentDate.getFullYear()}`);
      cy.get(
        "#maincolumnfixed > form:nth-child(14) > div > h3.first-child > span"
      ).then($elem => {
        const caseNumber1 = $elem.text().replace(/\D/g, "");
        cy.get("#altrows > div:nth-child(2) > span.value > strong > a").then($elem => {
          const case1EmployeeLName = $elem.text().split(",")[0]
          createCase(currentDate, 2);
          cy.contains(`Performance Appraisal - ${currentDate.getFullYear()}`);
          cy.get(
            "#maincolumnfixed > form:nth-child(14) > div > h3.first-child > span"
          ).then($elem1 => {
            const caseNumber2 = $elem1.text().replace(/\D/g, "");
            cy.log(caseNumber2);
            cy.log(case1EmployeeLName)
            cy.log(caseNumber1)
            cy.get("#navItem_1006 > a").click()
            cy.get("#trackingNumber").type(caseNumber1)
            cy.get("#TBL22 > tbody > tr:nth-child(2) > td > button:nth-child(4)").click()
            cy.get(`a[name="case_${caseNumber1}"]`).click();
            cy.contains("DHS 310 FEMA").click()
            cy.get("#ui-id-1 > div > button.btnstandard.btnstandard-alt.right-margin-xs").click()
            cy.get("#LAST_NAME").should("have.value", case1EmployeeLName)
          });
        })
      });
    });
  });

  it("checks user is unable to search for users from other accounts", function() {
    cy.task("queryAdminCreds", 1).then(result => {
      const cred = result.filter(row => row.account_id === 100)[0];
      visitAs(cred.client_code);
      login(cred);
      selectRole(`Administrator for ${cred.client_code.toUpperCase()}`);
      cy.task("queryAdminCreds", 1).then(result => {
        const nonFEMACreds = result.filter(obj => obj.account_id != 100);
        nonFEMACreds.forEach(nonFEMAcred => {
          cy.get("#head_search_employee_name")
            .clear()
            .type(nonFEMAcred.last_name);
          cy.get("#head_search_employee_submit > i").click();
          cy.get("employeetable").should(
            "not.have.value",
            nonFEMAcred.last_name
          );
        });
      });
    });
  });
});
