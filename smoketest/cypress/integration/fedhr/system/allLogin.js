import visitAs from "../common/visitAs";
import login from "../common/login";
import logout from "../common/logout";

describe("allLogin", function() {
  it("test login to fedhr for all accounts", function() {
    cy.task("queryAdminCreds", 1).then(result => {
      result.forEach(cred => {
        visitAs(cred.client_code);
        if (cred.account_id === 110 || cred.account_id === 111) {
          cy.get("#agreeToTermsBtn").click();
          login(cred);
          logout();
        } else {
          login(cred);
          logout();
        }
      });
    });
  });
});
