import login from "../common/login";
import visitAs from "../common/visitAs";
import logout from "../common/logout";

describe("allRules", function() {
  it("Validates rules of behavior pages shows", function() {
    cy.task("queryAdminCreds", 2).then(result => {
      result.forEach(cred => {
        visitAs(cred.client_code);
        if (cred.account_id === 110 || cred.account_id === 111) {
          cy.get("#agreeToTermsBtn").click();
          login(cred);
          cy.contains("I agree and accept these rules of behavior");
          logout();
        } else {
          login(cred);
          cy.contains("I agree and accept these rules of behavior");
          logout();
        }
      });
    });
  });
});
