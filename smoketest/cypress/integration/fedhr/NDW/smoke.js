import login from "../common/login";
import logout from "../common/logout";
import selectRole from "../common/selectRole";
import visitAs from "../common/visitAs";
import acceptRules from "../common/acceptRules";
import createCase from "../common/createCase";

describe("FEMA Smoke Test", function() {
  const currentDate = new Date();

  //   it("runs a search as an admin for their own user", function() {
  //     cy.task("queryAdminCreds", 1).then(result => {
  //       const cred = result.filter(row => row.account_id === 232)[0];
  //       const id = cred.username.replace(/\D/g, "");
  //       visitAs(cred.client_code);
  //       login(cred);
  //       selectRole(`Administrator for CNIC`);
  //       cy.get("#head_search_employee_name").type(`LAST ${id}`);
  //       cy.get("#head_search_employee_submit > i").click();
  //       cy.get("#employeetable").within(() => {
  //         cy.contains(`LAST ${id}`).click();
  //       });
  //       cy.contains("Personal Information");
  //       cy.contains("Employee Setup");
  //       logout();
  //     });
  //   });

  it("Create a new vacancy announcement", function() {
    cy.task("queryAdminCreds", 1).then(result => {
      const cred = result.filter(row => row.account_id === 232)[0];
      visitAs(cred.client_code);
      login(cred);
      selectRole(`Administrator for CNIC`);
      cy.get('a[title="Vacancy Announcements"]').click();
      cy.get('a[title="Create a Vacancy Announcement"]').click();
      cy.get("#templateId").select("1994");
      cy.get('button[title="Select to creat vacancy announcement"]').click();
      cy.get("#va_job_title").type("cypress test job title");
      cy.get('button[title="Select to search for organization"]').click();
      cy.get(
        'button[title="Select to submit search and view list of results below"]'
      ).click();
      cy.contains("tr", "NAS Patuxent River").within(() =>
        cy.get('[type="radio"]').check()
      );
      cy.get(
        'button[title="Select to submit selection and close modal"]'
      ).click();
      //   logout();
    });
  });

  //   it("create two back to back cases and checks form data submission is isolated", function() {
  //     let caseNumber1;
  //     let caseNumber2;
  //     cy.task("queryAdminCreds", 1).then(result => {
  //       const cred = result.filter(row => row.account_id === 232)[0];
  //       visitAs(cred.client_code);
  //       login(cred);
  //       selectRole(`Administrator for CNIC`);
  //       createCase(currentDate);
  //       cy.contains(`Performance Appraisal - ${currentDate.getFullYear()}`);
  //       cy.get(
  //         "#maincolumnfixed > form:nth-child(14) > div > h3.first-child > span"
  //       ).then($elem => {
  //         caseNumber1 = $elem.text().replace(/\D/g, "");
  //         createCase(currentDate);
  //         cy.contains(`Performance Appraisal - ${currentDate.getFullYear()}`);
  //         cy.get(
  //           "#maincolumnfixed > form:nth-child(14) > div > h3.first-child > span"
  //         ).then($elem1 => {
  //           caseNumber2 = $elem1.text().replace(/\D/g, "");
  //           cy.log(caseNumber1);
  //           cy.log(caseNumber2);
  //         });
  //       });
  //     });
  //   });

  //   it("checks user is unable to search for users from other accounts", function() {
  //     cy.task("queryAdminCreds", 1).then(result => {
  //       const cred = result.filter(row => row.account_id === 232)[0];
  //       visitAs(cred.client_code);
  //       login(cred);
  //       selectRole(`Administrator for CNIC`);
  //       cy.task("queryAdminCreds", 1).then(result => {
  //         const nonNDWCreds = result.filter(obj => obj.account_id != 232);
  //         nonNDWCreds.forEach(nonNDWCred => {
  //           cy.get("#head_search_employee_name")
  //             .clear()
  //             .type(nonNDWCred.last_name);
  //           cy.get("#head_search_employee_submit > i").click();
  //           cy.get("employeetable").should(
  //             "not.have.value",
  //             nonNDWCred.last_name
  //           );
  //         });
  //       });
  //     });
  //   });
});
