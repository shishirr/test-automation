const acceptRules = () => {
  cy.contains("I agree and accept these rules of behavior").click();
};

export default acceptRules;
