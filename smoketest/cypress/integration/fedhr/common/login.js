const login = (cred) => {
  cy.get("#page-links-username").click();
  cy.get("#q").type(`${cred.username}`);
  cy.get("#r").type(`${cred.password}`);
  cy.get("#username-submit").click();

  cy.get('body')
  .then($body => {
    if ($body.find('#roleSelection').length) {
      return 'multiple-roles';
    }
    return 'one-role';
  })
  .then(roles => {
    cy.log(roles);

    if(roles == 'one-role') {
      cy.wait(6000);
    }
    cy.contains("Logout");
  });
  
};

export default login;
