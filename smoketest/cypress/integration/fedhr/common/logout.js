const logout = () => {
  cy.contains("Logout").click();
  cy.contains("Goodbye!");
  cy.contains(
    "You have successfully logged out of FedHR Navigator. For your security, you should close your browser window."
  );
  cy.url().should("include", "/logoff.do");
};

export default logout;
