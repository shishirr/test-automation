const selectRole = role => {
  cy.contains("Select a Role");
  cy.contains("Select a role from the available roles below.");
  cy.contains(role).click();
  cy.contains("Case Tracking");
};

export default selectRole;