const visitAs = client_code => {
  cy.visit(
    `https://app.stage.fedhrnavigator.com/frbweb/logon.do?operation=index&client=${client_code}&redir=0`
  );
};

export default visitAs;
