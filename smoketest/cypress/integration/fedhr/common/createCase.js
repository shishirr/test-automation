const createCase = (currentDate,nameOption) => {
  cy.get("#menuNewCase > a").click();
  cy.get("#case_type_id").select("Performance");
  cy.get("#case_activity_type_id").select(
    `Performance Appraisal - ${currentDate.getFullYear()}`
  );
  cy.get("#case_step_type_id").select("Interim Evaluation");
  cy.get('input[value="1120"]').check();
  cy.get("#comments").type("test create case comment");
  cy.get("#comments_hr").type("test create case hr comment");
  cy.get("#empsearch_input").type("last");
  cy.get("#empsearch").click();
  cy.get(`#emplist > div:nth-child(${nameOption}) > button`, {
    timeout: 15000
  }).click();
  cy.get("#email_emp_y_n").select("No");
  cy.get("#save").click();
  cy.contains("Case Information");
  cy.url().should("include", "/employee/casetracking");
  
};

export default createCase;
