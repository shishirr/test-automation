// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const Promise = require("bluebird");
const oracledb = require("oracledb");
require('dotenv').config();
const dbconfig = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  connectString: process.env.DB_CONNECT_STRING
};

oracledb.initOracleClient({
	libDir: process.env.ORA_INSTACLIENT_DIR,
  });

const adminsWoROB = `
SELECT DISTINCT
	client_code,
	account_id,
	Username,
	Password,
	FIRST_NAME,
	LAST_NAME,
	role_name
FROM
	(
	SELECT
		a.URL_CODE AS client_code ,
		a.account_id AS account_id ,
		u.user_name AS Username ,
		'password' AS Password ,
		u.USER_FNAME AS FIRST_NAME ,
		u.USER_LNAME AS LAST_NAME ,
		r.role_name ,
		g.group_name ,
		u.user_id ,
		DENSE_RANK() OVER (PARTITION BY a.URL_CODE,
		r.role_name
	ORDER BY
		u.user_id,
		a.URL_CODE,
		r.role_name) AS rnk
  FROM sa_account sa
  INNER JOIN cp_account_settings a ON
    sa.account_id = a.account_id
	INNER JOIN cp_user u ON
		a.account_id = u.account_id
	INNER JOIN cp_USER_GROUP_ROLE ugr ON
		u.user_id = ugr.user_id
	INNER JOIN cp_group_role gr ON
		gr.group_role_id = ugr.group_role_id
	INNER JOIN cp_group g ON
		g.group_id = gr.group_id
	INNER JOIN cp_role r ON
		r.role_id = gr.role_id
	WHERE
		r.ROLE_NAME = 'Administrator'
    AND u.user_name LIKE 'user%'
    AND u.USER_EMAIL IS NOT NULL
    AND u.USER_PW IS NOT NULL
    AND sa.acct_status = 'O'
    AND user_status = 'O'
    AND u.rofb_accept_date IS NULL
		AND u.INITIALIZED_Y_N = 'Y'
    AND g.GROUP_NAME != 'Former Employees'
    AND sa.account_id != 224
	AND sa.account_id != 194
	ORDER BY
		a.URL_CODE,
		r.ROLE_NAME) f
	WHERE rnk < 2
ORDER BY ACCOUNT_ID
`;

const adminsWithROB = `
SELECT
	client_code,
	account_id,
	Username,
	Password,
	FIRST_NAME,
	LAST_NAME,
	role_name
FROM
	(
	SELECT
		a.URL_CODE AS client_code ,
		a.account_id AS account_id ,
		u.user_name AS Username ,
		'password' AS Password ,
		u.USER_FNAME AS FIRST_NAME ,
		u.USER_LNAME AS LAST_NAME ,
		r.role_name ,
		g.group_name ,
		u.user_id ,
		DECODE((SELECT MAX(1) FROM employee_data e WHERE u.user_id = e.employee_id), NULL, 'No', 'Yes') AS is_employee,
		DENSE_RANK() OVER (PARTITION BY a.URL_CODE,
		r.role_name
	ORDER BY
		u.user_id,
		a.URL_CODE,
		r.role_name) AS rnk
  FROM sa_account sa
  INNER JOIN cp_account_settings a ON
    sa.account_id = a.account_id
	INNER JOIN cp_user u ON
		a.account_id = u.account_id
	INNER JOIN cp_USER_GROUP_ROLE ugr ON
		u.user_id = ugr.user_id
	INNER JOIN cp_group_role gr ON
		gr.group_role_id = ugr.group_role_id
	INNER JOIN cp_group g ON
		g.group_id = gr.group_id
	INNER JOIN cp_role r ON
		r.role_id = gr.role_id
	WHERE
		r.ROLE_NAME = 'Administrator'
    AND u.user_name LIKE 'user%'
    AND u.USER_PW IS NOT NULL
    AND sa.acct_status = 'O'
    AND user_status = 'O'
    AND u.USER_EMAIL IS NOT NULL
    AND u.rofb_accept_date IS NOT NULL
		AND u.INITIALIZED_Y_N = 'Y'
    AND g.GROUP_NAME != 'Former Employees'
    AND sa.account_id != 224
	AND sa.account_id != 194
    AND DECODE((SELECT MAX(1) FROM employee_data e WHERE u.user_id = e.employee_id), NULL, 'No', 'Yes') = 'Yes'
	ORDER BY
		a.URL_CODE,
		r.ROLE_NAME) f
WHERE
	rnk <2
	AND client_code IS NOT NULL
`;

module.exports = (on, config) => {
  // "cy.task" can be used from specs to "jump" into Node environment
  // and doing anything you might want. For example, checking "data.json" file!
  on("task", {
    queryAdminCreds: type => {
      if (type === 1) {
        query = adminsWithROB;
      }
      if (type === 2) {
        query = adminsWoROB;
      }
      return oracledb
        .getConnection(dbconfig)
        .then(function(conn) {
		  conn.currentSchema = "FEDHR";
          return conn
            .execute(query)
            .then(function(result) {
              const credList = result.rows;
              const namedList = credList.map(row => {
                let obj = {};
                obj["client_code"] = row[0];
                obj["account_id"] = row[1];
                obj["username"] = row[2];
                obj["password"] = row[3];
                obj["role_name"] = row[6];
                obj["last_name"] = row[5];
                return obj;
              }, []);
              console.log(namedList);
              return namedList;
            })
            .catch(function(err) {
              console.error(err);
              return conn.close();
            });
        })
        .catch(function(err) {
          console.error(err);
        });
    }
  });
};
