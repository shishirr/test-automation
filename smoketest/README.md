# Checkout project
- `git clone ssh://git@git.econsys.com:2222/fhr/test-automation.git`
- `git checkout develop`

# Connect to stage database 
- Instructions to connect to database is at [https://wiki.econsys.com/display/FedHR/FedHR+Code+Deployments]

# Install cypress and dependencies
- In the terminal, `cd smoketest` and run
    - `yarn install`

- Additionaly, if you are on Windows, run 
    - `./node_modules/.bin/cypress cache clear`
    - `./node_modules/.bin/cypress install --force`

# Update .env file 
- `cd smoketest`
- Open `.env` file and modify to set the the appropriate database configuration, and save.
- In the terminal, run: `npm run cypress:open` to open Cypress window
- Click on `fedhr -> system -> allLogin.js`
- This will open up browser and start running the test. It take a few minutes to complete. 
- Watch the terminal for any errors (the terminal where you typed `npm run cypress:open`)
    - *A frequent error when setting up this project is it fails to connect to the stage database. You can verify if the connection is working by connect to the stage db using SQL Developer with same information from your .env file.*
    - *`[Error: ORA-12541: TNS:no listener] { errorNum: 12541, offset: 0 }`*
    - *If you edi the config, you need to close cypress, check your stage db connect and the config in .env file and restart the test.*

# On successful complete completion
- When the test is complete successfully, take a screenshot of the test browser. For an example screenshot, see https://jira.econsys.com/browse/FHRINF-3975
- Before each deployment, ISSO will provide a ticket number to attach the test result to.

